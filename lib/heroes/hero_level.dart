import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import '../utils/constants.dart';

class HeroLevel extends StatelessWidget {
  final int level;
  final int xp;

  HeroLevel(this.level, this.xp);

  @override
  Widget build(BuildContext context) {
    var maxXp = (level * XP_COST_PER_LEVEL);
    return SleekCircularSlider(
        appearance: CircularSliderAppearance(
          startAngle: 180,
          angleRange: 360,
          customWidths: CustomSliderWidths(progressBarWidth: 8),
          customColors: CustomSliderColors(
            trackColor: Theme.of(context).unselectedWidgetColor,
            progressBarColors: [Theme.of(context).primaryColor, Theme.of(context).accentColor],
          ),
        ),
        min: 0,
        max: (level*XP_COST_PER_LEVEL).toDouble(),
        initialValue: xp.toDouble(),
        innerWidget: (double value) {
          return Center(
              child: Container(
                height: 50,
                child: Column(
                  children: [
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: 'Level: ',
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1,
                          ),
                          TextSpan(
                            text: level.toString(),
                            style: Theme
                                .of(context)
                                .textTheme
                                .overline,
                          ),
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'XP: ',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyText1,
                            ),
                            TextSpan(
                              text: '$xp / $maxXp',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .overline,
                            )
                          ]
                      ),
                    )
                  ],
                ),
              )
          );
        }
    );
  }
}