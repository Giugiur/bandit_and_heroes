import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../battle/ability.dart';
import '../battle/spell.dart';
import '../items/item.dart';
import '../items/items.dart';
import '../utils/constants.dart';
import '../utils/custom_icons.dart';
import '../world/world.dart';

class Hero with ChangeNotifier {
  Hero({
    this.heroClass,
    this.level,
    this.xp,
    this.isBenched,
    this.id,
    this.items,
    this.lastQuest,
    this.questsCompleted,
    this.stats
  });

  Hero.clone(Hero otherHero): this(
      id: otherHero.id,
      heroClass: otherHero.heroClass,
      level: otherHero.level,
      xp: otherHero.xp,
      isBenched: otherHero.isBenched,
      items: otherHero.items,
      lastQuest: otherHero.lastQuest,
      questsCompleted: otherHero.questsCompleted,
      stats: otherHero.stats
  );

  final String heroClass;
  int level;
  int xp;
  bool isBenched;
  final String id;
  DateTime lastQuest;
  List<String> items;
  List<Spell> availableSpells;
  List<dynamic> questsCompleted;
  Map<String, int> stats;
  int hp;
  int armorModifier = 0;
  int damageModifier = 0;
  int criticalModifier = 0;
  bool hasAdditionalTarget = false;
  bool receivedDamageThisRound = false;
  bool isStunned = false;
  bool preventIncomingDamage = false;
  int preventQuantity = 0;
  bool returnDamage = false;
  int returnDamageQuantity = 0;
  bool hasPassiveDisabled = false;
  bool cleanCriticalModifier = true;
  int additionalTargetsPerItems = 0;

  int maxHp(context) {
    return 5 + (vitality+getVitalityFromItems(context))*3;
  }

  int get strength {
    return stats['strength'];
  }

  int get dexterity {
    return stats['dexterity'];
  }

  int get vitality {
    return stats['vitality'];
  }

  int get intelligence {
    return stats['intelligence'];
  }

  int setStat(String stat, int amount) {
    stats[stat] = amount;
    notifyListeners();
  }

  void dispel() {
    isStunned = false;
    notifyListeners();
  }

  int get postBattleReward {
    return level * POST_BATTLE_REWARDS_GOLD;
  }

  bool get isSpellcaster {
    return heroClass == 'priest' || heroClass == 'sorceress' || heroClass == 'warlock';
  }

  List<Widget> getSpellsText(context) {
    switch (heroClass) {
      case 'priest':
        {
          return [
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Hands Imposition:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Heals a random ally for an amount based on his intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Blessing:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Increases armor and critical chance of a random ally based on his intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Heavenly Strength:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Increases the strength and removes any stuns of the ally with most strength in the party based on his intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
          ];
        }
      case 'sorceress':
        {
          return [
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Frostbite:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Stuns a random enemy for a round and reduces its strength and dexterity for a small amount based on her intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Lightning Strike:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Damages two random enemies based on her intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Fire Decoy:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Prevent the next attack directed to her. Damages the attacker based on her intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
          ];
        }
      case 'warlock':
        {
          return [
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Decay:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' Two random enemies lose stats and receive damage based on his intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Devil\'s Speed:',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' The slowest ally gains dexterity based on his intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Hell\'s Chains',
                    style: Theme.of(context).textTheme.overline,
                  ),
                  TextSpan(
                    text: ' The quickest enemy loses dexterity and receives damage based on his intelligence.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
          ];
        }
      default: {
        return [];
      }
      break;
    }
  }

  List<Spell> get spells {
    switch (heroClass) {
      case 'priest':
        {
          return [
            Spell(
              name: 'Hands Imposition',
              originalMaxTargets: 1,
            ),
            Spell(
              name: 'Blessing',
              originalMaxTargets: 1,
            ),
            Spell(
              name: 'Heavenly Strength',
              originalMaxTargets: 1,
            )
          ];
        }
      case 'sorceress':
        {
          return [
            Spell(
              name: 'Frostbite',
              originalMaxTargets: 1,
            ),
            Spell(
              name: 'Lightning Strike',
              originalMaxTargets: 2,
            ),
            Spell(
              name: 'Fire Decoy',
            )
          ];
        }
      case 'warlock':
        {
          return [
            Spell(
              name: 'Devil\'s Speed',
              originalMaxTargets: 1,
            ),
            Spell(
              name: 'Decay',
              originalMaxTargets: 2,
            ),
            Spell(
              name: 'Hell\'s Chains',
              originalMaxTargets: 1,
            )
          ];
        }
      default: {
        return [];
      }
      break;
    }
  }

  Ability get instantiateAbility {
    switch (heroClass) {
      case 'warlord':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Fury',
            triggerOnReceivingDamage: true,
          );
        }
      case 'shieldbearer':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Glorybearer',
            triggerOnAllyDeath: true,
            triggersOncePerRound: true,
          );
        }
      case 'paladin':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Resurrection',
            triggerOnOwnerDeath: true,
            triggersOncePerBattle: true,
          );
        }
      case 'huntress':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Marked',
            triggerOnInflictingDamage: true,
          );
        }
      case 'sniper':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Take Aim',
            triggerOnNotReceivingDamage: true,
          );
        }
      case 'assassin':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Backstab',
            triggerOnBattleStart: true,
          );
        }
      case 'bounty_hunter':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Payday',
            triggerOnBattleEnd: true,
          );
        }
      case 'priest':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Yerobath\'s Training',
          );
        }
      case 'sorceress':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Multicast',
            triggerOnSpellCast: true,
            triggerChance: 50,
          );
        }
      case 'warlock':
        {
          return Ability(
            owner: this,
            isPassive: true,
            name: 'Touch of Magobath',
            triggerOnTargetingSpell: true,
          );
        }
    }
  }

  void setHp(int mod){
    hp = mod;
    notifyListeners();
  }

  void setHeroAbilitiesPerItems(context) {
    var itemsProvider = Provider.of<Items>(context, listen: false);
    int additionalAttacks = 0;
    items.forEach((item) {
      Item foundItem = itemsProvider.findById(item);
      if (foundItem != null && foundItem.ability != null && foundItem.ability.hasAdditionalAttack) {
        additionalAttacks++;
      }
      if (foundItem != null && foundItem.ability != null && foundItem.ability.name.toLowerCase() == 'carapace') {
        returnDamage = true;
      }
    });
    additionalTargetsPerItems = additionalAttacks;
  }

  int getVitalityFromItems(context) {
    var itemsProvider = Provider.of<Items>(context, listen: false);
    int vitality = 0;
    items.forEach((item) {
      Item foundItem = itemsProvider.findById(item);
      if (foundItem != null && foundItem.isArmor()) {
        vitality += foundItem.vitality;
      }
    });
    return vitality;
  }

  List<Ability> getItemAbilities(context) {
    var itemsProvider = Provider.of<Items>(context, listen: false);
    List<Ability> abilities = [];
    items.forEach((item) {
      Item foundItem = itemsProvider.findById(item);
      if (foundItem != null && foundItem.ability != null) {
        foundItem.ability.owner = this;
        abilities.add(foundItem.ability);
      }
    });
    return abilities;
  }

  void unequipItem (Item item) {
    items.removeWhere((equippedItems) => equippedItems == item.id);
    notifyListeners();
  }

  void equipItem (Item item) {
    items.add(item.id);
    notifyListeners();
  }

  int getArmor(context) {
    var itemsProvider = Provider.of<Items>(context, listen: false);
    int armor = 0;
    items.forEach((item) {
      Item foundItem = itemsProvider.findById(item);
      if (foundItem != null && foundItem.isArmor()) {
        armor += foundItem.armor;
      }
    });
    return armor + armorModifier;
  }

  int getItemsStatsBonus(context, String stat) {
    var itemsProvider = Provider.of<Items>(context, listen: false);
    int bonus = 0;
    items.forEach((item) {
      Item itemFound = itemsProvider.findById(item);
      if (itemFound != null) {
        if (stat == 'strength') {
          bonus += itemFound.strength ;
        }
        if (stat == 'dexterity') {
          bonus += itemFound.dexterity;
        }
        if (stat == 'intelligence') {
          bonus += itemFound.intelligence;
        }
        if (stat == 'vitality') {
          bonus += itemFound.vitality;
        }
      }
    });
    return bonus;
  }

  int attributeDamage(context) {
    var itemsProvider = Provider.of<Items>(context, listen: false);
    int damage;
    if (mainStat == 'strength') {
      damage = strength;
    }
    if (mainStat == 'dexterity') {
      damage = dexterity;
    }
    if (mainStat == 'intelligence') {
      damage = intelligence;
    }

    //TODO: Replace this for damage = getItemsStatsBonus(context, mainstat);
    items.forEach((item) {
      Item itemFound = itemsProvider.findById(item);
      if (itemFound != null) {
        if (mainStat == 'strength') {
          damage += itemFound.strength ;
        }
        if (mainStat == 'dexterity') {
          damage += itemFound.dexterity;
        }
        if (mainStat == 'intelligence') {
          damage += itemFound.intelligence;
        }
      }
    });

    itemsProvider.opponentItemList.forEach((item) {
      if (item.owner == id) {
        if (mainStat == 'strength') {
          damage += item.strength;
        }
        if (mainStat == 'dexterity') {
          damage += item.dexterity;
        }
        if (mainStat == 'intelligence') {
          damage += item.intelligence;
        }
      }
    });
    return damage;
  }

  int weaponDamage(context) {
    var itemProvider = Provider.of<Items>(context, listen: false);
    int damage = 0, i = 0, j = 0;
    bool found = false;
    Item weapon;
    while (i < items.length && !found) {
     weapon = itemProvider.findById(items[i]);
      if (weapon != null && weapon.isWeapon()) {
         found = true;
      }
      i++;
    }
    while(j < itemProvider.opponentItemList.length && !found) {
      weapon = itemProvider.opponentItemList[j];
      if (weapon != null && weapon.isWeapon() && weapon.owner == id) {
        found = true;
      }
      j++;
    }
    if (weapon != null) {
      damage = weapon.damage;
    }
    return damage + damageModifier;
  }

  bool isEquipped(Item item) {
    return items.firstWhere((equippedItem) => equippedItem == item.id, orElse: () => null) != null;
  }

  bool isOnQuest(context) {
    int cooldown = Provider.of<World>(context, listen: false).questCooldown.inSeconds;
    return lastQuest.add(Duration(seconds: cooldown)).isAfter(DateTime.now());
  }

  void addExperience(int experience) {
    xp += experience;
    if (xp >= level*XP_COST_PER_LEVEL) {
      xp -= level*XP_COST_PER_LEVEL;
      level++;
      stats['strength'] += statGain['strength'];
      stats['dexterity'] += statGain['dexterity'];
      stats['intelligence'] += statGain['intelligence'];
      stats['vitality'] += statGain['vitality'];
    }
    notifyListeners();
  }

  Map<String, int> get statGain {
    switch (heroClass) {
      case 'warlord': {
        return {
          'strength': 2,
          'intelligence': 1,
          'dexterity': 1,
          'vitality': 2,
        };
      }
      case 'shieldbearer': {
        return {
          'strength': 2,
          'intelligence': 1,
          'dexterity': 1,
          'vitality': 2,
        };
      }
      case 'paladin': {
        return {
          'strength': 2,
          'intelligence': 1,
          'dexterity': 1,
          'vitality': 2,
        };
      }
      case 'huntress': {
        return {
          'strength': 1,
          'intelligence': 1,
          'dexterity': 2,
          'vitality': 2,
        };
      }
      case 'sniper': {
        return {
          'strength': 1,
          'intelligence': 1,
          'dexterity': 3,
          'vitality': 1,
        };
      }
      case 'assassin': {
        return {
          'strength': 1,
          'intelligence': 1,
          'dexterity': 3,
          'vitality': 1,
        };
      }
      case 'bounty_hunter': {
        return {
          'strength': 1,
          'intelligence': 1,
          'dexterity': 2,
          'vitality': 2,
        };
      }
      case 'priest': {
        return {
          'strength': 2,
          'intelligence': 2,
          'dexterity': 1,
          'vitality': 1,
        };
      }
      case 'sorceress': {
        return {
          'strength': 1,
          'intelligence': 3,
          'dexterity': 1,
          'vitality': 1,
        };
      }
      case 'warlock': {
        return {
          'strength': 1,
          'intelligence': 2,
          'dexterity': 1,
          'vitality': 2,
        };
      }
    }
  }

  IconData get icon {
    switch (heroClass) {
      case 'warlord': {
        return CustomIcons.relic_blade;
      }
      case 'shieldbearer': {
        return CustomIcons.broken_shield;
      }
      case 'paladin': {
        return CustomIcons.angel_wings;
      }
      case 'huntress': {
        return CustomIcons.target_arrows;
      }
      case 'sniper': {
        return CustomIcons.crossed_pistols;
      }
      case 'assassin': {
        return CustomIcons.cloak_and_dagger;
      }
      case 'bounty_hunter': {
        return CustomIcons.crossbow;
      }
      case 'priest': {
        return CustomIcons.aura;
      }
      case 'sorceress': {
        return CustomIcons.crystal_wand;
      }
      case 'warlock': {
        return CustomIcons.death_skull;
      }
    }
  }

  String get mainStat {
    switch (heroClass) {
      case 'warlord': {
        return 'strength';
      }
      case 'shieldbearer': {
        return 'strength';
      }
      case 'paladin': {
        return 'strength';
      }
      case 'huntress': {
        return 'dexterity';
      }
      case 'sniper': {
        return 'dexterity';
      }
      case 'assassin': {
        return 'dexterity';
      }
      case 'bounty_hunter': {
        return 'dexterity';
      }
      case 'priest': {
        return 'intelligence';
      }
      case 'sorceress': {
        return 'intelligence';
      }
      case 'warlock': {
        return 'intelligence';
      }
    }
  }

  List<ItemType> get availableTypes {
    switch (heroClass) {
      case 'warlord': {
        return [ItemType.One_Handed_Sword, ItemType.Two_Handed_Sword, ItemType.Shield, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'shieldbearer': {
        return [ItemType.One_Handed_Sword, ItemType.Shield, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'paladin': {
        return [ItemType.One_Handed_Sword, ItemType.Two_Handed_Sword, ItemType.Shield, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'huntress': {
        return [ItemType.Bow, ItemType.Crossbow, ItemType.Dagger, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'sniper': {
        return [ItemType.Bow, ItemType.Crossbow, ItemType.Gun, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'assassin': {
        return [ItemType.One_Handed_Sword, ItemType.Dagger, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'bounty_hunter': {
        return [ItemType.Bow, ItemType.Crossbow, ItemType.Gun, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'priest': {
        return [ItemType.One_Handed_Sword, ItemType.Staff, ItemType.Shield, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'sorceress': {
        return [ItemType.Staff, ItemType.Armor, ItemType.Trinket];
      }
      break;
      case 'warlock':  {
        return [ItemType.Staff, ItemType.Armor, ItemType.Trinket];
      }
      break;
    }
  }

  String get weapons {
    switch (heroClass) {
      case 'warlord': {
        return 'Sword, Axe, Shield';
      }
      break;
      case 'shieldbearer': {
        return 'Sword, Axe, Shield';
      }
      break;
      case 'paladin': {
        return 'Sword, Axe, Shield';
      }
      break;
      case 'huntress': {
        return 'Bow, Crossbow, Dagger';
      }
      break;
      case 'sniper': {
        return 'Bow, Crossbow, Gun.';
      }
      break;
      case 'assassin': {
        return 'Sword, Dagger';
      }
      break;
      case 'bounty_hunter': {
        return 'Bow, Crossbow, Gun';
      }
      break;
      case 'priest': {
        return 'Sword, Staff, Shield';
      }
      break;
      case 'sorceress': {
        return 'Staff';
      }
      break;
      case 'warlock':  {
        return 'Staff';
      }
      break;
    }
  }

  Map<String, dynamic> get passiveAbility {
    switch (heroClass) {
      case 'warlord': {
        return {
          'title': 'Fury: ',
          'description': 'If the Warlord has been attacked or the target of a spell during this round, he gains an additional attack when he attacks this round.',
          'icon': CustomIcons.monster_skull
        };
      }
      break;
      case 'shieldbearer': {
        return {
          'title': 'Glorybearer: ',
          'description': 'If an ally would die from an attack, instead redirect that attack to the Shieldbearer. This passive can trigger only once per round.',
          'icon': CustomIcons.player_pain
        };
      }
      break;
      case 'paladin': {
        return {
          'title': 'Resurrection: ',
          'description': 'When the Paladin dies, resurrect her with ${(ABILITY_PALADIN_HEALTH * 100).toStringAsFixed(0)}% of her Max HP. This ability can only trigger once per battle.',
          'icon': CustomIcons.ankh
        };
      }
      break;
      case 'huntress': {
        return {
          'title': 'Mark of the Hunt: ',
          'description': 'Enemies damaged by the Huntress have their armor reduced by $ABILITY_HUNTRESS_MARK. This bonus can stack.',
          'icon': CustomIcons.cracked_shield
        };
      }
      break;
      case 'sniper': {
        return {
          'title': 'Take Aim: ',
          'description': 'If during this round, the Sniper hasn\'t been attacked, it gains +$ABILITY_SNIPER_AIM% Critical Hit Chance.',
          'icon': CustomIcons.targeted
        };
      }
      break;
      case 'assassin': {
        return {
          'title': 'Backstab: ',
          'description': 'At the start of the battle, the Assassin critically attacks the slowest hero on the opposing Party.',
          'icon': CustomIcons.diving_dagger
        };
      }
      break;
      case 'bounty_hunter': {
        return {
          'title': 'Payday: ',
          'description': 'Victories against other parties grants Gold based on his level.',
          'icon': CustomIcons.gold_bar
        };
      }
      break;
      case 'priest': {
        return {
          'title': 'Yerobath\'s Training: ',
          'description': 'The Priest can attack and cast a spell on the same round.',
          'icon': CustomIcons.aura
        };
      }
      break;
      case 'sorceress': {
        return {
          'title': 'Multicast: ',
          'description': 'Casting a spell has a 50% chance of targeting an additional target.',
          'icon': CustomIcons.focused_lightning
        };
      }
      break;
      case 'warlock':  {
        return {
          'title': 'Touch of Magobath: ',
          'description': 'Whenever it casts a spell, deactivates the passive of the enemy affected by the spell for an entire round.',
          'icon': CustomIcons.hand_emblem
        };
      }
      break;
    }
  }

  Map<String, dynamic> get activeAbility {
    switch (heroClass) {
      case 'warlord': {
        return {
          'title': 'Taunt: ',
          'description': 'The next enemy attack is directed to the Warlord.',
          'icon': CustomIcons.player_pyromaniac
        };
      }
      break;
      case 'shieldbearer': {
        return {
          'title': 'Shield Bash: ',
          'description': 'Attacks with his shield damaging based on Strength. Reduce the next incoming attack or spell damage by 25%.',
          'icon': CustomIcons.bolt_shield
        };
      }
      break;
      case 'paladin': {
        return {
          'title': 'Yerobath\'s Hand: ',
          'description': 'Heal the ally with lowest health for an amount based on Intelligence.',
          'icon': Icons.volunteer_activism
        };
      }
      break;
      case 'huntress': {
        return {
          'title': 'Lay Traps: ',
          'description': 'Attacks to a member of your Party have a 25% chance of failing and stunning the attacker for the rest of the round. Consecutive Traps will increase this chance by 5%.',
          'icon': CustomIcons.bear_trap
        };
      }
      break;
      case 'sniper': {
        return {
          'title': 'Rapid Fire: ',
          'description': 'Shoots to an additional target if she can. Reduces damage to 75% during this round.',
          'icon': CustomIcons.musket
        };
      }
      break;
      case 'assassin': {
        return {
          'title': 'Poison Attack: ',
          'description': 'Inflicts a poisonous wound on an enemy, reducing its Strength by 25% for the next round and damaging it based on Dexterity.',
          'icon': CustomIcons.venomous_snake
        };
      }
      break;
      case 'bounty_hunter': {
        return {
          'title': 'On the Track: ',
          'description': 'Increases Dexterity for the slowest allied based on his Dexterity.',
          'icon': CustomIcons.divert
        };
      }
      break;
      case 'priest': {
        return {
          'title': 'Spell: ',
          'description': 'Casts a Spell.',
          'icon': Icons.auto_stories
        };
      }
      break;
      case 'sorceress': {
        return {
          'title': 'Spell: ',
          'description': 'Skips her attack and casts a Spell.',
          'icon': Icons.auto_stories
        };
      }
      break;
      case 'warlock':  {
        return {
          'title': 'Spell: ',
          'description': 'Skips his attack and casts a Spell.',
          'icon': Icons.auto_stories
        };
      }
      break;
    }
  }

  String get heroDescription {
    switch (heroClass) {
      case 'warlord': {
          return 'A fearsome warrior that outputs a lot of damage and thrives when attacked.';
        }
        break;
      case 'shieldbearer': {
        return 'A defensive hero which excels at protecting the members of his Party.';
      }
      break;
      case 'paladin': {
          return 'An advocate of justice, the Paladin resurrects while on combat to keep fighting.';
        }
      break;
      case 'huntress': {
        return 'The Huntress specializes in debilitating the enemy Party while dealing damage.';
      }
      break;
      case 'sniper': {
        return 'Deadly when is not attacked, the Sniper has great damage but low survivability.';
      }
      break;
      case 'assassin': {
        return 'Furtiveness and stealth are the hallmarks of the Assassin, which can strike the enemy Party even before the battle starts.';
      }
      break;
      case 'bounty_hunter': {
        return 'Expert on ranged weapons, the Bounty Hunter makes your Party a lot more ... profitable.';
      }
      break;
      case 'priest': {
        return 'Combining spells and physical attacks, the Priest can heal and buff allies while delivering blows to the enemy Party.';
      }
      break;
      case 'sorceress': {
        return 'Strong spells and multicasting makes the Sorceress one of the biggest threats in the battle.';
      }
      break;
      case 'warlock':  {
        return 'A master of dark magics and curses, the Warlock manipulates the stats and abilities of both friends and enemies.';
      }
      break;
    }
  }

  String get assetImage {
    switch(heroClass) {
      case 'warlord': {
        return 'assets/images/warlord.jpg';
      }
      break;
      case 'paladin': {
        return 'assets/images/paladin.jpg';
      }
      break;
      case 'shieldbearer': {
        return 'assets/images/shieldbearer.jpg';
      }
      break;
      case 'huntress': {
        return 'assets/images/huntress.jpg';
      }
      break;
      case 'assassin': {
        return 'assets/images/assassin.jpg';
      }
      break;
      case 'bounty_hunter': {
        return 'assets/images/bounty_hunter.jpg';
      }
      break;
      case 'sniper': {
        return 'assets/images/sniper.jpg';
      }
      break;
      case 'priest': {
        return 'assets/images/priest.jpg';
      }
      break;
      case 'sorceress': {
        return 'assets/images/sorceress.jpg';
      }
      break;
      case 'warlock': {
        return 'assets/images/warlock.jpg';
      }
      break;
      default: {
        return 'assets/images/hero_placeholder.jpg';
      }
      break;
    }
  }

}