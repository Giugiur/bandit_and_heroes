import 'package:bandit_and_heroes/world/world.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'hero.dart' as Bandit;
import 'heroes.dart';
import 'hero_draggable.dart';
import 'heroes_panel.dart';
import 'recruit_hero_screen.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';
import '../widgets/announcer.dart';
import '../widgets/empty_state.dart';
import '../widgets/loading_elevated_button.dart';

class HeroesScreen extends StatefulWidget {
  static const routeName = '/heroes';

  @override
  _HeroesScreenState createState() => _HeroesScreenState();
}

class _HeroesScreenState extends State<HeroesScreen> {
  List<Bandit.Hero> _heroes = [];
  List<Bandit.Hero> partyHeroes = [];
  List<Bandit.Hero> benchedHeroes = [];
  List panels = [];
  bool _isLoading = false;

  void _buildPanels() {
    partyHeroes = _heroes.where((hero) => !hero.isBenched).toList();
    benchedHeroes = _heroes.where((hero) => hero.isBenched).toList();
    panels = [
      HeroesPanel(
        isExpanded: partyHeroes.length == 0 ? false : true,
        header: 'Your Party (${partyHeroes.length})',
        body: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: 300, minHeight: 56.0),
          child: SingleChildScrollView(
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: (ctx, index) => HeroDraggable(partyHeroes[index]),
              itemCount: partyHeroes.length,
            ),
          )
        ),
        iconpic: Icon(Icons.group),
        heroes: partyHeroes
      ),
      HeroesPanel(
        isExpanded: benchedHeroes.length == 0 ? false : true,
        header: 'Bench (${benchedHeroes.length})',
        body: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: 260, minHeight: 56.0),
          child: Container(
            padding: const EdgeInsets.only(bottom: 10),
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: (ctx, index) => HeroDraggable(benchedHeroes[index]),
              itemCount: benchedHeroes.length,
            ),
          ),
        ),
        iconpic: Icon(Icons.cached),
        heroes: benchedHeroes,
      ),
    ];
  }

  void didChangeDependencies() async {
    _heroes = Provider.of<Heroes>(context).heroes;
    if (_heroes.isEmpty) {
      setState(() {
        _isLoading = true;
      });
      await Provider.of<World>(context, listen: false).getUserData();
      setState(() {
        _isLoading = false;
      });
    }
    _buildPanels();
    super.didChangeDependencies();
  }

  void _goToRecruitHero() {
    Navigator.of(context).pushNamed(RecruitHeroScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      appBar: BanditBar(),
      body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            height: deviceSize.height,
            child: _isLoading ? Center(child: CircularProgressIndicator(),) :
            ListView(
              children: [
                DragTarget<String>(
                  builder: (context, incoming, rejected) {
                    return ExpansionPanelList(
                      elevation: 4,
                      expandedHeaderPadding: const EdgeInsets.all(0),
                      dividerColor: Theme.of(context).dividerColor,
                      expansionCallback: (int index, bool isExpanded) {
                        setState(() {
                          panels[index].isExpanded = !panels[index].isExpanded;
                        });
                      },
                      children: panels.map((panel) {
                        return ExpansionPanel(
                          backgroundColor: Theme.of(context).canvasColor,
                          headerBuilder: (BuildContext context, bool isExpanded) {
                            return ListTile(
                              leading: panel.iconpic,
                              title: Text(
                                panel.header,
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.headline4,
                              ),
                            );
                          },
                          isExpanded: panel.isExpanded,
                          body: panel.body,
                        );
                      }).toList(),
                    );
                  },
                  onWillAccept: (id) {
                    var hero = _heroes.firstWhere((hero) => hero.id == id);
                    if ((partyHeroes.length > 4 && hero.isBenched == true) || hero.isOnQuest(context)) {
                      return false;
                    }
                    return true;
                  },
                  onAcceptWithDetails: (details) {
                    // I'm not entirely happy with this. There's no way of knowing if the draggable is being dropped on the
                    // Party or the Bench. I tried doing some magic with offset but is not working for all cases.
                      var hero = _heroes.firstWhere((hero) => hero.id == details.data);
                      Provider.of<Heroes>(context, listen: false).switchBenched(hero);
                      _buildPanels();
                  },
                  onLeave: (data) {
                    Announcer(context).showErrorMessage(
                      'Couldn\'t move your hero',
                      'Maybe your party is full, your hero is on a quest or you moved the hero to an undroppable target.'
                    );
                  },
                ),
                SizedBox(height: 20,),
                LoadingElevatedButton('Recruit New Hero', false, false, _goToRecruitHero),
              ],
            ),
          ),
        ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}