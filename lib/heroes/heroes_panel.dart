import 'package:flutter/material.dart';

class HeroesPanel {
  bool isExpanded;
  final String header;
  final Widget body;
  final Icon iconpic;
  final String title;
  final List heroes;

  HeroesPanel({
    this.isExpanded,
    this.header,
    this.body,
    this.iconpic,
    this.title,
    this.heroes,
  });

}