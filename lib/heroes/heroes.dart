import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';
import 'hero.dart' as Bandit;
import '../items/item.dart';
import '../resources/resources.dart';
import '../utils/helpers.dart';

class Heroes with ChangeNotifier {
  List<Bandit.Hero> _heroes;
  final String _token;
  final String _userId;
  final int world;

  Heroes(this._token, this._userId, this._heroes, this.world);

  List<Bandit.Hero> get heroes  {
    return _heroes;
  }

  List<Bandit.Hero> get party  {
    return _heroes.where((hero) => !hero.isBenched).toList();
  }

  Future<List<Bandit.Hero>> fetchHeroes() async {
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world.toString()}/users/$_userId/heroes.json?auth=$_token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      List<Bandit.Hero> heroes = [];
      extractedData.forEach((heroId, hero) {
        heroes.add(buildHero(hero));
      });
      _heroes = heroes;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<List<Bandit.Hero>> getPartyById(String id) async {
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world.toString()}/users/$id/heroes.json?auth=$_token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      List<Bandit.Hero> heroes = [];
      extractedData.forEach((heroId, hero) {
        if (hero['isBenched'] == false) {
          heroes.add(buildHero(hero));
        }
      });
      return heroes;
    } catch (error) {
      throw error;
    }
  }

  Bandit.Hero findById(id) {
    return _heroes.firstWhere((hero) => hero.id == id, orElse: () => null);
  }

  Future<void> saveHero(Bandit.Hero hero) async {
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world.toString()}/users/$_userId/heroes/${hero.id}.json?auth=$_token');
    try {
      await http.put(
        url,
        body: json.encode({
          'heroClass': hero.heroClass,
          'level': hero.level,
          'xp': hero.xp,
          'id': hero.id,
          'items': hero.items,
          'isBenched': hero.isBenched,
          'lastQuest': hero.lastQuest.toIso8601String(),
          'questsCompleted': hero.questsCompleted,
          'stats': {
            'strength': hero.strength,
            'dexterity': hero.dexterity,
            'intelligence': hero.intelligence,
            'vitality': hero.vitality,
          },
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  Future<void> unequipItem(Item item, String heroId) async {
    Bandit.Hero heroFound = findById(heroId);
    heroFound.unequipItem(item);
    await saveHero(heroFound);
    notifyListeners();
  }

  bool alreadyOwnsClass(hero) {
    var found;
    if (_heroes != []) {
      found = _heroes.firstWhere((ownedHero) => ownedHero.heroClass == hero.heroClass, orElse: () => null);
    }
    return found != null;
  }

  Future<void> switchBenched(Bandit.Hero hero) async {
    Bandit.Hero heroFound = findById(hero.id);
    heroFound.isBenched = !heroFound.isBenched;
    await saveHero(heroFound);
    notifyListeners();
  }

  Future<void> startQuest(Bandit.Hero hero, String timestamp) async {
    Bandit.Hero heroFound = findById(hero.id);
    heroFound.isBenched = true;
    heroFound.lastQuest = DateTime.parse(timestamp);
    await saveHero(heroFound);
    notifyListeners();
  }

  Future<void> finishQuest(Bandit.Hero hero, int questId, int xp, String result) async {
    Bandit.Hero heroFound = findById(hero.id);
    if (result == 'success') {
      heroFound.questsCompleted.add(questId);
      heroFound.addExperience(xp);
      await saveHero(heroFound);
      notifyListeners();
    }
  }

  Future<Bandit.Hero> recruitHero(context, Bandit.Hero hero, int cost) async {
    try{
      await saveHero(hero);
      _heroes.add(hero);
      await Provider.of<Resources>(context, listen: false).setGold(-cost);
      notifyListeners();
      return hero;
    } catch (error) {
      throw error;
    }
  }

  List<Bandit.Hero> getAvailableHeroes () {
    return [
      Bandit.Hero(
        heroClass: 'warlord',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 15,
          'dexterity': 11,
          'intelligence': 10,
          'vitality': 13,
        },
      ),
      Bandit.Hero(
        heroClass: 'shieldbearer',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 14,
          'dexterity': 10,
          'intelligence': 10,
          'vitality': 14,
        },
      ),
      Bandit.Hero(
        heroClass: 'paladin',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 14,
          'dexterity': 12,
          'intelligence': 11,
          'vitality': 14,
        },
      ),
      Bandit.Hero(
        heroClass: 'huntress',
        level: 1,
        xp: 0,
        isBenched: true,
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        id: Uuid().v1(),
        items: [],
        questsCompleted: [],
        stats: {
          'strength': 11,
          'dexterity': 14,
          'intelligence': 11,
          'vitality': 12,
        },
      ),
      Bandit.Hero(
        heroClass: 'sniper',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 11,
          'dexterity': 15,
          'intelligence': 11,
          'vitality': 10,
        },

      ),
      Bandit.Hero(
        heroClass: 'assassin',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 12,
          'dexterity': 15,
          'intelligence': 10,
          'vitality': 10,
        },
      ),
      Bandit.Hero(
        heroClass: 'bounty_hunter',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 11,
          'dexterity': 13,
          'intelligence': 10,
          'vitality': 13,
        },
      ),
      Bandit.Hero(
        heroClass: 'priest',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 12,
          'dexterity': 10,
          'intelligence': 14,
          'vitality': 15,
        },
      ),
      Bandit.Hero(
        heroClass: 'sorceress',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 10,
          'dexterity': 11,
          'intelligence': 15,
          'vitality': 10,
        },
      ),
      Bandit.Hero(
        heroClass: 'warlock',
        level: 1,
        xp: 0,
        isBenched: true,
        id: Uuid().v1(),
        items: [],
        lastQuest: DateTime.now().subtract(Duration(days: 91)),
        questsCompleted: [],
        stats: {
          'strength': 10,
          'dexterity': 12,
          'intelligence': 14,
          'vitality': 11,
        },
      )
    ];
  }
}