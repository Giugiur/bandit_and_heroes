import 'package:bandit_and_heroes/heroes/heroes_screen.dart';
import 'package:bandit_and_heroes/resources/gold_cost.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../items/item.dart';
import '../items/item_tile.dart';
import '../items/items.dart';
import 'hero.dart' as Bandit;
import 'heroes.dart';
import 'hero_level.dart';
import 'heroes_screen.dart';
import 'stat_indicator.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources.dart';
import '../resources/resources_bar.dart';
import '../utils/constants.dart';
import '../utils/extensions.dart';
import '../widgets/announcer.dart';
import '../widgets/loading_elevated_button.dart';

class HeroDetailScreen extends StatefulWidget {
  static const routeName = '/hero_detail';

  @override
  _HeroDetailScreenState createState() => _HeroDetailScreenState();
}

class _HeroDetailScreenState extends State<HeroDetailScreen> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final Bandit.Hero hero = ModalRoute.of(context).settings.arguments;
    final List<Item> itemList = Provider.of<Items>(context, listen: false).getHeroItems(hero);
    final gold = Provider.of<Resources>(context).gold;
    final heroes = Provider.of<Heroes>(context).heroes;
    final bool heroAlreadyOwned = Provider.of<Heroes>(context).alreadyOwnsClass(hero);
    var _isDisabled = gold < heroes.length*RECRUIT_COST_PER_HERO ? true : false,
    announcer = Announcer(context);

    void _recruitHero() {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Heroes>(context, listen:false).recruitHero(context, hero, heroes.length*RECRUIT_COST_PER_HERO)
        .then((response) {
          Navigator.pushReplacementNamed(context, HeroesScreen.routeName);
          announcer.showSuccessMessage('The ${hero.heroClass.capitalize().removeUnderscore()} is now part of your team.',
              'Send them on a quest to earn rewards!');
          }).catchError((error) {
            announcer.showErrorMessage('Oops! There was an error while trying to recruit the hero', error.toString());
          }).whenComplete(() {
            setState(() {
              _isLoading = false;
            });
          });
    }

    return Scaffold(
      appBar: BanditBar(),
      body: SingleChildScrollView(
        child: Container(
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: [
              Container(
                height: 400,
                width: double.infinity,
                child: Hero(
                  tag: hero.heroClass,
                  child: Image.asset(
                    hero.assetImage,
                    fit: BoxFit.cover
                  ),
                ),
              ),
              Row(
                children: [
                  StatIndicator('Strength', hero.strength + hero.getItemsStatsBonus(context, 'strength'), hero.mainStat == 'strength', hero.statGain['strength']),
                  StatIndicator('Dexterity', hero.dexterity + hero.getItemsStatsBonus(context, 'dexterity'), hero.mainStat == 'dexterity', hero.statGain['dexterity']),
                  StatIndicator('Intelligence', hero.intelligence + hero.getItemsStatsBonus(context, 'intelligence'), hero.mainStat == 'intelligence', hero.statGain['intelligence']),
                  StatIndicator('Vitality', hero.vitality + hero.getItemsStatsBonus(context, 'vitality'), hero.mainStat == 'vitality', hero.statGain['vitality']),
                ],
              ),
              if(heroAlreadyOwned)
                SizedBox(height: 20,),
              if(heroAlreadyOwned)
                HeroLevel(hero.level, hero.xp),
              SizedBox(height: 20,),
              Container(
                alignment: Alignment.topCenter,
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'Weapons Allowed: ',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      TextSpan(
                        text: hero.weapons,
                        style: Theme.of(context).textTheme.overline,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Container(
                padding: const EdgeInsets.only(right: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 25,
                      child: Icon(
                        hero.passiveAbility['icon'],
                        size: 50,
                      ),
                    ),
                    Expanded(
                      flex: 75,
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: hero.passiveAbility['title'],
                              style: Theme.of(context).textTheme.overline,
                            ),
                            TextSpan(
                              text: hero.passiveAbility['description'],
                              style: Theme.of(context).textTheme.bodyText1,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20,),
              if (itemList.isNotEmpty)
                Text(
                  'Items equipped',
                  style: Theme.of(context).textTheme.headline3,
                  textAlign: TextAlign.center,
                ),
              if (itemList.isNotEmpty)
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (ctx, index) => ItemTile(itemList[index], false),
                    itemCount: itemList.length,
                  ),
                ),
              if (heroAlreadyOwned && itemList.isEmpty)
                Text(
                  'This hero has no items equipped.',
                  style: Theme.of(context).textTheme.headline3,
                  textAlign: TextAlign.center,
                ),
              SizedBox(height: 20,),
              if (hero.getSpellsText(context).isNotEmpty)
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                  child: Column(
                    children: [
                      Text(
                        'Spells',
                        style: Theme.of(context).textTheme.headline3,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 10,),
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (ctx, index) => hero.getSpellsText(context)[index],
                        itemCount: hero.getSpellsText(context).length,
                      ),
                    ]
                  ),
                ),
              if (hero.getSpellsText(context).isNotEmpty)
                SizedBox(height: 20,),
              if (!heroAlreadyOwned)
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                  child: Column(
                    children: [
                      LoadingElevatedButton(
                          'Recruit ${hero.heroClass.capitalize().removeUnderscore()}',
                          _isLoading,
                          _isDisabled,
                          _recruitHero
                      ),
                      Center(
                        child: Container(
                            width: 60,
                            child: GoldCost(heroes.length*RECRUIT_COST_PER_HERO)
                        ),
                      ),
                      SizedBox(height: 20,),
                    ],
                  ),
                ),
            ]
          ),
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
