import 'package:bandit_and_heroes/utils/custom_icons.dart';
import 'package:flutter/material.dart';

class StatIndicator extends StatelessWidget {
  final String name;
  final int amount;
  final bool isMainStat;
  final int statGain;
  StatIndicator(this.name, this.amount, this.isMainStat, this.statGain);

  @override
  Widget build(BuildContext context) {
    Widget icon = name == 'Strength' ? Icon(CustomIcons.muscle_up)
      : name == 'Intelligence' ? Icon(CustomIcons.aware)
      : name == 'Dexterity' ? Icon(CustomIcons.player_dodge)
      : Icon(CustomIcons.health);
    return Expanded(
      child: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          color: isMainStat ? Theme.of(context).primaryColor : null,
          child: Row(
            children: [
              icon,
              SizedBox(width: 5),
              Text(
                 amount.toString(),
                 style: Theme.of(context).textTheme.bodyText2,
              ),
              Text(
                ' (+$statGain)',
                style: TextStyle(
                  fontSize: 12
                ),
              )
            ],
          )
        ),
      ),
    );
  }
}
