import 'package:bandit_and_heroes/utils/custom_icons.dart';
import 'package:flutter/material.dart';
import 'hero.dart' as Bandit;
import 'hero_detail_screen.dart';
import '../utils/extensions.dart';

class RecruitHeroTile extends StatelessWidget {
  final Bandit.Hero hero;

  RecruitHeroTile(this.hero);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).pushNamed(
        HeroDetailScreen.routeName,
        arguments: hero,
      ),
      child: Container(
        height: 140,
        child: Row(
          children: [
            Expanded(
              flex: 35,
              child: Container(
                child: ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Colors.black, Colors.transparent],
                    ).createShader(Rect.fromLTRB(75, 0, rect.width, rect.height));
                  },
                  blendMode: BlendMode.dstIn,
                  child: Hero(
                    tag: hero.heroClass,
                    child: Image.asset(
                      hero.assetImage,
                    ),
                  ),
                )
              ),
            ),
            Expanded(
              flex: 65,
              child: ListTile(
                title: Text(hero.heroClass.capitalize().removeUnderscore()),
                subtitle: Text(hero.heroDescription),
                trailing: Icon(Icons.chevron_right),
              )
            ),
          ],
        ),
      ),
    );
  }
}

