import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'hero.dart' as Bandit;
import 'heroes.dart';
import '../items/item.dart';
import '../items/items.dart';
import '../quests/quests.dart';
import '../utils/extensions.dart';


class HeroSelector extends StatefulWidget {
  final String screen;
  Item item;

  HeroSelector({
    this.screen,
    this.item,
  });

  @override
  _HeroSelectorState createState() => _HeroSelectorState();
}

class _HeroSelectorState extends State<HeroSelector> {

  List<DropdownMenuItem<Bandit.Hero>> _dropDownMenuItems = [];
  var _isInit = false;

  List<DropdownMenuItem<Bandit.Hero>> _buildDropDownMenuItems(List<Bandit.Hero> heroes) {
    List<DropdownMenuItem<Bandit.Hero>> items = [];
    for (Bandit.Hero hero in heroes) {
      items.add(
       DropdownMenuItem(
         child: Container(
           width: 250,
           child: Row(
             children: [
               CircleAvatar(
                 backgroundImage: AssetImage(hero.assetImage),
               ),
               SizedBox(width: 10,),
               Text(
                 '${hero.heroClass} - Lvl ${hero.level}'.capitalize().removeUnderscore(),
                 style: Theme.of(context).textTheme.bodyText1,
               ),
             ]
           ),
         ),
         value: hero,
       ),
     );
    }
    return items;
  }

  void didChangeDependencies() {
    if (!_isInit) {
      List<Bandit.Hero> heroes = Provider.of<Heroes>(context).heroes;
      _dropDownMenuItems = _buildDropDownMenuItems(heroes);
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  DropdownMenuItem _pickFromDropdown(String heroClass) {
    return _dropDownMenuItems.firstWhere((item) {
      return item.value.heroClass == heroClass;
    }, orElse: () => null);
  }

  @override
  Widget build(BuildContext context) {
    Bandit.Hero selHero = widget.screen.toLowerCase() == 'quests' ? Provider.of<Quests>(context).selectedHero : Provider.of<Items>(context).heroToEquip;
    Bandit.Hero _selectedHero = selHero != null ? _pickFromDropdown(selHero.heroClass).value : null;
    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).canvasColor,
          border: Border.all(
              width: 0.0,
              style: BorderStyle.none
          ),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButtonFormField (
            value: _selectedHero,
            items: _dropDownMenuItems,
            hint: Text(
              'Select a hero',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            onChanged: (value) {
              setState(() {
                _selectedHero = value;
                widget.screen.toLowerCase() == 'quests' ? Provider.of<Quests>(context, listen: false).setHero(value) : Provider.of<Items>(context, listen: false).setHeroToEquip(value);
              });
            },
          ),
        ),
      ),
    );
  }
}
