import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'hero.dart' as Bandit;
import 'heroes.dart';
import 'recruit_hero_tile.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';
import '../widgets/empty_state.dart';

class RecruitHeroScreen extends StatelessWidget {
  static const routeName = '/recruit_hero';

  @override
  Widget build(BuildContext context) {
    var _isLoading = false;
    List<Bandit.Hero> _availableHeroes = Provider.of<Heroes>(context, listen: false).getAvailableHeroes();
    _availableHeroes.removeWhere((hero) => Provider.of<Heroes>(context, listen: false).alreadyOwnsClass(hero));

    return Scaffold(
      appBar: BanditBar(),
      body: _isLoading == true
      ? Center(child: CircularProgressIndicator(),)
      : _availableHeroes.length == 0 ? EmptyState('There are no more heroes left to recruit.')
      : SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(bottom: 20,),
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (ctx, index) {
              return RecruitHeroTile(_availableHeroes[index]);
            },
            itemCount: _availableHeroes.length,
          ),
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
