import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'hero.dart' as Bandit;
import 'heroes.dart';
import 'hero_detail_screen.dart';
import '../utils/extensions.dart';

class HeroTile extends StatelessWidget {
  final Bandit.Hero hero;
  final bool compact;
  final bool reversed;

  HeroTile(this.hero, this.compact, this.reversed);

  @override
  Widget build(BuildContext context) {
    var trailing;
    if (reversed && compact) {
      trailing = CircleAvatar(
        backgroundImage: AssetImage(hero.assetImage),
      );
    } else {
      if (compact) {
        trailing = null;
      } else {
        trailing = PopupMenuButton(
          color: Theme.of(context).canvasColor,
          initialValue: 2,
          child: IconButton(
            icon: Icon(Icons.more_vert),
          ),
          onSelected: (result) {
            if (result == 0) {
              Navigator.of(context).pushNamed(
                HeroDetailScreen.routeName,
                arguments: hero,
              );
            }
            if (result == 1) {
              Provider.of<Heroes>(context, listen: false).switchBenched(hero);
            }
          },
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                value: 0,
                child: Text('See Hero'),
              ),
              PopupMenuItem(
                value: 1,
                child: hero.isBenched == true ? Text('Move to Party') : Text('Move to Bench'),
              )
            ];
          },
        );
      }
    }
    return Material(
      color: Colors.transparent,
      child: Container(
        height: 50,
        width: 500,
        child: Center(
          child: ListTile(
            leading: reversed && compact ? null : CircleAvatar(
              backgroundImage: AssetImage(hero.assetImage),
            ),
            title: compact ? Text('Lvl ${hero.level}' ) :
            Text(
              '${hero.heroClass} - Lvl ${hero.level}'.capitalize().removeUnderscore(),
              style: Theme.of(context).textTheme.bodyText1,
            ),
            trailing: trailing,
          ),
        ),
      ),
    );
  }
}