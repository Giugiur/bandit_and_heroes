import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'hero.dart' as Bandit;
import 'hero_tile.dart';

class HeroDraggable extends StatelessWidget {
  final Bandit.Hero hero;

  HeroDraggable(this.hero);

  @override
  Widget build(BuildContext context) {
    return Draggable(
      child: HeroTile(hero, false, false),
       feedback: Container(
        child: Opacity(
          opacity: 0.5,
          child: CircleAvatar(
            backgroundImage: AssetImage(hero.assetImage),
          ),
        ),
      ),
      childWhenDragging: Container(
        decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).accentColor),
        ),
        child: Opacity(
          opacity: 0.80,
          child: HeroTile(hero, false, false),
        )
      ),
      data: hero.id,
    );
  }
}
