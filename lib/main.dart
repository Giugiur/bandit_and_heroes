import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'auth/auth.dart';
import 'auth/auth_screen.dart';
import 'account/account_screen.dart';
import 'account/premium_subscription_screen.dart';
import 'battle/attack_screen.dart';
import 'battle/battle_detail_screen.dart';
import 'battle/battle_detail_screen2.dart';
import 'battle/battles.dart';
import 'battle/battles_history_screen.dart';
import 'config/ad_state.dart';
import 'config/report_bug_screen.dart';
import 'config/theme.dart';
import 'heroes/hero_detail_screen.dart';
import 'heroes/heroes.dart';
import 'heroes/heroes_screen.dart';
import 'heroes/recruit_hero_screen.dart';
import 'items/items.dart';
import 'items/items_screen.dart';
import 'items/market_screen.dart';
import 'leaderboards/leaderboards_screen.dart';
import 'news/news.dart';
import 'overview/overview_screen.dart';
import 'quests/quest_result_screen.dart';
import 'quests/quests.dart';
import 'quests/quests_screen.dart';
import 'resources/resources.dart';
import 'subscription/purchase_api.dart';
import 'subscription/subscriptions.dart';
import 'world/world.dart';
import 'world/world_selector.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  final initFuture = MobileAds.instance.initialize();
  final adState = AdState(initFuture);
  await PurchaseApi.init();
  runApp(
    Provider.value(
      value: adState,
      builder: (context, child) => MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Auth()),
        ChangeNotifierProvider(create: (_) => ThemeSwitcher()),
        ChangeNotifierProvider(create: (_) => Subscription()),
        ChangeNotifierProxyProvider<Auth, World>(
          update: (ctx, auth, _) => World(
            auth.token,
            auth.userId,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, News>(
          update: (ctx, auth, _) => News(
            auth.token,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, Quests>(
          update: (ctx, auth, _) => Quests(
            auth.token
          ),
        ),
        ChangeNotifierProxyProvider2<Auth, World, Resources>(
          update: (ctx, auth, world, _) => Resources(
            auth.token,
            auth.userId,
            world.resources != null ? world.resources['gold'] : 50,
            world.resources != null ? world.resources['sapphires'] : 5,
            DateTime.parse(world.resources != null ? world.resources['lastQuest'] : DateTime.now().toIso8601String()),
            world.questCooldown,
            world.id
          ),
        ),
        ChangeNotifierProxyProvider2<Auth, World, Heroes>(
          update: (ctx, auth, world, _) => Heroes(
            auth.token,
            auth.userId,
            world != null ? world.heroes : [],
            world.id
          ),
        ),
        ChangeNotifierProxyProvider2<Auth, World, Items>(
          update: (ctx, auth, world, _) => Items(
              auth.token,
              auth.userId,
              world != null ? world.items : [],
              world != null ? world.marketItems : [],
              world.id
          ),
        ),
        ChangeNotifierProxyProvider2<Auth, World, Battles>(
          update: (ctx, auth, world, _) => Battles(
              auth.token,
              auth.userId,
              world.id
          ),
        ),
      ],
      child: Consumer2<Auth, ThemeSwitcher>(
        builder: (ctx, auth, theme, _) => MaterialApp(
          title: 'Bandits and Heroes',
          theme: theme.darkTheme,
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('en', 'US'),
            const Locale('es'),
          ],
          home: I18n(
            child: auth.isAuth ? WorldSelectorScreen() : FutureBuilder(
              future: auth.tryAutoLogin(ctx),
              builder: (ctx, authResultSnapshot) =>
                authResultSnapshot.connectionState ==
                ConnectionState.waiting
                ? Center(child: CircularProgressIndicator(),)
                : AuthScreen(),
            ),
          ),
          routes: {
            AccountScreen.routeName: (ctx) => AccountScreen(),
            AttackScreen.routeName: (ctx) => AttackScreen(),
            AuthScreen.routeName: (ctx) => AuthScreen(),
            BattlesHistoryScreen.routeName: (ctx) => BattlesHistoryScreen(),
            BattleDetailScreen.routeName: (ctx) => BattleDetailScreen(),
            BattleDetailScreen2.routeName: (ctx) => BattleDetailScreen2(),
            HeroDetailScreen.routeName: (ctx) => HeroDetailScreen(),
            HeroesScreen.routeName: (ctx) => HeroesScreen(),
            ItemsScreen.routeName: (ctx) => ItemsScreen(),
            LeaderboardsScreen.routeName: (ctx) => LeaderboardsScreen(),
            MarketScreen.routeName: (ctx) => MarketScreen(),
            OverviewScreen.routeName: (ctx) => OverviewScreen(),
            PremiumSubscriptionScreen.routeName: (ctx) => PremiumSubscriptionScreen(),
            QuestResultScreen.routeName: (ctx) => QuestResultScreen(),
            QuestsScreen.routeName: (ctx) => QuestsScreen(),
            RecruitHeroScreen.routeName: (ctx) => RecruitHeroScreen(),
            ReportBugScreen.routeName: (ctx) => ReportBugScreen(),
            WorldSelectorScreen.routeName: (ctx) => WorldSelectorScreen(),
          },
        ),
      )
    );
  }
}