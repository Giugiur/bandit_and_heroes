import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../auth/auth.dart';

class LeaderboardsTable extends StatelessWidget {
  List<String> columnsLabel;
  List<String> rowsLabel;
  List<Map<String, String>> rowsData;

  LeaderboardsTable(
    this.columnsLabel,
    this.rowsLabel,
    this.rowsData,
  );

  @override
  Widget build(BuildContext context) {
    List<DataColumn> columns = columnsLabel.map((e) => DataColumn(label: Text(e, style: TextStyle(color: Theme.of(context).accentColor),))).toList();
    String username = Provider.of<Auth>(context, listen: false).username.toLowerCase();
    return Container(
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
        child: DataTable(
          columns: columns,
          rows: rowsData.map(
            ((element) => DataRow(
              cells: [
                DataCell(Text(element['position'], style: TextStyle(color: element['username'].toLowerCase() == username ? Theme.of(context).accentColor : Theme.of(context).textTheme.bodyText1.color))),
                DataCell(Text(element['username'], style: TextStyle(color: element['username'].toLowerCase() == username ? Theme.of(context).accentColor : Theme.of(context).textTheme.bodyText1.color))),
                DataCell(Text(element['points'], style: TextStyle(color: element['username'].toLowerCase() == username ? Theme.of(context).accentColor : Theme.of(context).textTheme.bodyText1.color))),
              ],
            )),
          ).toList(),
        ),
      ),
    );
  }
}
