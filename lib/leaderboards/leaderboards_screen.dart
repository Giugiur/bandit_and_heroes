import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'leaderboards_table.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';
import '../world/world.dart';

class LeaderboardsScreen extends StatefulWidget {
  static const routeName = '/leaderboards';
  @override
  _LeaderboardsScreenState createState() => _LeaderboardsScreenState();
}

class _LeaderboardsScreenState extends State<LeaderboardsScreen> {

  bool _isLoading = false;
  List<Map<String, String>> rows = [];

  void didChangeDependencies() {
    setState(() {
      _isLoading = true;
    });
    Provider.of<World>(context, listen: false).getLeaderboards(context).then((response) {
      rows = response;
    }).whenComplete(() {
      setState(() {
        _isLoading = false;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    List<String> columnsLabel = ['#', 'Username', 'Points'];
    List<String> rowsLabel = ['position', 'username', 'points'];
    return Scaffold(
      appBar: BanditBar(),
      bottomNavigationBar: ResourcesBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator()) :
        LeaderboardsTable(columnsLabel, rowsLabel, rows)
    );
  }
}
