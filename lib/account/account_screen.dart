import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'account_screen.i18n.dart';
import 'premium_subscription_screen.dart';
import '../auth/auth.dart';
import '../bandit_bar/bandit_bar.dart';
import '../config/report_bug_screen.dart';
import '../utils/helpers.dart';
import '../widgets/announcer.dart';
import '../world/world.dart';
import '../world/world_selector.dart';

class AccountScreen extends StatefulWidget {
  static const routeName = '/account';

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  int _battlesWon;
  int _battlesLost;
  int _points;
  int _winrate;
  String _world;
  bool _isLoading = false;

  void _deleteAccount () {
    Provider.of<Auth>(context, listen: false).deleteAccount().whenComplete(() {
      Announcer(context).showInfoMessage('We are sad to see you go', 'Your data has been erased. No one can access it now, not even you.');
      cleanLogout(context);
    });
  }

  void didChangeDependencies() async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<World>(context, listen: false).getUserData();
    setState(() {
      _points = Provider.of<World>(context, listen: false).points;
      _battlesWon = Provider.of<World>(context, listen: false).battlesWon;
      _battlesLost = Provider.of<World>(context, listen: false).battlesLost;
      _winrate = _battlesWon != 0 ? ((_battlesWon / (_battlesWon + _battlesLost))*100).floor() : 0;
      _world = Provider.of<World>(context, listen: false).world['name'];
      _isLoading = false;
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var announcer = Announcer(context);
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: BanditBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 0),
          height: deviceSize.height,
          child: _isLoading ? Center(child: CircularProgressIndicator(),) :
          Column(
            children: [
              Text(
                'Your stats on $_world',
                style: Theme.of(context).textTheme.headline3,
                textAlign: TextAlign.start,
              ),
              SizedBox(height: 10,),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Battle Points: ',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                                TextSpan(
                                  text: _points.toString(),
                                  style: Theme.of(context).textTheme.overline,
                                ),
                              ],
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Win Rate: ',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                                TextSpan(
                                  text: '$_winrate%',
                                  style: Theme.of(context).textTheme.overline,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Battles Won: ',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                                TextSpan(
                                  text: _battlesWon.toString(),
                                  style: Theme.of(context).textTheme.overline,
                                ),
                              ],
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Battles Lost: ',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                                TextSpan(
                                  text: _battlesLost.toString(),
                                  style: Theme.of(context).textTheme.overline,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: ListTile(
                  leading: const Icon(Icons.public),
                  title: Text('Switch Worlds'),
                  subtitle: Text('You can login or signup for one of the other available worlds.'),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, WorldSelectorScreen.routeName);
                  },
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: ListTile(
                  leading: const Icon(Icons.monetization_on),
                  title: Text('Premium Subscription'),
                  subtitle: Text('See how you can get some non-P2W perks for a small paid subscription.'),
                  onTap: () {
                    Navigator.of(context).pushNamed(PremiumSubscriptionScreen.routeName);
                  },
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: ListTile(
                  leading: const Icon(Icons.bug_report),
                  title: Text('Report a Bug'),
                  subtitle: Text('Help us improve the game! If you spotted a bug, you can report it here.'),
                  onTap: () {
                    Navigator.of(context).pushNamed(ReportBugScreen.routeName);
                  },
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: ListTile(
                  leading: const Icon(Icons.restore_from_trash_sharp),
                  title: Text('Delete Account'),
                  subtitle: Text('Deletes your account with all its data in all the worlds you signed up.'),
                  onTap: () {
                    announcer.showActionMessage(
                        'Are you sure?',
                        'Deleting your account means that you won\'t be able to recover your data.',
                        'Delete',
                        _deleteAccount
                    );
                  },
                ),
              ),
              Divider(),
            ],
          ),
        ),
      ),
    );
  }
}
