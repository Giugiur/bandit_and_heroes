import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

  static var _t = Translations('en_us') + {
    'en_us': 'Switch Worlds',
    'es': 'Cambia de mundo',
  } + {
    'en_us': 'Switch Themes',
    'es': 'Cambia de tema',
  } + {
    'en_us': 'Change the theme from dark to light or light to dark.',
    'es': 'Cambia de tema de oscuro a claro o de claro a oscuro.'
  } + {
    'en_us': 'You can login or signup for one of the other available worlds.',
    'es': 'Puedes crear una cuenta o ingresar en otro de los mundos disponibles.'
  }
  ;
  String get i18n => localize(this, _t);
}