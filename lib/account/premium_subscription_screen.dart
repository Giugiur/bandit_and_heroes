import 'package:bandit_and_heroes/widgets/empty_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:purchases_flutter/object_wrappers.dart';
import '../subscription/purchase_api.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';
import '../subscription/subscriptions.dart';
import '../widgets/benefit_description.dart';
import '../widgets/loading_elevated_button.dart';


class PremiumSubscriptionScreen extends StatefulWidget {
  static const routeName = '/premium-subscription';

  @override
  _PremiumSubscriptionScreenState createState() => _PremiumSubscriptionScreenState();
}

class _PremiumSubscriptionScreenState extends State<PremiumSubscriptionScreen> {
  bool _isLoading = false, isPremium = false;
  List<Offering> offerings = [];
  Package yearlySubscription, monthlySubscription;

  Future<void> _subscribe(Package package) async {
    await PurchaseApi.purchasePackage(package);
  }

  void didChangeDependencies() async {
    setState(() {
      _isLoading = true;
    });
    offerings = await PurchaseApi.fetchOffers();
    monthlySubscription = offerings.first.availablePackages[0];
    yearlySubscription = offerings.first.availablePackages[1];
    Provider.of<Subscription>(context, listen: false).updatePurchaseStatus();
    isPremium = Provider.of<Subscription>(context, listen: false).isPremium;
    setState(() {
      _isLoading = false;
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: BanditBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator(),) :
      offerings.isEmpty ? Center(child: EmptyState('No subscriptions options to show. Maybe you need to log in to the store.')) :
      isPremium ? Center(child: EmptyState('Nothing to do here. You are already subscribed, enjoy!')) :
      Container(
        height: deviceSize.height,
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              alignment: Alignment.topCenter,
              child: Column(
                children: [
                  Text(
                    'Go Premium',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text(
                      'No commitment. Cancel anytime.'
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  BenefitDescription('No pesky and disruptive ads.'),
                  BenefitDescription('Queue heroes for quests.'),
                  BenefitDescription('See more options on the market.'),
                  BenefitDescription('See the defender\'s points.'),
                  BenefitDescription('And more benefits coming soon!'),
                ],
              ),
            ),
            Container(
              child: isPremium ? Container(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                alignment: Alignment.topCenter,
                child: Text(
                  'You are already subscribed. If you want to cancel your subscription, you can do it via the Store of your OS.',
                  style: Theme.of(context).textTheme.headline4,
                ),
              ) : ListView(
                shrinkWrap: true,
                children: [
                  LoadingElevatedButton('Subscribe Yearly (${yearlySubscription.product.priceString} ${yearlySubscription.product.currencyCode}/year)', false, false, () => _subscribe(yearlySubscription)),
                  SizedBox(height: 5,),
                  TextButton(
                    child: Text(
                      'Subscribe Monthly (${monthlySubscription.product.priceString} ${monthlySubscription.product.currencyCode}/month)',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    onPressed: () async {
                      setState(() {
                        _isLoading = true;
                      });
                      await _subscribe(monthlySubscription);
                      setState(() {
                        _isLoading = false;
                      });
                    },
                  )
                ],
              )
            )
          ],
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
