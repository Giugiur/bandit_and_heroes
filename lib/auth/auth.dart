import 'dart:convert';
import 'dart:async';
import 'package:bandit_and_heroes/widgets/announcer.dart';
import 'package:bandit_and_heroes/world/world.dart';
import 'package:bandit_and_heroes/world/world_selector.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import '../utils/keys.dart';
import '../utils/http_exception.dart';

// Handles authentication, session data and autologin/autologout
class Auth with ChangeNotifier {
  String _token;
  String _refreshToken;
  DateTime _expiryDate;
  String _userId;
  Timer _authTimer;
  String _username;

  bool get isAuth {
    return token != null;
  }

  String get token {
    if (_expiryDate != null && _expiryDate.isAfter(DateTime.now()) && _token != null) {
      return _token;
    }
    return null;
  }

  Future<void> refreshSession() async {
    final url =
        Uri.parse('https://securetoken.googleapis.com/v1/token?key=$FIREBASE_API_KEY');

    try {
      final response = await http.post(
        url,
        body: json.encode({
          'grant_type': 'refresh_token',
          'refresh_token': _refreshToken,
        }),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      _token = responseData['id_token'];
      _refreshToken = responseData['refresh_token'];
      _userId = responseData['user_id'];
      _expiryDate = DateTime.now()
          .add(Duration(seconds: int.parse(responseData['expires_in'])));
      notifyListeners();
      _saveSessionData();
    } catch (error) {
      throw error;
    }
  }

  String get userId {
    return _userId;
  }

  String get username  {
    return _username;
  }

  Future<void> createUser (String username) async {
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/users/$_userId.json?auth=$_token');
    try {
      final response = await http.put(
        url,
        body: json.encode({
          'username': username,
          'creationDate': DateTime.now().toIso8601String(),
          'history': {},
        }),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      } else {
        _username = username;
        notifyListeners();
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> sendReport (String text) async {
    final String id = Uuid().v1();
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/bugs/$id.json?auth=$_token');
    try {
      await http.put(
        url,
        body: json.encode({
          'reporter': _userId,
          'username': username,
          'description': text,
          'date': DateTime.now().toIso8601String(),
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  void _saveSessionData() async {
    final prefs = await SharedPreferences.getInstance();
    final sessionData = json.encode({
      'token': _token,
      'refreshToken': _refreshToken,
      'userId': _userId,
      'expiryDate': _expiryDate.toIso8601String(),
      'username': _username,
    }
    );
    prefs.setString('sessionData', sessionData);
  }

  Future<void> _authenticate(context, String email, String password, String operation) async {
    final url = Uri.parse('https://identitytoolkit.googleapis.com/v1/accounts:$operation?key=$FIREBASE_API_KEY');
    try {
      final response = await http.post(
        url,
        body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _refreshToken = responseData['refreshToken'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: int.parse(
            responseData['expiresIn'],
          ),
        ),
      );
      _autoLogout(context);
      notifyListeners();
      _saveSessionData();
    } catch (error) {
      throw error;
    }
  }

  Future<void> signup(context, String email, String password) async {
    return _authenticate(context, email, password, 'signUp');
  }

  Future<void> login(context, String email, String password) async {
    return _authenticate(context, email, password, 'signInWithPassword').then((response) async {
      final url2 = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/users/$_userId.json?auth=$_token');
      final response2 = await http.get(url2);
      final extractedData = json.decode(response2.body);
      _username = extractedData['username'];
      notifyListeners();
      _saveSessionData();
    });
  }

  Future<void> deleteAccount() async {
    final url = Uri.parse('https://identitytoolkit.googleapis.com/v1/accounts:delete?key=$FIREBASE_API_KEY');
    try {
      final response = await http.post(
        url,
        body: json.encode({
          'idToken': _token,
        }),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      final url2 = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/users/$_userId.json?auth=$_token');
      await http.delete(url2);
    }
    catch (error) {
      throw error;
    }
  }

  Future<bool> tryAutoLogin(context) async {
    final sessionPrefs = await SharedPreferences.getInstance();
    if (!sessionPrefs.containsKey('sessionData')) {
      return false;
    }
    final sessionData = json.decode(sessionPrefs.getString('sessionData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(sessionData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = sessionData['token'];
    _refreshToken = sessionData['refreshToken'];
    _userId = sessionData['userId'];
    _expiryDate = expiryDate;
    _username = sessionData['username'];
    _autoLogout(context);
    notifyListeners();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _refreshToken = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('sessionData');
  }

  void _autoLogout(context) {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry-60), ()  {
      refreshSession();
    });
  }
}
