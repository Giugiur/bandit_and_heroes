import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'auth.dart';
import '../utils/http_exception.dart';
import '../utils/validators/validators.dart';
import '../widgets/announcer.dart';
import '../widgets/loading_elevated_button.dart';
import '../widgets/version_display.dart';

enum AuthMode { Signup, Login }

//The screen for signup/login
class AuthScreen extends StatelessWidget {
  static const routeName = '/auth';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery
        .of(context)
        .size;

    return Scaffold(
        body: SafeArea(
          child: Container(
              height: deviceSize.height,
              width: deviceSize.width,
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 30),
              child: AuthForm(),
          ),
        ),
      bottomNavigationBar: Container(
        height: 50,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: VersionDisplay(),
        ),
      ),
    );
  }
}

class AuthForm extends StatefulWidget {
  const AuthForm({
    Key key,
  }) : super(key: key);

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  Map<String, String> _authData = {
    'email': '',
    'password': '',
  };
  var _isLoading = false;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _emailAddressController = TextEditingController();
  final _tapGestureRecognizer = TapGestureRecognizer();
  bool _acceptedTerms = false;

  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _emailAddressController.dispose();
    _tapGestureRecognizer.dispose();
    super.dispose();
  }

  void _showErrorDialog(String message) {
    Announcer(context).showErrorMessage('There was an unexpected error', message);
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      if (_authMode == AuthMode.Login) {
        // Log user in
        await Provider.of<Auth>(context, listen: false).login(
          context,
          _authData['email'],
          _authData['password']
        );
      } else {
        // Sign user up
        await Provider.of<Auth>(context, listen: false).signup(
          context,
          _authData['email'],
          _authData['password'],
        ).then((_) async {
          if (mounted) {
            Announcer(context).showSuccessMessage('Success!', 'Account created successfully. Now you can pick a world to start playing.');
          }
          await Provider.of<Auth>(context, listen: false).createUser(_authData['username']);
        }).whenComplete(() {
          if (mounted) {
            setState(() {
              _isLoading = false;
            });
          };
        });
      }
    } on HttpException catch (error) {
      var errorMessage = 'Authentication failed';
      if (error.toString().contains('EMAIL_EXISTS')) {
        errorMessage = 'This email address is already in use.';
      } else if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = 'This is not a valid email address.';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = 'This password is too weak.';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage = 'Could not find a user with that email.';
      } else if (error.toString().contains('INVALID_PASSWORD')) {
        errorMessage = 'Invalid email/password combination.';
      }
      _showErrorDialog(errorMessage);
    } catch (error) {
      _emailAddressController.text = '';
      _usernameController.text = '';
      _passwordController.text = '';
      final errorMessage =
          'Could not authenticate you. Please try again later.';
      _showErrorDialog(errorMessage);
    }
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    };

  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(() {
        _authMode = AuthMode.Signup;
      });
    } else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }

  Future<void> _launchURL(String url) async {
    await launch(url);
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final validator = Validator();

    return Center(
      child: Form(
        key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  if (_authMode != AuthMode.Signup)
                  Image.asset(
                    'assets/images/icon.png',
                    height: 100,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 0),
                      child: Text(
                        _authMode == AuthMode.Signup ? 'Sign Up' : 'Sign In',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                  ),
                  if (_authMode == AuthMode.Signup)
                    TextFormField(
                      decoration: InputDecoration(labelText: 'Username'),
                      style: Theme.of(context).textTheme.bodyText1,
                      controller: _usernameController,
                      validator: (value) => validator.usernameValidator(value),
                      onSaved: (value) {
                        _authData['username'] = value;
                      },
                      textInputAction: TextInputAction.next,
                    ),
                  if (_authMode == AuthMode.Signup)
                    SizedBox(height: 30),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Email Address'),
                    style: Theme.of(context).textTheme.bodyText1,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    controller: _emailAddressController,
                    validator: (value) => validator.emailValidator(value),
                    onSaved: (value) {
                      _authData['email'] = value;
                    },
                  ),
                  SizedBox(height: 30),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Password'),
                    style: Theme.of(context).textTheme.bodyText1,
                    obscureText: true,
                    textInputAction: TextInputAction.next,
                    controller: _passwordController,
                    validator: (value) => validator.passwordValidator(value),
                    onSaved: (value) {
                      _authData['password'] = value;
                    },
                  ),
                  if (_authMode == AuthMode.Signup)
                    SizedBox(height: 30),
                  if (_authMode == AuthMode.Signup)
                    TextFormField(
                        enabled: _authMode == AuthMode.Signup,
                        decoration: InputDecoration(labelText: 'Confirm Password'),
                        style: Theme.of(context).textTheme.bodyText1,
                        obscureText: true,
                        textInputAction: TextInputAction.next,
                        validator: (value) => validator.passwordMatcher(value, _passwordController.text),
                    ),
                  if (_authMode == AuthMode.Signup)
                    SizedBox(height: 30),
                  if (_authMode == AuthMode.Signup)
                  CheckboxListTile(
                    title: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: 'By signing up you accept the ',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          TextSpan(
                              text: 'Terms of Service',
                              style: Theme.of(context).textTheme.overline,
                              recognizer: _tapGestureRecognizer
                                ..onTap = () {
                                  _launchURL('https://www.websitepolicies.com/policies/view/w6gdVe5j');
                                }
                          ),
                          TextSpan(
                            text: ' and ',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          TextSpan(
                              text: 'Privacy Policy.',
                              style: Theme.of(context).textTheme.overline,
                              recognizer: _tapGestureRecognizer
                                ..onTap = () {
                                  _launchURL('https://www.websitepolicies.com/policies/view/FD8fDYZw');
                                }
                          ),
                        ],
                      ),
                    ),
                    value: _acceptedTerms,
                    onChanged: (newValue) {
                      setState(() {
                        _acceptedTerms = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  SizedBox(height: 30),
                  LoadingElevatedButton(
                      _authMode == AuthMode.Signup ? 'Sign Up' : 'Sign In',
                      _isLoading,
                      _authMode == AuthMode.Signup ? !_acceptedTerms : false,
                      _submit
                  ),
                  SizedBox(height: 60),
                  Center(
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: _authMode == AuthMode.Signup ? 'Already have an account? ' : 'Don\'t have an account yet? ',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          TextSpan(
                              text: _authMode == AuthMode.Signup ? 'Sign In' : 'Sign Up',
                              style: Theme.of(context).textTheme.overline,
                              recognizer: _tapGestureRecognizer
                                ..onTap = () {
                                  _switchAuthMode();
                                }
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
    );
  }
}

