import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

  static var _t = Translations('en_us') +  {
    'en_us': 'Sign Up',
    'es': 'Registrarse',
  } + {
    'en_us': 'Sign In',
    'es': 'Ingresar',
  } + {
    'en_us': 'There was an unexpected error',
    'es': 'Ha ocurrido un error inesperado',
  } + {
    'en_us': 'Authentication failed',
    'es': 'Error durante autenticación',
  } + {
    'en_us': 'This email address is already in use.',
    'es': 'Esta dirección de email ya está en uso.',
  } + {
    'en_us': 'This is not a valid email address.',
    'es': 'Esta dirección de email no es válida.',
  } + {
    'en_us': 'This password is too weak.',
    'es': 'Esta contraseña es muy débil.',
  } + {
    'en_us': 'Could not find a user with that email.',
    'es': 'No se encontró un usuario con ese email.',
  } + {
    'en_us': 'Invalid password.',
    'es': 'Contraseña incorrecta.',
  } + {
    'en_us': 'Could not authenticate you. Please try again later.',
    'es': 'No se pudo autenticar. Por favor, intenta de nuevo más tarde',
  } + {
    'en_us': 'Email Address',
    'es': 'Dirección de Email',
  } + {
    'en_us': 'Username',
    'es': 'Nombre de Usuario',
  } + {
    'en_us': 'Password',
    'es': 'Contraseña',
  } + {
    'en_us': 'Confirm Password',
    'es': 'Confirmar Contraseña',
  } + {
    'en_us': 'By signing up you accept the ',
    'es': 'Registrandote aceptas los ',
  } + {
    'en_us': 'Terms of Service',
    'es': 'Términos del Servicio',
  } + {
    'en_us': ' and ',
    'es': ' y ',
  } + {
    'en_us': 'Privacy Policy.',
    'es': 'Política de Privacidad.',
  } + {
    'en_us': 'Already have an account? ',
    'es': '¿Ya tienes una cuenta? ',
  } + {
    'en_us': 'Don\'t have an account yet? ',
    'es': '¿Todavía no tienes una cuenta? ',
  } + {
    'en_us': 'Don\'t have an account yet? ',
    'es': '¿Todavía no tienes una cuenta? ',
  } + {
    'en_us': 'Account created successfully. Now pick a world to start playing.',
    'es': 'Cuenta creada exitósamente. Ahora elige un mundo para empezar a jugar.',
  };

  String get i18n => localize(this, _t);
}