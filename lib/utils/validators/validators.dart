import 'validators.i18n.dart';

//Validator class for all inputs
class Validator {

  String usernameValidator(username) {
    return (username.isEmpty || username.length < 3)
        ? 'Username is too short'
        : null;
  }

  String emailValidator(email) {
    return (email.isEmpty || !email.contains('@'))
        ? 'Enter a valid email address'
        : null;
  }

  String passwordValidator(password) {
    return (password.isEmpty || password.length < 5)
        ? 'Password is too short'
        : null;
  }

  String passwordMatcher(password1, password2) {
    return password1 != password2
        ? 'Passwords do not match'
        : null;
  }

  String worldValidator(value) {
    return (value == null || value.isEmpty)
        ? 'Choose a world'
        : null;
  }

}