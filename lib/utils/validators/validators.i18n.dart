import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

  static var _t = Translations('en_us') + {
    'en_us': 'Username is too short',
    'es': 'El nombre de usuario es muy corto',
  } + {
    'en_us': 'Enter a valid email address',
    'es': 'Ingresa una dirección de email válida',
  } + {
    'en_us': 'Password is too short',
    'es': 'La contraseña es muy corta',
  } + {
    'en_us': 'Passwords do not match',
    'es': 'Las contraseñas no coinciden',
  } + {
    'en_us': 'Choose a world',
    'es': 'No elegiste un mundo',
  }
  ;

  String get i18n => localize(this, _t);
}