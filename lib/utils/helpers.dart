import 'dart:math';
import 'package:bandit_and_heroes/battle/battle.dart';
import 'package:bandit_and_heroes/battle/event.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'extensions.dart';
import '../auth/auth.dart';
import '../battle/battle_record.dart';
import '../heroes/hero.dart' as Bandit;
import '../items/item.dart';
import '../quests/quest.dart';
import '../users/user.dart';
import '../world/world.dart';

Bandit.Hero buildHero (hero) {
  List<String> items = [];
  if (hero['items'] != null) {
    hero['items'].forEach((item) {
      items.add(item);
    });
  }
  return Bandit.Hero(
      heroClass: hero['heroClass'],
      level: hero['level'],
      xp: hero['xp'],
      isBenched: hero['isBenched'],
      id: hero['id'].toString(),
      items: items,
      lastQuest: DateTime.parse(hero['lastQuest']),
      questsCompleted: hero['questsCompleted'] != null ? hero['questsCompleted'].toList() : [],
      stats: {
        'strength': hero['stats']['strength'],
        'intelligence': hero['stats']['intelligence'],
        'dexterity': hero['stats']['dexterity'],
        'vitality': hero['stats']['vitality']
      }
  );
}

Bandit.Hero searchForHero(String id, List<Bandit.Hero> party) {
  return party.firstWhere((hero) => hero.id == id, orElse: () => null);
}

List<Event> buildEvents(context, events, List<Bandit.Hero> attackerParty, List<Bandit.Hero> defenderParty, String attackerUsername) {
  List<Event> ret = [];
  List<Bandit.Hero> jointParties = []..addAll(attackerParty)..addAll(defenderParty);
  events.forEach((event) {
    Event eventToAdd = Event(
      origin: searchForHero(event['origin'], jointParties),
      originOwner: event['originOwner'],
      target: searchForHero(event['target'], jointParties),
      targetsOwner: event['targetsOwner'],
      action: event['action'],
      damage: event['damage'],
      hpLeft: event['hpLeft'],
      isCritical: event['isCritical'],
      abilityTriggered: event['abilityTriggered'],
      spellName: event['spellName'],
      additionalAction: event['additionalAction'],
      stat: event['stat'],
      modifier: event['modifier'],
      stat2: event['stat2'],
      modifier2: event['modifier2'],
      stunned: event['stunned'],
    );
    if (eventToAdd.spellName != null && eventToAdd.stat != null) {
      eventToAdd.setDescriptionStatMod(context, attackerUsername);
    } else {
      if (eventToAdd.spellName != null && eventToAdd.target == null) {
        eventToAdd.setDescriptionSpell(context, attackerUsername);
      } else {
        eventToAdd.setDescription(context, attackerUsername);
      }
    }
    ret.add(eventToAdd);
  });
  return ret;
}

BattleRecord buildBattleRecord(context, battle) {
  List<Bandit.Hero> attackerParty = [];
  List<Bandit.Hero> defenderParty = [];
  List<Round> rounds = [];
  if (battle['attackerParty'] != null) {
    battle['attackerParty'].forEach((hero) {
      attackerParty.add(buildHero(hero));
    });
  }
  if (battle['defenderParty'] != null) {
    battle['defenderParty'].forEach((hero) {
      defenderParty.add(buildHero(hero));
    });
  }
  if (battle['rounds'] != null) {
    battle['rounds'].forEach((round) {
      Round roundToAdd = Round(
          round['number'],
          buildEvents(context, round['events'], attackerParty, defenderParty,
              battle['attackerUsername']),
          round['expanded']
      );
      rounds.add(roundToAdd);
    });
  }

  return BattleRecord(
    id: battle['id'],
    timestamp: battle['timestamp'],
    attackerUsername: battle['attackerUsername'],
    defenderUsername: battle['defenderUsername'],
    attackerParty: attackerParty,
    defenderParty: defenderParty,
    rounds: rounds,
    winner: battle['winner'],
    rewards: battle['rewards'],
  );

}

Quest buildQuest(quest) {
  String questDifficulty = (quest['difficulty'] as String).toLowerCase();
  Difficulty difficulty = Difficulty.Insane;
  if (questDifficulty == 'easy') {
    difficulty = Difficulty.Easy;
  } else {
    if (questDifficulty == 'medium') {
      difficulty = Difficulty.Medium;
    } else {
      if (questDifficulty == 'hard') {
        difficulty = Difficulty.Hard;
      }
    }
  }
  return Quest(
    id: quest['id'],
    name: quest['name'],
    description: quest['description'],
    image: quest['image'],
    difficulty: difficulty,
  );
}

User buildUser(userId, user) {
  return User(
    userId,
    user['username'],
    user['points'],
  );
}

Item buildItem (item) {
  String itemRarity = (item['rarity'] as String).toLowerCase();
  Rarity rarity = Rarity.Legendary;

  if (itemRarity == 'common') {
    rarity = Rarity.Common;
  } else {
    if (itemRarity == 'rare') {
      rarity = Rarity.Rare;
    } else {
      if (itemRarity == 'mythic') {
        rarity = Rarity.Mythic;
      }
    }
  }
  return Item(
      id: item['id'],
      name: item['name'],
      image: item['image'],
      owner: item['owner'],
      damage: item['damage'],
      armor: item['armor'],
      abilityText: item['abilityText'],
      type: convertType(item['type']),
      rarity: rarity,
      isNew: item['isNew'],
      slot: item['slot'],
      stats: {
        'strength': item['stats']['strength'],
        'intelligence': item['stats']['intelligence'],
        'dexterity': item['stats']['dexterity'],
        'vitality': item['stats']['vitality']
      }
  );
}

Bandit.Hero findTheLowestHP(List<Bandit.Hero> party) {
  int lowestHp = 99999;
  Bandit.Hero ret = party[0];
  party.forEach((hero) {
    if (hero.hp > 0 && hero.hp < lowestHp) {
      lowestHp = hero.hp;
      ret = hero;
    }
  });
  return ret;
}

Bandit.Hero findTheSlowest(List<Bandit.Hero> party) {
  int lowestDexterity = 99999;
  Bandit.Hero ret = party[0];
  party.forEach((hero) {
    if (hero.hp > 0 && hero.dexterity < lowestDexterity) {
      lowestDexterity = hero.dexterity;
      ret = hero;
    }
  });
  return ret;
}

Bandit.Hero findTheHighestStrength(List<Bandit.Hero> party) {
  int highestStrength = 1;
  Bandit.Hero ret = party[0];
  party.forEach((hero) {
    if (hero.hp > 0 && hero.strength > highestStrength) {
      highestStrength = hero.strength;
      ret = hero;
    }
  });
  return ret;
}

Bandit.Hero findRandom(List<Bandit.Hero> party) {
  var rng = Random();
  List<Bandit.Hero> clonedParty = new List<Bandit.Hero>.from(party);
  while (clonedParty.isNotEmpty) {
    int roll = rng.nextInt(clonedParty.length);
    if (clonedParty[roll].hp > 0) {
      return clonedParty[roll];
    } else {
      clonedParty.removeAt(roll);
    }
  }
  return null;
}

void cleanLogout(context) {
  Provider.of<World>(context, listen:false).setWorld(null);
  Navigator.of(context).pop();
  Navigator.of(context).pushReplacementNamed('/');
  Provider.of<Auth>(context, listen: false).logout();
}

ItemType convertType(String type) {
  ItemType itemType;
  switch (type.parseType().toLowerCase()) {
    case 'armor':
      {
        return itemType = ItemType.Armor;
      }
      break;
    case 'shield':
      {
        return itemType = ItemType.Shield;
      }
      break;
    case 'one_handed_sword':
      {
        return itemType = ItemType.One_Handed_Sword;
      }
      break;
    case 'two_handed_sword':
      {
        return itemType = ItemType.Two_Handed_Sword;
      }
      break;
    case 'axe':
      {
        return itemType = ItemType.Axe;
      }
      break;
    case 'dagger':
      {
        return itemType = ItemType.Dagger;
      }
      break;
    case 'bow':
      {
        return itemType = ItemType.Bow;
      }
      break;
    case 'crossbow':
      {
        return itemType = ItemType.Crossbow;
      }
      break;
    case 'gun':
      {
        return itemType = ItemType.Gun;
      }
      break;
    case 'staff':
      {
        return itemType = ItemType.Staff;
      }
      break;
    case 'trinket':
      {
        return itemType = ItemType.Trinket;
      }
      break;
  }
}