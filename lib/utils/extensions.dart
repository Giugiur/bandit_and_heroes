extension CapitalizeExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

extension RemoveUnderscore on String {
  String removeUnderscore() {
    return this.replaceAll('_',' ');
  }
}

extension ParseDifficulty on String {
  String parseDifficulty() {
    return this.replaceAll('Difficulty.', '');
  }
}

extension ParseRarity on String {
  String parseRarity() {
    return this.replaceAll('Rarity.', '');
  }
}

extension ParseType on String {
  String parseType() {
    return this.replaceAll('ItemType.', '');
  }
}