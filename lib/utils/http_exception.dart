
// A class to throw custom exception, not catched by regular error handling
// For instance, sometimes we get a 200 response on an incorrect_password error type(using Firebase)
class HttpException implements Exception {
  final String message;

  HttpException(this.message);

  @override
  String toString() {
    return message;
  }
}