import 'package:flutter/material.dart';
import '../utils/custom_icons.dart';

class GoldCost extends StatelessWidget {

  const GoldCost(this.cost);

  final int cost;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(CustomIcons.gold_bar, size: 15,),
        SizedBox(width: 5,),
        Text(
          cost.toString(),
          style: TextStyle(
              fontWeight: FontWeight.bold
          ),
        ),
      ],
    );
  }
}