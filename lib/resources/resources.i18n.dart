import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

  static var _t = Translations('en_us') + {
    'en_us': 'Gold Coins',
    'es': 'Monedas',
  } + {
    'en_us': 'Sapphires',
    'es': 'Zafiros',
  } + {
    'en_us': 'Quest Ready!',
    'es': 'Aventura Lista',
  } + {
    'en_us': 'Sapphires',
    'es': 'Zafiros',
  } + {
    'en_us': 'Next Quest in',
    'es': 'Próxima Aventura en ',
  };

  String get i18n => localize(this, _t);
}