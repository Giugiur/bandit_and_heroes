import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Resources extends ChangeNotifier {
  String token;
  String userId;
  int _gold;
  int _sapphires;
  DateTime _lastQuest;
  Duration _questCooldown;
  int _world;

  Resources(this.token, this.userId, this._gold, this._sapphires, this._lastQuest, this._questCooldown, this._world);

  int get gold => _gold;

  Future<void> setGold(int amount) async {
    _gold = _gold + amount;
    await saveResources();
    notifyListeners();
  }

  int get sapphires => _sapphires;

  Future<void> setSapphires(int amount) async {
    _sapphires = _sapphires + amount;
    await saveResources();
    notifyListeners();
  }

  void setLastQuest(String timestamp) {
    _lastQuest = DateTime.parse(timestamp);
    saveResources();
    notifyListeners();
  }

  Future<void> saveResources() async {
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${_world.toString()}/users/$userId/resources.json?auth=$token');
    try {
      await http.patch(
        url,
        body: json.encode({
          'gold': _gold,
          'sapphires': _sapphires,
          'lastQuest': _lastQuest.toIso8601String()
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  Duration get nextQuest {
    Duration _timeForNextQuest = (_lastQuest.add(_questCooldown).difference(DateTime.now()));
    return _timeForNextQuest;
  }
}
