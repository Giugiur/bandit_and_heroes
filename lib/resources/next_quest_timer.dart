import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:countdown_timer_simple/countdown_timer_simple.dart';
import 'package:provider/provider.dart';
import '../quests/quests.dart';
import '../quests/quests_screen.dart';
import '../utils/custom_icons.dart';

class NextQuestTimer extends StatefulWidget {
  final Duration nextQuest;

  NextQuestTimer(this.nextQuest);

  @override
  _NextQuestTimerState createState() => _NextQuestTimerState();
}

class _NextQuestTimerState extends State<NextQuestTimer> {
  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 15;
  bool alreadyRun = false;

  @override
  Widget build(BuildContext context) {

    return Expanded(
      child: widget.nextQuest.isNegative ?
      InkWell(
        onTap: () => Navigator.pushNamed(context, QuestsScreen.routeName),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 2),
          height: 55,
          child: Column(
            children: [
              const Icon(CustomIcons.clockwork),
              Text(
                'Quest Ready!',
                style: TextStyle(
                  color: Colors.green[300],
                  fontSize: 16,
                )
              ),
            ]
          ),
        ),
      ) :
       Container(
         padding: EdgeInsets.only(top: 10),
         child: Column(
           children: <Widget>[
             CountdownTimerSimple(
              endTime: DateTime.now().add(widget.nextQuest).millisecondsSinceEpoch + 60 * 60,
              showDay: false,
              showHour: true,
              showMin: true,
              showSec: true,
              textStyle: Theme.of(context).textTheme.headline4,
              onEnd: () {
                // This condition is necessary because onEnd is calling twice after the time is finished.
                if (!Provider.of<Quests>(context, listen: false).resolvingQuest) {
                  Provider.of<Quests>(context, listen: false).resolveQuest(context);
                }
              }
             ),
             Text(
              'Next Quest in',
              style: Theme.of(context).textTheme.bodyText2
             )
          ],
        ),
       ),
    );
  }
}