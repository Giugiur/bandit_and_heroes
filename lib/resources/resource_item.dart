import 'package:flutter/material.dart';

// Widget for displaying the resources of the user
class ResourceItem extends StatelessWidget {
  String amount;
  Icon icon;
  String text;

  ResourceItem(this.icon, this.amount, this.text);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 2),
        height: 55,
        color: Theme.of(context).canvasColor,
        child: Column(
          children: [
            icon,
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: amount,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  TextSpan(
                      text: ' $text',
                      style: Theme.of(context).textTheme.bodyText2,
                  ),
                ],
              ),
            )
          ],
        )
      ),
    );
  }
}
