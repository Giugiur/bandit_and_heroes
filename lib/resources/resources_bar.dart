import 'package:bandit_and_heroes/subscription/subscriptions.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
import 'resources.dart';
import 'resources.i18n.dart';
import 'resource_item.dart';
import 'next_quest_timer.dart';
import '../config/ad_state.dart';
import '../utils/custom_icons.dart';

//This is a bottom bar that holds the resources of the user
class ResourcesBar extends StatefulWidget {
  @override
  _ResourcesBarState createState() => _ResourcesBarState();
}

class _ResourcesBarState extends State<ResourcesBar> {
  BannerAd banner;
  bool isPremium;

  void didChangeDependencies() {
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      setState(() {
        banner = BannerAd(
          adUnitId: adState.bannerAdUnitId,
          size: AdSize.banner, //320x50
          request: AdRequest(),
          listener: adState.bannerAdListener
        )..load();
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final gold = Provider.of<Resources>(context).gold;
    final sapphires = Provider.of<Resources>(context).sapphires;
    final nextQuest = Provider.of<Resources>(context).nextQuest;
    isPremium = Provider.of<Subscription>(context, listen: false).isPremium;

    return ListView(
      shrinkWrap: true,
      children: [
        if (!isPremium && banner == null)
          SizedBox(height: 50,)
        else if (!isPremium)
          Container(
            height: 50,
            child: AdWidget(ad: banner),
          ),
        Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              border: Border(top: BorderSide(color: Theme.of(context).dividerColor)),
            ),
            child: Row(
              children: [
                ResourceItem(
                    const Icon(CustomIcons.gold_bar),
                    gold.toString(),
                    'Gold'
                ),
                Container(
                  width: 2,
                  color: Theme.of(context).dividerColor,
                ),
                ResourceItem(
                    const Icon(CustomIcons.diamond),
                    sapphires.toString(),
                    'Sapphires'
                ),
                Container(
                  width: 1,
                  color: Theme.of(context).dividerColor,
                ),
                NextQuestTimer(nextQuest),
              ],
            ),
          ),
        ),
      ]
    );
  }
}
