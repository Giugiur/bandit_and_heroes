import 'package:flutter/foundation.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

class Subscription extends ChangeNotifier {
  Subscription() {
    init();
  }

  bool _isPremium = false;
  bool get isPremium => _isPremium;

  Future init() async {
    Purchases.addPurchaserInfoUpdateListener((purchaserInfo) async {
      updatePurchaseStatus();
    });
  }

  Future updatePurchaseStatus() async {
    final purchaserInfo = await Purchases.getPurchaserInfo();
    final entitlements = purchaserInfo.entitlements.active.values.toList();
    _isPremium = entitlements.isEmpty ? false : true;
    notifyListeners();
  }
}