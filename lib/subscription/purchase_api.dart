import 'package:flutter/services.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../utils/keys.dart';

class PurchaseApi {
  static const _apiKey = PURCHASE_API_KEY;

  static Future init() async {
    await Purchases.setDebugLogsEnabled(true);
    await Purchases.setup(_apiKey);
  }

  static Future<List<Offering>> fetchOffers() async {
    try {
      final offerings = await Purchases.getOfferings();
      final current = offerings.current;
      return current == null ? [] : [current];
    } on PlatformException catch (e) {
      return [];
    }
  }

  static Future<void> purchasePackage(Package package) async {
    await Purchases.purchasePackage(package);
  }
}