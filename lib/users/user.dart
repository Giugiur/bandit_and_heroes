import 'package:flutter/foundation.dart';
import '../heroes/hero.dart';
import '../utils/helpers.dart';

class User with ChangeNotifier {
  final String username;
  final int points;
  final String id;

  User(this.id, this.username, this.points);
}