import 'battle.dart';
import '../heroes/hero.dart' as Bandit;

class BattleRecord {
  final String attackerUsername;
  final String defenderUsername;
  final List<Bandit.Hero> attackerParty;
  final List<Bandit.Hero> defenderParty;
  final String id;
  List<Round> rounds = [];
  String winner;
  List<Bandit.Hero> winnerParty;
  String rewards;
  String timestamp;

  BattleRecord({
    this.id,
    this.timestamp,
    this.attackerUsername,
    this.defenderUsername,
    this.attackerParty,
    this.defenderParty,
    this.rounds,
    this.winner,
    this.rewards,
  });

}