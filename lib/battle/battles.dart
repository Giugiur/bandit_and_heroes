import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'battle.dart';
import 'battle_record.dart';
import '../auth/auth.dart';
import '../heroes/hero.dart' as Bandit;
import '../utils/constants.dart';
import '../utils/helpers.dart';
import '../world/world.dart';

class Battles with ChangeNotifier {

  Battles(this.token,
      this.userId,
      this.world);

  final String userId;
  final String token;
  final int world;

  Future<BattleRecord> getBattle(context, String id) async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId/battles/$id.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      return buildBattleRecord(context, extractedData);
    } catch (error) {
      throw error;
    }
  }

  Future<List<BattleRecord>> getLastBattles(context, {String id}) async {
    if (id == null) {
      id = userId;
    }
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$id/battles.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      List<BattleRecord> battles = [];
      if (extractedData != null) {
        extractedData.forEach((battleId, battle) {
          battles.add(buildBattleRecord(context, battle));
        });
      }
      battles.sort((a, b) => DateTime.parse(a.timestamp).isAfter(DateTime.parse(b.timestamp)) ? 0 : 1);
      return battles;
    } catch (error) {
      throw error;
    }
  }

  void deleteBattle(String id, {String idUser}) async {
    if (idUser == null) {
      idUser = userId;
    }
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$idUser/battles/$id.json?auth=$token');
    await http.delete(url);
  }

  Future<void> saveBattle(context, Battle battle, String userId) async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId/battles/${battle.id}.json?auth=$token');
    try {
      await http.put(
        url,
        body: json.encode({
          'id': battle.id,
          'timestamp': battle.timestamp,
          'attackerId': battle.attackerId,
          'defenderId': battle.defenderId,
          'attackerUsername': battle.attackerUsername,
          'defenderUsername': battle.defenderUsername,
          'attackerParty': convertPartyToJSON(battle.attackerParty),
          'defenderParty': convertPartyToJSON(battle.defenderParty),
          'rounds': getRounds(battle),
          'winner': battle.winner,
          'winnerParty': convertPartyToJSON(battle.winnerParty),
          'rewards': battle.rewards,
        }),
      );
      getLastBattles(context, id: battle.attackerId).then((response) {
        if (response.length > MAX_BATTLE_RECORDS) {
          deleteBattle(response[response.length-1].id);
        }
      });
      getLastBattles(context, id: battle.defenderId).then((response) {
        if (response.length > MAX_BATTLE_RECORDS) {
          deleteBattle(response[response.length-1].id, idUser: battle.defenderId);
        }
      });
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateStats(context, Battle battle, String userId, String username, String winner) async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId.json?auth=$token');
    int points, battlesWon, battlesLost;
    if (points == null || battlesWon == null || battlesLost == null) {
      points = await Provider.of<World>(context, listen: false).getFieldFromUser(userId, 'points');
      battlesWon = await Provider.of<World>(context, listen: false).getFieldFromUser(userId, 'battlesWon');
      battlesLost = await Provider.of<World>(context, listen: false).getFieldFromUser(userId, 'battlesLost');
    }
    if (winner != 'draw' && winner == username) {
      battlesWon++;
      points += 25;
    } else {
      if (winner != 'draw' && winner != username) {
        battlesLost++;
        points -= 25;
        if (points < 0) {
          points = 0;
        }
      }
    }
    if (username == Provider.of<Auth>(context,listen: false).username) {
      Provider.of<World>(context, listen: false).setPoints(points);
      Provider.of<World>(context, listen: false).setBattlesWon(battlesWon);
      Provider.of<World>(context, listen: false).setBattlesLost(battlesLost);
    }
    try {
      await http.patch(
        url,
        body: json.encode({
          'points': points,
          'battlesWon': battlesWon,
          'battlesLost': battlesLost,
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  List<dynamic> getRounds(Battle battle) {
    List<dynamic> ret = [];
    battle.rounds.forEach((round) {
      dynamic roundToReturn = {};
      roundToReturn['number'] = round.number;
      roundToReturn['expanded'] = round.expanded;
      roundToReturn['events'] = [];
      round.events.forEach((event) {
        dynamic eventToReturn = {};
        eventToReturn['origin'] = event.origin != null ? event.origin.id : null;
        eventToReturn['originOwner'] = event.originOwner;
        eventToReturn['target'] = event.target != null ? event.target.id : null;
        eventToReturn['targetsOwner'] = event.targetsOwner;
        eventToReturn['action'] = event.action;
        eventToReturn['damage'] = event.damage;
        eventToReturn['hpLeft'] = event.hpLeft;
        eventToReturn['isCritical'] = event.isCritical;
        eventToReturn['description'] = null;
        eventToReturn['abilityTriggered'] = event.abilityTriggered;
        eventToReturn['spellName'] = event.spellName;
        eventToReturn['additionalAction'] = event.additionalAction;
        eventToReturn['stat'] = event.stat;
        eventToReturn['modifier'] = event.modifier;
        eventToReturn['stat2'] = event.stat2;
        eventToReturn['modifier2'] = event.modifier2;
        eventToReturn['stunned'] = event.stunned;
        roundToReturn['events'].add(eventToReturn);
      });
      ret.add(roundToReturn);
    });
    return ret;
  }

  List<dynamic> convertPartyToJSON(List<Bandit.Hero> party) {
    List<dynamic> ret = [];
    party.forEach((hero) {
      dynamic aux = {};
      aux['heroClass'] = hero.heroClass;
      aux['level'] = hero.level;
      aux['xp'] = hero.xp;
      aux['id'] = hero.id;
      aux['items'] = hero.items;
      aux['isBenched'] = hero.isBenched;
      aux['lastQuest'] = hero.lastQuest.toIso8601String();
      aux['questsCompleted'] = hero.questsCompleted;
      aux['stats'] = {
        'strength': hero.strength,
        'dexterity': hero.dexterity,
        'intelligence': hero.intelligence,
        'vitality': hero.vitality,
      };
      ret.add(aux);
    });
    return ret;
  }
}