import 'package:flutter/material.dart';
import 'battle.dart';

class ExpandableRound extends StatefulWidget {

  ExpandableRound(this.round);

  final Round round;

  @override
  _ExpandableRoundState createState() => _ExpandableRoundState();
}

class _ExpandableRoundState extends State<ExpandableRound> {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Container(
      child: SingleChildScrollView(
        child: ListView(
          primary: false,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            Container(
              width: deviceSize.width,
              child: InkWell(
                onTap: () {
                  setState(() {
                    widget.round.expanded = !widget.round.expanded;
                  });
                },
                child: Row(
                  children: [
                    TextButton(
                      child: Container(
                        child: Text(
                          'Round ${widget.round.number}',
                          style: Theme.of(context).textTheme.headline3,
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Icon(widget.round.expanded ? Icons.arrow_drop_down : Icons.arrow_right),
                  ]
                ),
              ),
            ),
            if (widget.round.expanded)
                //height: widget.round.expanded ? bodyHeight : 0,
             ListView.builder(
               primary: false,
               physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (ctx, index) => widget.round.events[index].description,
                itemCount: widget.round.events.length,
              ),
          ],
        ),
      ),
    );
  }
}
