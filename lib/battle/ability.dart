import '../heroes/hero.dart' as Bandit;

class Ability {
  Bandit.Hero owner;
  bool canTrigger;
  int triggerChance;
  String name;
  bool isTaunt;
  bool isPassive;
  bool hasAdditionalAttack;
  bool hasAdditionalTarget;
  bool triggersOncePerRound;
  bool triggersOncePerBattle;
  bool triggerOnAllyDeath;
  bool triggerOnOwnerDeath;
  bool triggerOnReceivingDamage;
  bool triggerOnInflictingDamage;
  bool triggerOnNotReceivingDamage;
  bool triggerOnBattleStart;
  bool triggerOnBattleEnd;
  bool triggerOnAttack;
  bool triggerOnSpellCast;
  bool triggerOnTargetingSpell;
  bool triggerOnCalculatingTargets;
  bool triggerOnRoundStart;

  Ability({
    this.owner,
    this.name,
    this.canTrigger = true,
    this.triggerChance = 100,
    this.isTaunt = false,
    this.isPassive = false,
    this.hasAdditionalAttack = false,
    this.hasAdditionalTarget = false,
    this.triggersOncePerRound = false,
    this.triggersOncePerBattle = false,
    this.triggerOnAllyDeath = false,
    this.triggerOnOwnerDeath = false,
    this.triggerOnReceivingDamage = false,
    this.triggerOnInflictingDamage = false,
    this.triggerOnNotReceivingDamage = false,
    this.triggerOnBattleStart = false,
    this.triggerOnBattleEnd = false,
    this.triggerOnAttack = false,
    this.triggerOnSpellCast = false,
    this.triggerOnTargetingSpell = false,
    this.triggerOnCalculatingTargets = false,
    this.triggerOnRoundStart = false,
  });
}