import 'package:bandit_and_heroes/battle/battle_record.dart';
import 'package:bandit_and_heroes/items/item.dart';
import 'package:bandit_and_heroes/items/items.dart';
import 'package:bandit_and_heroes/items/legendaryItems.dart';
import 'package:uuid/uuid.dart';
import '../heroes/hero.dart' as Bandit;
import '../bandit_bar/bandit_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'battle.dart';
import 'battles.dart';
import 'battle_card.dart';

class BattleDetailScreen2 extends StatefulWidget {
  static const routeName = '/battle_detail_screen2';
  @override
  _BattleDetailScreen2State createState() => _BattleDetailScreen2State();
}

class _BattleDetailScreen2State extends State<BattleDetailScreen2> {

  void mockParties(context, List<Bandit.Hero> attackerParty, List<Bandit.Hero> defenderParty) {
    List<Item> legs = new List<Item>.from(getLegendaryItems());
    legs.forEach((item) {
      Provider.of<Items>(context, listen: false).addItem(item);
    });

    Item warlordSword = Item(
      id: '1a',
      name: 'Warlord Sword',
      image: 'assets/images/onehandsword.jpg',
      owner: 'a7',
      damage: 19,
      armor: 0,
      type: ItemType.Two_Handed_Sword,
      stats: {
        'strength': 7,
        'dexterity': 0,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(warlordSword);

    Bandit.Hero warlord = Bandit.Hero(
        id: 'a1',
        heroClass: 'warlord',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['2', '1'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 15,
          'dexterity': 11,
          'intelligence': 10,
          'vitality': 13,
        }
    );

    Item shieldBSword = Item(
      id: '2a',
      name: 'Shieldbearer Sword',
      image: 'assets/images/onehandsword.jpg',
      owner: 'a2',
      damage: 15,
      armor: 0,
      type: ItemType.One_Handed_Sword,
      stats: {
        'strength': 7,
        'dexterity': 0,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(shieldBSword);
    Item shieldBShield = Item(
      id: '2c',
      name: 'Shieldbearer Shield',
      image: 'assets/images/shield.jpg',
      owner: 'a2',
      damage: 0,
      armor: 5,
      type: ItemType.Shield,
      stats: {
        'strength': 0,
        'dexterity': 0,
        'intelligence': 0,
        'vitality': 7
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'shield',
    );
    Provider.of<Items>(context, listen: false).addItem(shieldBShield);
    Item shieldBArmor = Item(
      id: '2b',
      name: 'Common Armor',
      image: 'assets/images/armor.jpg',
      owner: 'a2',
      damage: 0,
      armor: 4,
      type: ItemType.Armor,
      stats: {
        'strength': 0,
        'dexterity': 0,
        'intelligence': 0,
        'vitality': 7
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'armor',
    );
    Provider.of<Items>(context, listen: false).addItem(shieldBArmor);
    Bandit.Hero shieldbearer = Bandit.Hero(
        id: 'a2',
        heroClass: 'shieldbearer',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['11', '2b', '7', '12'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 14,
          'dexterity': 10,
          'intelligence': 10,
          'vitality': 14,
        }
    );

    Bandit.Hero paladin = Bandit.Hero(
        id: 'a3',
        heroClass: 'paladin',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['4', '10'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 14,
          'dexterity': 12,
          'intelligence': 11,
          'vitality': 14,
        }
    );

    Item huntressBow = Item(
      id: '4a',
      name: 'Huntress Bow',
      image: 'assets/images/bow.jpg',
      owner: 'a3',
      damage: 19,
      armor: 0,
      type: ItemType.Bow,
      stats: {
        'strength': 0,
        'dexterity': 7,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(huntressBow);
    Bandit.Hero huntress = Bandit.Hero(
        id: 'a4',
        heroClass: 'huntress',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['4a', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 11,
          'dexterity': 14,
          'intelligence': 11,
          'vitality': 12,
        }
    );

    Item sniperGun = Item(
      id: '7a',
      name: 'Sniper Gun',
      image: 'assets/images/gun.jpg',
      owner: 'a7',
      damage: 19,
      armor: 0,
      type: ItemType.Gun,
      stats: {
        'strength': 0,
        'dexterity': 7,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(sniperGun);
    Bandit.Hero sniper = Bandit.Hero(
        id: 'a7',
        heroClass: 'sniper',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['8', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 11,
          'dexterity': 14,
          'intelligence': 11,
          'vitality': 10,
        }
    );

    Item assassinDagger = Item(
      id: '5a',
      name: 'Assassin Dagger',
      image: 'assets/images/dagger.jpg',
      owner: 'a5',
      damage: 19,
      armor: 0,
      type: ItemType.Dagger,
      stats: {
        'strength': 0,
        'dexterity': 7,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(assassinDagger);
    Bandit.Hero assassin = Bandit.Hero(
        id: 'a5',
        heroClass: 'assassin',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['6', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 12,
          'dexterity': 15,
          'intelligence': 10,
          'vitality': 10,
        }
    );

    Item bountyHunterCrossbow = Item(
      id: '6a',
      name: 'Bounty Hunter Crossbow',
      image: 'assets/images/crossbow.jpg',
      owner: 'a6',
      damage: 19,
      armor: 0,
      type: ItemType.Crossbow,
      stats: {
        'strength': 0,
        'dexterity': 7,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(bountyHunterCrossbow);
    Bandit.Hero bountyHunter = Bandit.Hero(
        id: 'a6',
        heroClass: 'bounty_hunter',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['9', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 11,
          'dexterity': 13,
          'intelligence': 10,
          'vitality': 13,
        }
    );

    Bandit.Hero priest = Bandit.Hero(
        id: 'a8',
        heroClass: 'priest',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['2a', '2c', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 12,
          'dexterity': 10,
          'intelligence': 14,
          'vitality': 15,
        }
    );

    Item staff = Item(
      id: '9a',
      name: 'Staff',
      image: 'assets/images/staff.jpg',
      owner: 'a9',
      damage: 10,
      armor: 0,
      type: ItemType.Staff,
      stats: {
        'strength': 0,
        'dexterity': 0,
        'intelligence': 7,
        'vitality': 0
      },
      rarity: Rarity.Common,
      abilityText: ' ',
      isNew: true,
      slot: 'weapon',
    );
    Provider.of<Items>(context, listen: false).addItem(staff);
    Bandit.Hero warlock = Bandit.Hero(
        id: 'a9',
        heroClass: 'warlock',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['9a', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 10,
          'dexterity': 12,
          'intelligence': 14,
          'vitality': 11,
        }
    );

    Bandit.Hero sorceress = Bandit.Hero(
        id: 'a10',
        heroClass: 'sorceress',
        level: 1,
        xp: 0,
        isBenched: false,
        items: ['0', '2b'],
        lastQuest: DateTime.now(),
        questsCompleted: [],
        stats: {
          'strength': 10,
          'dexterity': 11,
          'intelligence': 15,
          'vitality': 10,
        }
    );

    //Parties
    attackerParty.add(shieldbearer);
    attackerParty.add(huntress);
    attackerParty.add(priest);
    defenderParty.add(paladin);
    defenderParty.add(warlock);
    defenderParty.add(sorceress);
  }

  bool _isLoading = false,
      battleReady = false;
  BattleRecord _battleRecord;

  void fetchBattle(context) async {
    List<BattleRecord> lastBattles = await Provider.of<Battles>(context, listen: false).getLastBattles(context);
    setState(() {
      _battleRecord = lastBattles[1];
      battleReady = true;
    });
  }

  void _beginBattle(context) async {
    List<Bandit.Hero> attackerParty = [];
    List<Bandit.Hero> defenderParty = [];
    mockParties(context, attackerParty, defenderParty);
    //List<Bandit.Hero> attackerParty = new List<Bandit.Hero>.from(Provider.of<Heroes>(context, listen: false).party);
    //List<Bandit.Hero> defenderParty = new List<Bandit.Hero>.from(await Provider.of<Heroes>(context, listen: false).getPartyById(id));
    String id = Uuid().v1();
    String timestamp = DateTime.now().toIso8601String();
    Battle battle = Battle(id, timestamp, 'attacker', 'defender', attackerParty, defenderParty, 'Shula', 'RandomUser');
    battle.fight(context);
    setState(() {
      _isLoading = true;
    });
    fetchBattle(context);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BanditBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator(),)
      : Container(
        child: ListView(
          children: [
            TextButton(onPressed: () => _beginBattle, child: Text('Begin Battle')),
            battleReady? BattleCard(_battleRecord) : Container()
          ],
        ),
      )
    );
  }
}
