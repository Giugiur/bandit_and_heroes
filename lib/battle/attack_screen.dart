import 'package:bandit_and_heroes/auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'attack_user_tile.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';
import '../users/user.dart';
import '../world/world.dart';

class AttackScreen extends StatefulWidget {
  static const routeName = '/attack';

  @override
  _AttackScreenState createState() => _AttackScreenState();
}

class _AttackScreenState extends State<AttackScreen> {
  var _isLoading = false;
  List<User> _userList = [];
  int points;

  void didChangeDependencies() async {
    setState(() {
      _isLoading = true;
    });
    points = Provider.of<World>(context).points;
    if (points == null) {
      await Provider.of<World>(context, listen: false).getUserData();
    }
    Provider.of<World>(context, listen: false).getAttackableUsers(context)
      .then((response) {
        setState(() {
          _userList = response;
        });
    }).whenComplete(() => setState(() { _isLoading = false; }));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        height: deviceSize.height,
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.topCenter,
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Your Points: ',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    TextSpan(
                      text: points.toString(),
                      style: Theme.of(context).textTheme.overline,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 15,),
            _isLoading ? Center(
              child: CircularProgressIndicator(),
            ) : SingleChildScrollView(
              child: Container(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (ctx, index) =>
                    AttackUserTile(
                      _userList[index].id,
                      _userList[index].username,
                      _userList[index].points,
                    ),
                  itemCount: _userList.length,
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
