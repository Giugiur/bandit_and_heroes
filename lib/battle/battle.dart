import 'dart:math';
import 'package:bandit_and_heroes/items/items.dart';
import 'package:bandit_and_heroes/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'ability.dart';
import 'battle_helpers.dart';
import 'battles.dart';
import 'event.dart';
import 'spell.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/heroes.dart';
import '../utils/constants.dart';
import '../utils/extensions.dart';
import '../world/world.dart';

class Round {
  final int number;
  List<Event> events;
  bool expanded;

  Round(this.number, this.events, this.expanded);
}

class Battle {
  final String attackerId;
  final String defenderId;
  final String attackerUsername;
  final String defenderUsername;
  final List<Bandit.Hero> attackerParty;
  final List<Bandit.Hero> defenderParty;
  final String id;
  List<Round> rounds = [];
  String winner;
  List<Bandit.Hero> winnerParty;
  String rewards;
  List<Ability> abilities = [];
  String timestamp;

  Battle(
    this.id,
    this.timestamp,
    this.attackerId,
    this.defenderId,
    this.attackerParty,
    this.defenderParty,
    this.attackerUsername,
    this.defenderUsername
  );

  void sortAttackOrder(List<Bandit.Hero> attackOrder) {
    attackOrder.sort((a, b) => a.dexterity > b.dexterity ? 0 : 1);
    List<Bandit.Hero> heroesWithAlacrity = [];
    abilities.forEach((ability) {
      if (ability.canTrigger && ability.name.toLowerCase() == 'alacrity') {
        heroesWithAlacrity.add(ability.owner);
        attackOrder.remove(ability.owner);
      }
    });
    heroesWithAlacrity.sort((a, b) => a.dexterity > b.dexterity ? 0 : 1);
    attackOrder.insertAll(0, heroesWithAlacrity);
  }

  Future<void> closeBattle(context) async {
    if (winner == null) {
      if(atLeastOneMemberIsAlive(attackerParty)) {
        winner = attackerUsername;
        winnerParty = attackerParty;
      } else {
        if (atLeastOneMemberIsAlive(defenderParty)) {
          winner = defenderUsername;
          winnerParty = defenderParty;
        } else {
          winner = 'draw';
          winnerParty = [];
        }
      }
    }
    triggerBattleEnd(context);
    await saveBattle(context);
    await setProtection(context, defenderId);
    await updateStats(context);
  }

  Future<void> fight(context) async {
    if (defenderParty.isEmpty) {
      winner = attackerUsername;
      winnerParty = attackerParty;
      await closeBattle(context);
      return;
    }
    List<Bandit.Hero> attackOrder = []..addAll(attackerParty)..addAll(defenderParty);
    await Provider.of<Items>(context, listen: false).provideOppItemList(context, defenderId);
    attackOrder.forEach((hero) {
      hero.setHp(hero.maxHp(context));
      abilities.add(hero.instantiateAbility);
      abilities.addAll(hero.getItemAbilities(context));
      hero.setHeroAbilitiesPerItems(context);
      if (hero.isSpellcaster) {
        hero.availableSpells = hero.spells;
      }
    });
    sortAttackOrder(attackOrder);
    List<Event> battleStartEvents = [];
    triggerBattleStart(context, battleStartEvents);
    if (battleStartEvents.isNotEmpty) {
      rounds.add(Round(
        0,
        battleStartEvents,
        true
      ));
    }
    int i = 0;
    while(atLeastOneMemberIsAlive(attackerParty) && atLeastOneMemberIsAlive(defenderParty)) {
      List<Event> events = [];
      i++;
      Round round = Round(
        i,
        events,
        false,
      );
      abilities.forEach((ability) {
        if (ability.triggersOncePerRound && !ability.owner.hasPassiveDisabled) {
          ability.canTrigger = true;
        }
      });
      abilities.removeWhere((ability) => ability.owner.hp <= 0);
      attackOrder.forEach((hero) {
        hero.receivedDamageThisRound = false;
      });
      sortAttackOrder(attackOrder);
      attackOrder.forEach((hero) {
        if (hero.hp > 0) {
          if (!hero.isStunned) {
            List<Bandit.Hero> targets = calculateTargets(hero, 'attacks');
            if (!hero.isSpellcaster || isPriestAndCanAttack(hero)) {
              attack(context, hero, targets, events);
            }
            if (hero.isSpellcaster) { //&& atLeastOneMemberIsAlive(party)
              castSpell(context, hero, events, this);
            }
          } else {
            String originOwner = attackerParty.contains(hero) ? attackerUsername : defenderUsername;
            Event stunEvent = Event(
              stunned: true,
              origin: hero,
              originOwner: originOwner
            );
            stunEvent.setDescription(context, attackerUsername);
            events.add(stunEvent);
            hero.isStunned = false;
          }
        }
        reactivatePassive(hero);
      });
      rounds.add(round);
    }
    await closeBattle(context);
  }

  Future<void> setProtection(context, String id) async {
    await Provider.of<World>(context, listen:false).setProtection(id);
  }

  Future<void> updateStats(context) async {
    await Provider.of<Battles>(context, listen:false).updateStats(context, this, attackerId, attackerUsername, winner);
    await Provider.of<Battles>(context, listen:false).updateStats(context, this, defenderId, defenderUsername, winner);
  }

  Future<void> saveBattle(context) async {
    await Provider.of<Battles>(context, listen:false).saveBattle(context, this, attackerId);
    await Provider.of<Battles>(context, listen:false).saveBattle(context, this, defenderId);
  }

  void castSpell(context, Bandit.Hero hero, List<Event> events, Battle battle) {
    if (hero.availableSpells.isEmpty) {
      hero.availableSpells = hero.spells;
    }
    var rng = Random();
    int roll = rng.nextInt(hero.availableSpells.length);
    bool isMulticast = triggerAbilities(context, 'triggerOnSpellCast', hero, null, null, null, events) && hero.heroClass == 'sorceress';
    hero.availableSpells[roll].cast(context, battle, hero, attackerParty, defenderParty, attackerUsername, defenderUsername, isMulticast, events);
    hero.availableSpells.removeAt(roll);
    if (isMulticast) {
      hero.hasAdditionalTarget = false;
    }
  }

  void attack(context, Bandit.Hero hero, List<Bandit.Hero> targets, List<Event> events, {Ability abilityTriggered}) {
    int i = 0;
    bool keepAttacking = true;
    while(i<targets.length && keepAttacking) {
      List<Event> lazyEvents = [];
      Bandit.Hero target = targets[i];
      Event event = Event();
      triggerAbilities(context, 'triggerOnNotReceivingDamage', hero, target, targets, event, events);
      bool triggeredGlorybearer  = false,
        triggeredResurrection = false;
      int damage = calculateDamage(hero, target, event, context);
      int hpLeft, reducedDamage = 0, eventDamage, healHp = 0;
      if (!target.preventIncomingDamage) {
        hpLeft =  target.hp - damage;
        eventDamage = damage;
      } else {
        reducedDamage = damage - damage*target.preventQuantity;
        hpLeft = target.hp - reducedDamage;
        target.preventIncomingDamage = false;
        event.abilityTriggered = 'Prevent Damage';
        eventDamage = reducedDamage;
      }
      if (reducedDamage < damage) {
        target.receivedDamageThisRound = true;
        triggerAbilities(context, 'triggerOnReceivingDamage', hero, target, targets, event, events);
      }
      if (event.abilityTriggered == 'Lifesteal') {
        healHp = hero.hp + (eventDamage*ITEM_SOULDRINKER_LIFESTEAL).ceil() > hero.maxHp(context) ? hero.maxHp(context) : hero.hp + (eventDamage*ITEM_SOULDRINKER_LIFESTEAL).ceil();
        event.lifesteal = (eventDamage*ITEM_SOULDRINKER_LIFESTEAL).ceil();
        hero.setHp(healHp);
      }
      if (target.returnDamage) {
        Event returnedDamageEvent = Event();
        if (target.returnDamageQuantity == 0) {
          target.returnDamageQuantity = (eventDamage * ITEM_CARAPACE_RETURN).ceil();
        }
        int newHpLeft = hero.hp - target.returnDamageQuantity;
        hero.setHp(newHpLeft);
        setEventData(target, returnedDamageEvent, 'counter attacks', null, hero, target.returnDamageQuantity, newHpLeft, context);
        lazyEvents.add(returnedDamageEvent);
        if (target.heroClass == 'sorceress') {
          target.returnDamage = false;
        }
        if (newHpLeft < 0) {
          triggeredResurrection = triggerAbilities(context, 'triggerOnOwnerDeath', hero, target, targets, event, events);
          keepAttacking = false;
        }
      }
      if (hpLeft <= 0) {
        triggeredResurrection = triggerAbilities(context, 'triggerOnOwnerDeath', hero, target, targets, event, events);
        if (!triggeredResurrection) {
          triggeredGlorybearer = triggerAbilities(context, 'triggerOnAllyDeath', hero, target, targets, event, events);
        }
      }
      setEventData(hero, event, 'attacks', abilityTriggered, target, eventDamage, hpLeft, context);
      if (!triggeredGlorybearer) {
        triggerAbilities(context, 'triggerOnInflictingDamage', hero, target, targets, event, events);
        target.setHp(hpLeft);
        events.add(event);
      }
      if (triggeredResurrection) {
        triggerResurrection(context, target, events);
      }
      events.addAll(lazyEvents);
      i++;
    }
  }

  bool triggerAbilities(context, String property, Bandit.Hero hero, Bandit.Hero target, List<Bandit.Hero> targets, Event event, List<Event> events) {
    switch (property) {
      case 'triggerOnTargetingSpell':
        {
          abilities.forEach((ability) {
            if (ability.canTrigger && ability.triggerOnTargetingSpell && ability.owner.heroClass == 'warlock' && ability.owner.id == hero.id && ability.owner.hp > 0) {
              abilities.forEach((abilityToDisable) {
                if (abilityToDisable.owner.id == target.id && ability.isPassive) {
                  abilityToDisable.canTrigger = false;
                  target.hasPassiveDisabled = true;
                  event.abilityTriggered = ability.name;
                }
              });
              return true;
            }
          });
          return false;
        }
      case 'triggerOnSpellCast':
        {
          var rng = Random();
          int roll = rng.nextInt(100);
          abilities.forEach((ability) {
            if (ability.canTrigger && ability.triggerOnSpellCast && ability.owner.heroClass == 'sorceress' && ability.owner.id == hero.id && ability.owner.hp > 0 && roll < ability.triggerChance) {
              hero.hasAdditionalTarget = true;
              return true;
            }
            if (ability.canTrigger && ability.triggerOnSpellCast && ability.name.toLowerCase() == 'spark' && ability.owner.hp > 0) {
              int damage = (hero.intelligence * ITEM_THUNDERFURY_DAMAGE).ceil();
              String originOwner = attackerParty.contains(ability.owner) ? attackerUsername : defenderUsername;
              String targetOwner = attackerParty.contains(hero) ? attackerUsername : defenderUsername;
              int hpLeft = hero.hp - damage;
              hero.setHp(hpLeft);

              Event sparkEvent = Event(
                originOwner: originOwner,
                origin: ability.owner,
                target: hero,
                targetsOwner: targetOwner,
                action: 'zaps',
                abilityTriggered: ability.name,
                damage: damage,
                hpLeft: hpLeft,
              );
              sparkEvent.setDescription(context, attackerUsername);
              events.add(sparkEvent);
              if (hpLeft < 0) {
                triggerAbilities(context, 'triggerOnOwnerDeath', null, hero, null, event, events);
              }
              return false;
            }
          });
          return false;
        }
      case 'triggerOnBattleEnd':
        {
          abilities.forEach((ability) {
            if (ability.triggerOnBattleEnd && ability.name.toLowerCase() == 'payday' && winnerParty.contains(ability.owner)) {
              int loot = ability.owner.postBattleReward;
              Provider.of<Resources>(context, listen: false).setGold(loot);
              rewards = 'PAYDAY! $winner looted $loot gold!';
              return true;
            }
          });
          return false;
        }
      case 'triggerOnBattleStart':
        {
          abilities.forEach((ability) {
            if (ability.canTrigger && ability.triggerOnBattleStart && ability.owner.heroClass == 'assassin' && ability.owner.hp > 0) {
              ability.owner.criticalModifier += 100;
              List<Bandit.Hero> backstabTarget = calculateTargets(ability.owner, 'backstabs');
              attack(context, ability.owner, backstabTarget, events, abilityTriggered: ability);
              ability.owner.criticalModifier -=100;
              ability.canTrigger = false;
              return true;
            }
          });
          return false;
        }
      case 'triggerOnNotReceivingDamage':
        {
          abilities.forEach((ability) {
            if (ability.canTrigger && ability.triggerOnNotReceivingDamage && hero.receivedDamageThisRound == false && hero.heroClass == 'sniper' && ability.owner.hp > 0) {
              event.abilityTriggered = ability.name;
              hero.criticalModifier += ABILITY_SNIPER_AIM;
              hero.cleanCriticalModifier = true;
              return true;
            }
          });
          return false;
        }
      case 'triggerOnOwnerDeath':
        {
          int i = 0;
          bool triggered = false;
          while(i < abilities.length && !triggered) {
            Ability ability = abilities[i];
            if (ability.canTrigger && ability.triggerOnOwnerDeath && ability.name.toLowerCase() == 'resurrection' && ability.owner.id == target.id) {
              ability.canTrigger = false;
              triggered = true;
            }
            i++;
          }
          return triggered;
        }
      case 'triggerOnAllyDeath':
        {
          int i = 0;
          bool triggered = false;
          while(i < abilities.length && !triggered) {
            Ability ability = abilities[i];
            if (ability.owner.heroClass == 'shieldbearer' && !ability.owner.isStunned) {
              List<Bandit.Hero> party;
              party = attackerParty.contains(ability.owner) ? attackerParty : defenderParty;
              if (ability.canTrigger && ability.triggerOnAllyDeath && target.id != ability.owner.id && party.contains(target) && ability.owner.hp > 0) {
                event.abilityTriggered = ability.name;
                List<Bandit.Hero> newTarget = [ability.owner];
                attack(context, hero, newTarget, events, abilityTriggered: ability);
                if (hero.hp > 0 && ability.owner.hp <= 0 && targets.contains(ability.owner)) {
                  List<Bandit.Hero> putTargetBack = [target];
                  attack(context, hero, putTargetBack, events);
                  targets.remove(ability.owner);
                }
                ability.canTrigger = false;
                triggered = true;
              }
            }
            i++;
          }
          return triggered;
        }
      case 'triggerOnReceivingDamage':
        {
          abilities.forEach((ability) {
            if (ability.canTrigger && ability.triggerOnReceivingDamage && target.heroClass == 'warlord' && ability.owner.hp > 0 && ability.owner.id == target.id) {
              event.abilityTriggered = ability.name;
              target.hasAdditionalTarget = true;
              return true;
            }
          });
          return false;
        }
      case 'triggerOnInflictingDamage':
        {
          var rng = Random();
          int roll = rng.nextInt(100);
          abilities.forEach((ability) {
            if (ability.canTrigger && ability.triggerOnInflictingDamage && hero.heroClass == ability.owner.heroClass && ((hero.heroClass == 'huntress' && ability.name.toLowerCase() == 'marked') || (ability.name.toLowerCase() == 'desolation'))) {
              event.abilityTriggered = ability.name;
              targets.forEach((target) {
                target.armorModifier += ABILITY_HUNTRESS_MARK;
              });
              return true;
            }
            if (ability.canTrigger && ability.triggerOnInflictingDamage && hero.heroClass == ability.owner.heroClass && ability.name.toLowerCase() == 'lifesteal') {
              event.abilityTriggered = ability.name;
              hero.armorModifier += ITEM_SOULDRINKER_ARMOR_MOD;
              return true;
            }
            if (ability.canTrigger && ability.triggerOnInflictingDamage && hero.heroClass == ability.owner.heroClass && ability.name.toLowerCase() == 'stun' && roll < ability.triggerChance) {
              target.isStunned = true;
              event.abilityTriggered = ability.name;
              return true;
            }
          });
          return false;
        }
      default:
        {
          return false;
        }
    }
  }

  void triggerBattleEnd(context) {
    triggerAbilities(context, 'triggerOnBattleEnd', null, null, null, null, null);
  }

  void triggerBattleStart(context, List<Event> events) {
    triggerAbilities(context, 'triggerOnBattleStart', null, null, null, null, events);
  }

  void setEventData(Bandit.Hero hero, Event event, String action, Ability abilityTriggered, target, damage, hpLeft, context) {
    if (attackerParty.contains(hero)) {
      event.originOwner = attackerUsername;
      event.targetsOwner = defenderUsername;
    } else {
      event.originOwner = defenderUsername;
      event.targetsOwner = attackerUsername;
    }
    if (abilityTriggered != null) {
      event.abilityTriggered = abilityTriggered.name;
    }
    event.target = target;
    event.origin = hero;
    event.action = action;
    event.damage = damage;
    event.hpLeft = hpLeft;
    event.setDescription(context, attackerUsername);
  }

  List<Bandit.Hero> calculateTargets(Bandit.Hero hero, String action) {
    List<Bandit.Hero> targets = [];
    List<Bandit.Hero> possibleTargets = [];
    int maxTargets = 0;
    if (action == 'attacks') {
      maxTargets = 1 + hero.additionalTargetsPerItems;
      if (hero.hasAdditionalTarget) {
        maxTargets++;
        hero.hasAdditionalTarget = false;
      }
      if (attackerParty.contains(hero)) {
        defenderParty.forEach((bandit) {
          if (bandit.hp > 0){
            possibleTargets.add(bandit);
          }
        });
      } else {
        attackerParty.forEach((bandit) {
          if (bandit.hp > 0){
            possibleTargets.add(bandit);
          }
        });
      }
      Bandit.Hero heroTaunting = findTaunt(possibleTargets);
      int i = 0;
      var rng = Random();
      while(possibleTargets.isNotEmpty && i < maxTargets) {
        if (heroTaunting != null) {
          targets.add(heroTaunting);
          possibleTargets.remove(heroTaunting);
          heroTaunting = findTaunt(possibleTargets);
        } else {
          Bandit.Hero target = findRandom(possibleTargets);
          targets.add(target);
          possibleTargets.remove(target);
        }
        i++;
      }
    }
    if (action == 'backstabs') {
      if (attackerParty.contains(hero)) {
        targets.add(findTheSlowest(defenderParty));
      } else {
        targets.add(findTheSlowest(attackerParty));
      }
    }
    return targets;
  }

  Bandit.Hero findTaunt(List<Bandit.Hero> possibleTargets) {
    int i = 0;
    bool found = false;
    Bandit.Hero owner;
    while(i<abilities.length && !found) {
      Ability ability = abilities[i];
      if (ability.canTrigger && ability.triggerOnCalculatingTargets && ability.isTaunt && possibleTargets.contains(ability.owner)) {
        owner = ability.owner;
        found = true;
        ability.canTrigger = false;
      }
      i++;
    }
    return owner;
  }

  int calculateDamage(Bandit.Hero attacker, Bandit.Hero defender, Event event, context) {
    if (event.isCritical == null) {
      var rng = Random();
      int roll = rng.nextInt(100);
      event.isCritical = roll < 10 + attacker.criticalModifier ? true : false;
    }

    int armor = defender.getArmor(context);
    double mitigator = 1 - (0.06*armor)/(1+0.06*armor);
    int damage = ((attacker.weaponDamage(context) + attacker.attributeDamage(context))* mitigator).toInt();

    return event.isCritical ? (damage * CRITICAL_DAMAGE_MULTIPLIER).toInt() : damage;
  }

  void triggerResurrection(context, Bandit.Hero target, List<Event> events) {
    int resurrectedHp = (target.maxHp(context)*ABILITY_PALADIN_HEALTH).floor();
    target.setHp(resurrectedHp);
    String originOwner = attackerParty.contains(target) ? attackerUsername : defenderUsername;
    Event resurrectionEvent = Event(
        originOwner: originOwner,
        origin: target,
        action: 'heals',
        damage: resurrectedHp,
        abilityTriggered: 'Resurrection',
        isCritical: false,
        hpLeft: target.hp
    );
    resurrectionEvent.setDescription(context, attackerUsername);
    events.add(resurrectionEvent);
  }

  void reactivatePassive(Bandit.Hero hero) {
    if (hero.hasPassiveDisabled) {
      hero.hasPassiveDisabled = false;
      int i = 0;
      bool found = false;
      while (i < abilities.length && !found) {
        if (abilities[i].owner.id == hero.id) {
          abilities[i].canTrigger = true;
          found = true;
        }
        i++;
      }
    }
  }

  bool isPriestAndCanAttack(Bandit.Hero hero) {
    if (hero.heroClass != 'priest') {
      return false;
    }
    int i = 0;
    bool found = false, canAttack = true;
    while (i < abilities.length && !found) {
      if (abilities[i].owner.id == hero.id) {
        canAttack = abilities[i].canTrigger;
      }
      i++;
    }
    return canAttack;
  }
}
