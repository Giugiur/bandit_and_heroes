import 'dart:ui';
import 'package:flutter/material.dart';
import 'battle_record.dart';
import 'expandable_round.dart';
import '../bandit_bar/bandit_bar.dart';

class BattleDetailScreen extends StatefulWidget {
  static const routeName = '/battle_detail';

  @override
  _BattleDetailState createState() => _BattleDetailState();
}

class _BattleDetailState extends State<BattleDetailScreen> {
  var _isLoading = false;
  bool isWinnerExpanded = false;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    BattleRecord battle = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        padding: const EdgeInsets.all(20),
        height: deviceSize.height,
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                RichText(
                  text: TextSpan(
                  children: [
                    TextSpan(
                      text: '${battle.attackerUsername} ',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 24.0,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    TextSpan(
                      text: 'vs. ',
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    TextSpan(
                      text: '${battle.defenderUsername}',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 24.0,
                        decoration: TextDecoration.none,
                      )
                    ),
                  ]),
                ),
                if (battle.defenderParty.isEmpty)
                  Container(
                    padding: const EdgeInsets.all(20),

                    child: Text(
                      '${battle.defenderUsername}\'s Party was empty.\nThis was easy!',
                      style: Theme.of(context).textTheme.bodyText1,
                      textAlign: TextAlign.center,
                    ),
                  ),
                Container(
                  child: ListView(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      Container(
                        child: ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (ctx, roundIndex) => ExpandableRound(battle.rounds[roundIndex]),
                          itemCount: battle.rounds.length,
                        ),
                      ),
                      Container(
                        child: ListView(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: [
                            Container(
                              width: deviceSize.width,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isWinnerExpanded = !isWinnerExpanded;
                                  });
                                },
                                child: Row(
                                  children: [
                                    TextButton(
                                      child: Container(
                                        child: Text(
                                          'Reveal Winner',
                                          style: Theme.of(context).textTheme.headline3,
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Icon(isWinnerExpanded ? Icons.arrow_drop_down : Icons.arrow_right),
                                  ]
                                ),
                              ),
                            ),
                            if (isWinnerExpanded) Container(
                                child: Text(
                                  'Winner is: ${battle.winner}',
                                  style: TextStyle(
                                    color: battle.winner == battle.attackerUsername ? Colors.red : Colors.blue,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                  textAlign: TextAlign.center,
                                )
                            ),
                            if (isWinnerExpanded && battle.rewards != null) Text(
                              battle.rewards,
                            )
                          ]
                        ),
                      ),
                    ]
                  ),
                ),
              ]
            ),
          ),
        ),
      ),
    );
  }
}