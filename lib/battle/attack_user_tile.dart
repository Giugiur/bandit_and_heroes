import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'battle_detail_screen.dart';
import 'battle_record.dart';
import 'battles.dart';
import '../auth/auth.dart';
import '../battle/battle.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/heroes.dart';
import '../resources/resources.dart';
import '../subscription/subscriptions.dart';
import '../utils/custom_icons.dart';
import '../widgets/announcer.dart';

class AttackUserTile extends StatefulWidget {
  final String username;
  final int points;
  final String id;

  AttackUserTile(this.id, this.username, this.points);

  @override
  _AttackUserTileState createState() => _AttackUserTileState();
}

class _AttackUserTileState extends State<AttackUserTile> {
  bool _isLoading = false;

  void _beginBattle(context) async {
    String battleId = Uuid().v1();
    String timestamp = DateTime.now().toIso8601String();
    String userId = Provider.of<Auth>(context, listen: false).userId;
    await Provider.of<Resources>(context, listen: false).setSapphires(-1);
    List<Bandit.Hero> attackerParty = new List<Bandit.Hero>.from(Provider.of<Heroes>(context, listen: false).party);
    List<Bandit.Hero> defenderParty = new List<Bandit.Hero>.from(await Provider.of<Heroes>(context, listen: false).getPartyById(widget.id));
    String attackerUsername = Provider.of<Auth>(context, listen: false).username;
    Battle battle = Battle(battleId, timestamp, userId, widget.id, attackerParty, defenderParty, attackerUsername, widget.username);
    setState(() {
      _isLoading = true;
    });
    await battle.fight(context);
    BattleRecord battleRecord = await Provider.of<Battles>(context, listen: false).getBattle(context, battleId);
    setState(() {
      _isLoading = false;
    });
    Navigator.of(context).pushReplacementNamed(BattleDetailScreen.routeName, arguments: battleRecord);
  }

  void _beginAttack(context, String username) {
    var announcer = Announcer(context);
    if (Provider.of<Resources>(context, listen: false).sapphires > 0) {
      if (Provider.of<Heroes>(context, listen: false).party.isNotEmpty) {
        announcer.showActionMessage(
            'Begin attack on $username?',
            'Attacking will cost 1 Sapphire.',
            'Attack',
                () => _beginBattle(context)
        );
      } else {
        announcer.showErrorMessage(
          'You can not attack with an empty Party!',
          'Try dragging some heroes from the Bench to your Party on the Heroes screen.'
        );
      }
    } else {
      announcer.showErrorMessage(
        'Could not perform attack.',
        'You do not have enough sapphires.'
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool isPremium = Provider.of<Subscription>(context, listen: false).isPremium;

    return Card(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        child: Material(
          elevation: 5,
          child: ListTile(
            title: Text(
              widget.username,
            ),
            subtitle: isPremium? Text(widget.points.toString()): null,
            trailing: InkWell(
              child: _isLoading ? CircularProgressIndicator() : Icon(CustomIcons.crossed_swords),
              onTap: () => _beginAttack(context, widget.username),
            ),
          ),
        ),
    );
  }
}
