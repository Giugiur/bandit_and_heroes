import 'package:flutter/material.dart';
import '../heroes/hero.dart' as Bandit;
import '../utils/extensions.dart';

class Event {
  Bandit.Hero origin;
  String originOwner;
  Bandit.Hero target;
  String targetsOwner;
  String action;
  int damage;
  int hpLeft;
  int lifesteal;
  bool isCritical;
  Widget description;
  String abilityTriggered;
  String spellName;
  String additionalAction;
  String stat;
  int modifier;
  String stat2;
  int modifier2;
  bool stunned;

  Event({
    this.origin,
    this.originOwner,
    this.target,
    this.targetsOwner,
    this.action,
    this.damage,
    this.hpLeft,
    this.isCritical,
    this.description,
    this.abilityTriggered,
    this.spellName,
    this.additionalAction,
    this.stat,
    this.modifier,
    this.stat2,
    this.modifier2,
    this.stunned,
  });

  void setDescriptionStatMod(context, String attackerUsername) {
    description = RichText(
      text: TextSpan(
        children: [
          WidgetSpan(
            child: Icon(origin.icon, size: 19),
          ),
          TextSpan(
            text: " $originOwner ${origin.heroClass.capitalize().removeUnderscore()}",
            style: TextStyle(
              color: originOwner == attackerUsername ? Colors.red : Colors.blue,
              fontSize: 16,
            ),
          ),
          TextSpan(
            text: " casts $spellName $action ",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          TextSpan(
            text: target != null ? "$targetsOwner's ${target.heroClass.capitalize().removeUnderscore()}'s" : null,
            style: TextStyle(
              color: targetsOwner == attackerUsername ? Colors.red : Colors.blue,
              fontSize: 16,
            ),
          ),
          TextSpan(
            text: " $stat by $modifier",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (stat2 != null) TextSpan(
            text: " and $stat2 by $modifier2",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (additionalAction == null) TextSpan(
            text: ".",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (additionalAction != null )TextSpan(
            text: " and $additionalAction them.",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (abilityTriggered != null) TextSpan(
            text: " ${abilityTriggered.toUpperCase()}!",
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }

  void setDescriptionSpell(context, String attackerUsername) {
    description =
      RichText(
        text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(origin.icon, size: 19),
              ),
              TextSpan(
                text: " $originOwner's ${origin.heroClass.capitalize().removeUnderscore()}",
                style: TextStyle(
                  color: originOwner == attackerUsername ? Colors.red : Colors.blue,
                  fontSize: 16,
                ),
              ),
              TextSpan(
                text: " casts ",
                style: Theme.of(context).textTheme.bodyText1,
              ),
              TextSpan(
                text: "$spellName $action.",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ]
        )
      );
  }

  void setDescription(context, String attackerUsername) {
    description = stunned != null && stunned ?
    RichText(
      text: TextSpan(
        children: [
          WidgetSpan(
            child: Icon(origin.icon, size: 19),
          ),
          TextSpan(
            text: " $originOwner's ${origin.heroClass.capitalize().removeUnderscore()}",
            style: TextStyle(
              color: originOwner == attackerUsername ? Colors.red : Colors.blue,
              fontSize: 16,
            ),
          ),
          TextSpan(
            text: " is STUNNED!",
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ]
      )
    )
    : RichText(
      text: TextSpan(
        children: [
          WidgetSpan(
            child: Icon(origin.icon, size: 19),
          ),
          TextSpan(
            text: " $originOwner's ${origin.heroClass.capitalize().removeUnderscore()}",
            style: TextStyle(
              color: originOwner == attackerUsername ? Colors.red : Colors.blue,
              fontSize: 16,
            ),
          ),
          TextSpan(
            text: " $action ",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (spellName != null) TextSpan(
            text: "$spellName on ",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          TextSpan(
            text: target != null ? "$targetsOwner's ${target.heroClass.capitalize().removeUnderscore()}" : null,
            style: TextStyle(
              color: targetsOwner == attackerUsername ? Colors.red : Colors.blue,
              fontSize: 16,
            ),
          ),
          TextSpan(
            text: " for $damage damage, leaving them with $hpLeft hit points left",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (lifesteal != null) TextSpan(
            text: " and healing for $lifesteal",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          // if (target != null && target.returnDamage != null) TextSpan(
          //   text: " and returning damage for ${target.returnDamageQuantity}",
          //   style: Theme.of(context).textTheme.bodyText1,
          // ),
          if (additionalAction == null) TextSpan(
            text: '.',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (additionalAction != null) TextSpan(
            text: ' $additionalAction.',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (abilityTriggered != null) TextSpan(
            text: " ${abilityTriggered.toUpperCase()}!",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (isCritical != null && isCritical) TextSpan(
            text: ' CRITICAL!',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (hpLeft != null && hpLeft <= 0) TextSpan(
            text: ' DEAD!',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }

}