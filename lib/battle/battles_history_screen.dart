import 'package:bandit_and_heroes/battle/battle_card.dart';
import 'package:bandit_and_heroes/widgets/empty_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'battle_record.dart';
import 'battles.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';

class BattlesHistoryScreen extends StatefulWidget {
  static const routeName = '/battles_history';

  @override
  _BattlesHistoryScreenState createState() => _BattlesHistoryScreenState();
}

class _BattlesHistoryScreenState extends State<BattlesHistoryScreen> {
  bool _isLoading = false;
  List<BattleRecord> _battleRecords = [];

  void didChangeDependencies() {
    setState(() {
      _isLoading = true;
    });
    Provider.of<Battles>(context, listen: false).getLastBattles(context).then((response) {
      setState(() {
        _battleRecords = response;
      });
    }).whenComplete(() => setState(() { _isLoading = false; }));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BanditBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator()) :
      Container(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Text(
                'Battle Records',
                style: Theme.of(context).textTheme.headline2,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 50,),
              _battleRecords.isNotEmpty ?
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (ctx, index) => Container(
                    margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
                    child: BattleCard(_battleRecords[index])
                ),
                itemCount: _battleRecords.length,
              )  : EmptyState('There are no battle records to show for now.'),
            ],
          )
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
