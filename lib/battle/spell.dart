import 'battle.dart';
import 'battle_helpers.dart';
import 'event.dart';
import '../heroes/hero.dart' as Bandit;
import '../utils/constants.dart';

class Spell {
  int originalMaxTargets;
  String name;

  Spell({
    this.name,
    this.originalMaxTargets
  });

  List<Bandit.Hero> calculateSpellTargets(Battle battle, Bandit.Hero hero, int maxTargets, List<Bandit.Hero> attackerParty, List<Bandit.Hero> defenderParty, bool friendly, {String criteria}) {
    List<Bandit.Hero> targets = [];
    List<Bandit.Hero> clonedParty;
    int i = 0;
    if (attackerParty.contains(hero)) {
      if (friendly) {
        clonedParty = new List<Bandit.Hero>.from(attackerParty);
      } else {
        clonedParty = new List<Bandit.Hero>.from(defenderParty);
      }
    }
    if (defenderParty.contains(hero)) {
      if (friendly) {
        clonedParty = new List<Bandit.Hero>.from(defenderParty);
      } else {
        clonedParty = new List<Bandit.Hero>.from(attackerParty);
      }
    }
    while(i < maxTargets && clonedParty.isNotEmpty) {
      Bandit.Hero target;
      if (criteria == 'random' || criteria == null) {
        target = findRandom(clonedParty);
      }
      if (criteria == 'highestStrOrStunned') {
        clonedParty.forEach((hero) {
          if (hero.isStunned) {
            if (target == null) {
              target = hero;
            } else if (target.strength < hero.strength ) {
              target = hero;
            }
          }
        });
        if (target == null) {
          target = findTheHighestStrength(clonedParty);
        }
      }
      if (criteria == 'lowestHP') {
        target = findTheLowestHP(clonedParty);
      }
      if (criteria == 'slowest') {
        target = findTheSlowest(clonedParty);
      }
      if (target != null) {
        targets.add(target);
        clonedParty.removeWhere((hero) => hero.id == target.id);
      }
      i++;
    }
    return targets;
  }

  void cast(context, Battle battle, Bandit.Hero hero, List<Bandit.Hero> attackerParty, List<Bandit.Hero> defenderParty, String attackerUsername, String defenderUsername, bool isMulticast, List<Event> events) {
    switch (name.toLowerCase()) {
      case 'hell\'s chains':
        {
          int maxTargets = originalMaxTargets;
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          List<Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, false);
          String originOwner;
          String targetsOwner;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
            targetsOwner = defenderUsername;
          } else {
            originOwner = defenderUsername;
            targetsOwner = attackerUsername;
          }
          int i = 0;
          while (i < targets.length) {
            Bandit.Hero target = targets[i];
            int damage = (hero.attributeDamage(context) * SPELL_WARLOCK_CHAINS_DAMAGE).ceil();
            int dexterityLoss = (hero.attributeDamage(context)  * SPELL_WARLOCK_CHAINS_STAT).ceil();
            int hpLeft = target.hp - damage;
            target.setHp(hpLeft);
            target.setStat('dexterity', target.dexterity - dexterityLoss);
            Event event = Event(
                spellName: 'Hell\'s Chains',
                action: 'casts',
                origin: hero,
                originOwner: originOwner,
                target: target,
                targetsOwner: targetsOwner,
                damage: damage,
                hpLeft: hpLeft,
                isCritical: false,
                additionalAction: 'and causing to lose dexterity',
                abilityTriggered: hero.instantiateAbility.name
            );
            battle.triggerAbilities(context, 'triggerOnTargetingSpell', hero, target, targets, event, events);
            battle.triggerAbilities(context, 'triggerOnInflictingDamage', hero, target, targets, event, events);
            battle.triggerAbilities(context, 'triggerOnReceivingDamage', hero, target, targets, event, events);
            bool triggeredResurrection = false;
            if (target.hp <= 0) {
              triggeredResurrection = battle.triggerAbilities(context, 'triggerOnOwnerDeath', hero, target, targets, event, events);
              battle.triggerAbilities(context, 'triggerOnAllyDeath', hero, target, targets, event, events);
            }
            event.setDescription(context, attackerUsername);
            events.add(event);
            if (triggeredResurrection) {
              battle.triggerResurrection(context, target, events);
            }
            i++;
          }
        }
        break;
      case 'decay':
        {
          int maxTargets = originalMaxTargets;
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          List<Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, false);
          String originOwner;
          String targetsOwner;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
            targetsOwner = defenderUsername;
          } else {
            originOwner = defenderUsername;
            targetsOwner = attackerUsername;
          }
          int i = 0;
          while (i < targets.length) {
            Bandit.Hero target = targets[i];
            int damage = (hero.attributeDamage(context)  * SPELL_WARLOCK_DECAY_DAMAGE).ceil();
            int statLoss = (hero.attributeDamage(context)  * SPELL_WARLOCK_DECAY_STAT).ceil();
            int hpLeft = target.hp - damage;
            target.setHp(hpLeft);
            target.setStat('strength', target.strength - statLoss);
            target.setStat('dexterity', target.dexterity - statLoss);
            target.setStat('intelligence', target.intelligence - statLoss);
            Event event = Event(
              spellName: 'Decay',
              action: 'casts',
              origin: hero,
              originOwner: originOwner,
              target: target,
              targetsOwner: targetsOwner,
              damage: damage,
              hpLeft: hpLeft,
              isCritical: false,
              additionalAction: 'and causing to lose stats',
              abilityTriggered: hero.instantiateAbility.name
            );
            battle.triggerAbilities(context, 'triggerOnTargetingSpell', hero, target, targets, event, events);
            battle.triggerAbilities(context, 'triggerOnInflictingDamage', hero, target, targets, event, events);
            battle.triggerAbilities(context, 'triggerOnReceivingDamage', hero, target, targets, event, events);
            bool triggeredResurrection = false;
            if (target.hp <= 0) {
              triggeredResurrection = battle.triggerAbilities(context, 'triggerOnOwnerDeath', hero, target, targets, event, events);
              battle.triggerAbilities(context, 'triggerOnAllyDeath', hero, target, targets, event, events);
            }
            event.setDescription(context, attackerUsername);
            events.add(event);
            if (triggeredResurrection) {
              battle.triggerResurrection(context, target, events);
            }
            i++;
          }
        }
      break;
      case 'devil\'s speed': {
        String originOwner;
        int maxTargets = originalMaxTargets;
        if (attackerParty.contains(hero)) {
          originOwner = attackerUsername;
        } else {
          originOwner = defenderUsername;
        }
        if (hero.hasAdditionalTarget) {
          maxTargets++;
        }
        List <Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, true, criteria: 'slowest');
        targets.forEach((target) {
          int amountToIncrease = (hero.attributeDamage(context)  * SPELL_WARLOCK_DEVIL_SPEED).ceil();
          int setDexterity =  target.dexterity + amountToIncrease;
          target.setStat('dexterity', setDexterity);
          Event event = Event(
            action: 'increasing',
            spellName: 'Devil\'s Speed',
            target: target,
            stat: 'Dexterity',
            modifier: amountToIncrease,
            origin: hero,
            originOwner: originOwner,
            targetsOwner: originOwner,
            isCritical: false,
          );
          event.setDescriptionStatMod(context, attackerUsername);
          events.add(event);
        });
      }
      break;
      case 'fire decoy':
        {
          String originOwner;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
          } else {
            originOwner = defenderUsername;
          }
          hero.preventIncomingDamage = true;
          hero.preventQuantity = 1;
          hero.returnDamage = true;
          hero.returnDamageQuantity = (hero.attributeDamage(context) * SPELL_SORCERESS_FIRE_DECOY).ceil();
          Event event = Event(
            origin: hero,
            originOwner: originOwner,
            spellName: 'Fire Decoy',
            action: 'preventing the next attack targeting her and returning damage'
          );
          event.setDescriptionSpell(context, attackerUsername);
          events.add(event);
        }
        break;
      case 'lightning strike':
        {
          int maxTargets = originalMaxTargets;
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          List<Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, false);
          String originOwner;
          String targetsOwner;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
            targetsOwner = defenderUsername;
          } else {
            originOwner = defenderUsername;
            targetsOwner = attackerUsername;
          }
          int i = 0;
          while (i < targets.length) {
            Bandit.Hero target = targets[i];
            int damage = (hero.attributeDamage(context)  * SPELL_SORCERESS_LIGHTNING_STRIKE).ceil();
            int hpLeft = target.hp - damage;
            target.setHp(hpLeft);
            Event event = Event(
              spellName: 'Lightning Strike',
              action: 'casts',
              origin: hero,
              originOwner: originOwner,
              target: target,
              targetsOwner: targetsOwner,
              damage: damage,
              hpLeft: hpLeft,
              isCritical: false,
              abilityTriggered: hero.hasAdditionalTarget && i >= originalMaxTargets ? 'Multicast' : null,
            );
            battle.triggerAbilities(context, 'triggerOnInflictingDamage', hero, target, targets, event, events);
            battle.triggerAbilities(context, 'triggerOnReceivingDamage', hero, target, targets, event, events);
            bool triggeredResurrection = false;
            if (target.hp <= 0) {
              triggeredResurrection = battle.triggerAbilities(context, 'triggerOnOwnerDeath', hero, target, targets, event, events);
              battle.triggerAbilities(context, 'triggerOnAllyDeath', hero, target, targets, event, events);
            }
            event.setDescription(context, attackerUsername);
            events.add(event);
            if (triggeredResurrection) {
              battle.triggerResurrection(context, target, events);
            }
            i++;
          }
        }
        break;
      case 'frostbite':
        {
          String originOwner;
          String targetsOwner;
          List<Bandit.Hero> targets = [];
          int maxTargets = originalMaxTargets;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
            targetsOwner = defenderUsername;
          } else {
            originOwner = defenderUsername;
            targetsOwner = attackerUsername;
          }
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, false);
          int amountToDecrease = (hero.attributeDamage(context)  * SPELL_SORCERESS_FROSTBITE_MOD).ceil(),
          i = 0;
          while(i<targets.length) {
            Bandit.Hero target = targets[i];
            int setStrength =  target.strength - amountToDecrease > 0 ? target.strength - amountToDecrease : 0;
            int setDexterity =  target.dexterity - amountToDecrease > 0 ? target.dexterity - amountToDecrease  : 0;
            target.isStunned = true;
            target.setStat('strength', setStrength);
            target.setStat('dexterity', setDexterity);
            Event event = Event(
              action: 'decreasing',
              additionalAction: 'stunning',
              spellName: 'Frostbite',
              target: target,
              abilityTriggered: hero.hasAdditionalTarget && i >= originalMaxTargets ? 'Multicast' : null,
              stat: 'Strength',
              modifier: amountToDecrease,
              stat2: 'Dexterity',
              modifier2: amountToDecrease,
              origin: hero,
              originOwner: originOwner,
              targetsOwner: targetsOwner,
              isCritical: false,
            );
            event.setDescriptionStatMod(context, attackerUsername);
            events.add(event);
            i++;
          }
        }
        break;
      case 'heavenly strength':
        {
          String originOwner;
          int maxTargets = originalMaxTargets;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
          } else {
            originOwner = defenderUsername;
          }
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          List<Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, true, criteria: 'highestStrOrStunned');
          int amountToIncrease = (hero.attributeDamage(context)  * SPELL_PRIEST_HEAVENLY_STRENGTH).ceil();
          targets.forEach((target) {
            int setStrength =  target.strength + amountToIncrease;
            target.setStat('strength', setStrength);
            target.dispel();
            Event event = Event(
              action: target.isStunned ? 'dispelling stun and increasing' : 'increasing',
              spellName: 'Heavenly Strength',
              target: target,
              stat: 'Strength',
              modifier: amountToIncrease,
              origin: hero,
              originOwner: originOwner,
              targetsOwner: originOwner,
              isCritical: false,
            );
            event.setDescriptionStatMod(context, attackerUsername);
            events.add(event);
          });
        }
        break;
      case 'hands imposition':
        {
          String originOwner;
          int maxTargets = originalMaxTargets;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
          } else {
            originOwner = defenderUsername;
          }
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          List<Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, true, criteria: 'lowestHP');
          int amountToHeal = (hero.attributeDamage(context) * SPELL_PRIEST_HANDS).ceil();
          targets.forEach((target) {
            int setHp =  target.maxHp(context) < amountToHeal + target.hp ? target.maxHp(context) : amountToHeal + target.hp;
            target.setHp(setHp);
            Event event = Event(
              action: 'healing',
              spellName: 'Hands Imposition',
              target: target,
              stat: 'hit points',
              modifier: amountToHeal,
              origin: hero,
              originOwner: originOwner,
              targetsOwner: originOwner,
              isCritical: false,
            );
            event.setDescriptionStatMod(context, attackerUsername);
            events.add(event);
          });
        }
        break;
      case 'blessing':
        {
          String originOwner;
          int maxTargets = originalMaxTargets;
          if (attackerParty.contains(hero)) {
            originOwner = attackerUsername;
          } else {
            originOwner = defenderUsername;
          }
          if (hero.hasAdditionalTarget) {
            maxTargets++;
          }
          List <Bandit.Hero> targets = calculateSpellTargets(battle, hero, maxTargets, attackerParty, defenderParty, true);
          targets.forEach((target) {
            int criticalMod = (hero.attributeDamage(context)  * SPELL_PRIEST_BLESS_CRIT).ceil();
            int armorMod = (hero.attributeDamage(context)  * SPELL_PRIEST_BLESS_ARMOR).ceil();
            target.criticalModifier += criticalMod;
            target.armorModifier += armorMod;
            Event event = Event(
              action: 'increasing',
              spellName: 'Blessing',
              target: target,
              stat: 'Critical Chance',
              modifier: criticalMod,
              stat2: 'Armor',
              modifier2: armorMod,
              origin: hero,
              originOwner: originOwner,
              targetsOwner: originOwner,
              isCritical: false,
            );
            event.setDescriptionStatMod(context, attackerUsername);
            events.add(event);
          });
        }
        break;
    }
  }
}