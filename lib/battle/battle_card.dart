import 'package:bandit_and_heroes/auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:expansion_card/expansion_card.dart';
import 'package:timeago/timeago.dart' as timeAgo;
import 'battle_record.dart';
import 'battle_detail_screen.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/hero_tile.dart';
import '../utils/extensions.dart';
import '../utils/helpers.dart';

class BattleCard extends StatelessWidget {
  BattleRecord _battleRecord;

  BattleCard(this._battleRecord);

  @override
  Widget build(BuildContext context) {
    final difference =  DateTime.now().difference(DateTime.parse(_battleRecord.timestamp));
    final when = DateTime.now().subtract(difference);
    final deviceSize = MediaQuery.of(context).size;
    final String username = Provider.of<Auth>(context, listen: false).username;
    final String against = username == _battleRecord.attackerUsername ? _battleRecord.defenderUsername : _battleRecord.attackerUsername;
    final String result = _battleRecord.winner == username ? 'victory' : 'defeat';

    return ExpansionCard(
      borderRadius: 25,
      background: Opacity(
          child: Image.asset(
            'assets/images/$result.jpg',
          ),
          opacity: 0.2,
        ),
      title: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Text(
              '${result.capitalize()} vs. $against',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              timeAgo.format(when),
              style: Theme.of(context).textTheme.bodyText1,
            )
          ],
        ),
      ),
      children: <Widget>[
         Container(
          margin: EdgeInsets.symmetric(horizontal: 7),
          child: Row(
            children: [
              Expanded(
                flex: 40,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (ctx, index) => HeroTile(_battleRecord.attackerParty[index], true, false),
                  itemCount: _battleRecord.attackerParty.length,
                ),
              ),
              Expanded(
                flex: 20,
                child: Center(child: Text('vs.')),
              ),
              Expanded(
                flex: 40,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (ctx, index) => HeroTile(_battleRecord.defenderParty[index], true, true),
                  itemCount: _battleRecord.defenderParty.length,
                ),
              )
            ],
          )
        ),
        Container(
          child: TextButton(
            child: Text(
              'Battle Details',
              style: TextStyle(
                color: Theme.of(context).accentColor
              ),
            ),
            onPressed: () => Navigator.of(context).pushNamed(
              BattleDetailScreen.routeName,
              arguments: _battleRecord
            ),
          ),
        ),
      ],
    );
  }
}