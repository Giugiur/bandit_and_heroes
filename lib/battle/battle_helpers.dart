import 'dart:math';
import '../heroes/hero.dart' as Bandit;

Bandit.Hero findTheLowestHP(List<Bandit.Hero> party) {
  int lowestHp = 99999;
  Bandit.Hero ret = party[0];
  party.forEach((hero) {
    if (hero.hp > 0 && hero.hp < lowestHp) {
      lowestHp = hero.hp;
      ret = hero;
    }
  });
  return ret;
}

Bandit.Hero findTheSlowest(List<Bandit.Hero> party) {
  int lowestDexterity = 99999;
  Bandit.Hero ret = party[0];
  party.forEach((hero) {
    if (hero.hp > 0 && hero.dexterity < lowestDexterity) {
      lowestDexterity = hero.dexterity;
      ret = hero;
    }
  });
  return ret;
}

Bandit.Hero findTheHighestStrength(List<Bandit.Hero> party) {
  int highestStrength = 1;
  Bandit.Hero ret = party[0];
  party.forEach((hero) {
    if (hero.hp > 0 && hero.strength > highestStrength) {
      highestStrength = hero.strength;
      ret = hero;
    }
  });
  return ret;
}

Bandit.Hero findRandom(List<Bandit.Hero> party) {
  var rng = Random();
  List<Bandit.Hero> clonedParty = new List<Bandit.Hero>.from(party);
  while (clonedParty.isNotEmpty) {
    int roll = rng.nextInt(clonedParty.length);
    if (clonedParty[roll].hp > 0) {
      return clonedParty[roll];
    } else {
      clonedParty.removeAt(roll);
    }
  }
  return null;
}

bool atLeastOneMemberIsAlive(List<Bandit.Hero> party) {
  bool found = false;
  int i = 0;
  while(!found && i < party.length) {
    if (party[i].hp > 0) {
      found = true;
    }
    i++;
  }
  return found;
}

