import 'package:uuid/uuid.dart';
import 'item.dart';

List<Item> getMythicItems() {
  return [
    Item(
      id: Uuid().v1(),
      name: 'Shinning Star',
      image: "assets/images/items/shinning_star.jpg",
      owner: ' ',
      damage: 0,
      armor: 0,
      type: ItemType.Trinket,
      stats: {
        'strength': 10,
        'intelligence': 10,
        'dexterity': 10,
        'vitality': 10
      },
      rarity: Rarity.Mythic,
      abilityText: ' ',
      isNew: true,
      slot: 'trinket',
    ),
    Item(
      id: Uuid().v1(),
      name: 'Leaf Coat',
      image: "assets/images/items/leaf_coat.jpg",
      owner: ' ',
      damage: 0,
      armor: 12,
      type: ItemType.Armor,
      stats: {
        'strength': 0,
        'dexterity': 20,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Mythic,
      abilityText: ' ',
      isNew: true,
      slot: 'armor',
    ),
    Item(
      id: Uuid().v1(),
      name: 'Gladiator Armor',
      image: "assets/images/items/gladiator_armor.jpg",
      owner: ' ',
      damage: 0,
      armor: 12,
      type: ItemType.Armor,
      stats: {
        'strength': 20,
        'dexterity': 0,
        'intelligence': 0,
        'vitality': 0
      },
      rarity: Rarity.Mythic,
      isNew: true,
      slot: 'armor',
    ),
    Item(
      id: Uuid().v1(),
      name: 'Robes of the Council',
      image: "assets/images/items/robes_of_the_council.jpg",
      owner: ' ',
      damage: 0,
      armor: 12,
      type: ItemType.Armor,
      stats: {
        'strength': 0,
        'dexterity': 0,
        'intelligence': 20,
        'vitality': 0
      },
      rarity: Rarity.Mythic,
      abilityText: ' ',
      isNew: true,
      slot: 'armor',
    ),
    Item(
      id: Uuid().v1(),
      name: 'Battle Chest Plate',
      image: "assets/images/items/battle_chestplate.jpg",
      owner: ' ',
      damage: 0,
      armor: 12,
      type: ItemType.Armor,
      stats: {
        'strength': 0,
        'dexterity': 0,
        'intelligence': 0,
        'vitality': 20
      },
      rarity: Rarity.Mythic,
      abilityText: ' ',
      isNew: true,
      slot: 'armor',
    ),
  ];
}