import 'dart:ffi';
import 'dart:ui';
import 'dart:math';
import 'package:flutter/material.dart';
import 'item.dart';
import 'item_tile.dart';

class ExpandableItemList extends StatefulWidget {

  ExpandableItemList(
    this.itemList,
    this.label,
    this.color,
  );

  final List<Item> itemList;
  final String label;
  final Color color;
  bool expanded = true;

  @override
  _ExpandableItemListState createState() => _ExpandableItemListState();
}

class _ExpandableItemListState extends State<ExpandableItemList> {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Container(
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: [
               Container(
                 width: deviceSize.width,
                 child: InkWell(
                   onTap: () {
                    setState(() {
                      widget.expanded = !widget.expanded;
                    });
                   },
                   child: Row(
                     children: [
                       TextButton(
                         child: Container(
                           child: Text(
                             '${widget.label.toUpperCase()} (${widget.itemList.length})',
                             style: TextStyle(
                                 color: widget.color,
                                 fontSize: 14,
                                 fontWeight: FontWeight.bold
                             ),
                             textAlign: TextAlign.left,
                           ),
                         ),
                       ),
                       Icon(
                         widget.expanded ? Icons.arrow_drop_down : Icons.arrow_right,
                         color: widget.color,
                       )
                     ]
                   ),
                 ),
               ),
              if (widget.expanded)
                SingleChildScrollView(
                  child: Container(
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (ctx, index) => ItemTile(
                        widget.itemList[index],
                        false
                      ),
                      itemCount: widget.itemList.length,
                    ),
                  ),
                ),
            ],
          ),
      );
  }
}