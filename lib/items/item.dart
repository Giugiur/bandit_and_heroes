import 'package:provider/provider.dart';
import '../battle/ability.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/heroes.dart';
import '../utils/extensions.dart';

enum Rarity {
  Common,
  Rare,
  Mythic,
  Legendary
}

enum ItemType {
  Armor,
  Shield,
  One_Handed_Sword,
  Two_Handed_Sword,
  Axe,
  Dagger,
  Bow,
  Crossbow,
  Gun,
  Staff,
  Trinket
}

class Item {
  Item({
    this.id,
    this.name,
    this.image,
    this.owner,
    this.damage,
    this.armor,
    this.type,
    this.stats,
    this.rarity,
    this.ability,
    this.abilityText,
    this.slot,
    this.isNew,
  });

  final String id;
  final String name;
  final String image;
  String owner;
  final Map<String, int> stats;
  final int damage;
  final int armor;
  final ItemType type;
  final Rarity rarity;
  final Ability ability;
  final String abilityText;
  final String slot;
  bool isNew;

  String get itemType {
    return type.toString().parseType();
  }

  bool isWeapon() {
    List<ItemType> weaponTypes = [ItemType.Axe, ItemType.Bow, ItemType.Crossbow, ItemType.Dagger, ItemType.Gun, ItemType.One_Handed_Sword, ItemType.Staff, ItemType.Two_Handed_Sword];
    return weaponTypes.contains(type);
  }

  bool isArmor() {
    List<ItemType> armorTypes = [ItemType.Armor, ItemType.Shield];
    return armorTypes.contains(type);
  }

  bool isEquipped() {
    return owner != ' ';
  }

  int get strength {
    return stats['strength'] != null ? stats['strength'] : 0;
  }

  int get dexterity {
    return stats['dexterity'] != null ? stats['dexterity'] : 0;
  }

  int get vitality {
    return stats['vitality'] != null ? stats['vitality'] : 0;
  }

  int get intelligence {
    return stats['intelligence'] != null ? stats['intelligence'] : 0;
  }
}