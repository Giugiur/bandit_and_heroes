import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;
import 'item.dart';
import 'legendaryItems.dart';
import 'mythicItems.dart';
import '../auth/auth.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/heroes.dart';
import '../resources/resources.dart';
import '../subscription/subscriptions.dart';
import '../utils/constants.dart';
import '../utils/extensions.dart';
import '../utils/helpers.dart';
import '../widgets/announcer.dart';

class Items with ChangeNotifier {

  Items(this.token,
      this.userId,
      this._itemList,
      this._marketItems,
      this.world);

  List<Item> _itemList;
  List<Item> _opponentItemList;
  List<Item> _marketItems;
  final String userId;
  final String token;
  final int world;
  Bandit.Hero heroToEquip;

  List<Item> get itemList {
    return _itemList;
  }

  List<Item> get opponentItemList {
    return _opponentItemList;
  }

  List<Item> get marketItems {
      return _marketItems;
  }

  Future<void> provideOppItemList(context, String oppId) async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$oppId/items.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      List<Item> items = [];
      if (extractedData != null) {
        extractedData.forEach((itemId, item) {
          if (item != null) {
            items.add(buildItem(item));
          }
        });
      }
      _opponentItemList = items;
    } catch (error) {
      throw error;
    }
  }

  Item findById(String id) {
    return _itemList.firstWhere((item) => item.id == id, orElse: () => null);
  }

  void setHeroToEquip(Bandit.Hero hero) {
    heroToEquip = hero;
    notifyListeners();
  }

  bool tryingToEquipShieldOrTwoHand(Item itemToEquip, Item equippedItem) {
    return itemToEquip.type == ItemType.Two_Handed_Sword && equippedItem.type == ItemType.Shield ||
        itemToEquip.type == ItemType.Axe && equippedItem.type == ItemType.Shield ||
        itemToEquip.type == ItemType.Shield && equippedItem.type == ItemType.Two_Handed_Sword ||
        itemToEquip.type == ItemType.Shield && equippedItem.type == ItemType.Axe;
  }

  Future<void> unequipAnyItemInSlot(context, Item itemToEquip, Bandit.Hero hero) async {
    Item toUnequip, toUnequipTwoHanded;
    bool execute = false, executeTwoHanded = false;
    hero.items.forEach((equippedItem) async {
      Item item = findById(equippedItem);
      if (tryingToEquipShieldOrTwoHand(itemToEquip, item)) {
        toUnequipTwoHanded = item;
        executeTwoHanded = true;
        print(executeTwoHanded);
      }
      if (item != null && item.slot == itemToEquip.slot) {
        toUnequip = item;
        execute = true;
      }
    });
    if (executeTwoHanded) {
      await unequip(context, toUnequipTwoHanded);
    }
    if (execute) {
      await unequip(context, toUnequip);
    }
  }

  bool canEquip(Item item, Bandit.Hero hero) {
    return hero.availableTypes.contains(item.type);
  }

  Future<void> equip(context, Item item, Bandit.Hero hero) async {
    if (canEquip(item, hero)) {
      await unequipAnyItemInSlot(context, item, hero);
      if (item.owner != ' ') {
        await unequip(context, item);
      }
      heroToEquip.items.add(item.id);
      await Provider.of<Heroes>(context, listen: false).saveHero(heroToEquip);
      item.owner = heroToEquip.id;
      await saveItem(item);
      notifyListeners();
      Navigator.of(context).pop();
      Announcer(context).showSuccessMessage('Done!', 'This item has been equipped to your hero.');
    } else {
      Announcer(context).showErrorMessage('Error!', 'This item can\'t be equipped to this hero');
    }

  }

  List<Item> filterItemsByRarity(Rarity rarity) {
    return _itemList.where((item) => item.rarity == rarity).toList();
  }

  Future<void> addItem(Item item) async {
    _itemList.add(item);
    notifyListeners();
    await saveItem(item);
  }

  Future<void> buyItem(context, Item item, int cost) async {
    Provider.of<Resources>(context, listen: false).setGold(-cost);
    Announcer(context).showSuccessMessage('Success!', 'You have bought an item from the market.');
    await removeItemFromMarket(item);
    await addItem(item);
  }

  Future<void> deleteItem(Item item) async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId/items/${item.id}.json?auth=$token');
    await http.delete(url);
  }

  Future<void> removeItemFromMarket(Item item) async {
    _marketItems.removeWhere((marketItem) => marketItem.id == item.id);
    notifyListeners();
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId/marketItems/${item.id}.json?auth=$token');
    await http.delete(url);
  }

  Future<void> sellItem(context, Item sellItem, int cost) async {
    Provider.of<Resources>(context, listen: false).setGold(cost);
    Announcer(context).showSuccessMessage('Success!', 'You sold an item for $cost gold coins.');
    _itemList.removeWhere((item) => item.id == sellItem.id);
    notifyListeners();
    await deleteItem(sellItem);
  }

  Future<void> saveItem(Item item) async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId/items/${item.id}.json?auth=$token');
    try {
      await http.put(
        url,
        body: json.encode({
          'damage': item.damage,
          'id': item.id,
          'image': item.image,
          'name': item.name,
          'owner': item.owner,
          'rarity': item.rarity.toString().parseRarity().toLowerCase(),
          'armor': item.armor,
          'type': item.itemType,
          'abilityText': item.abilityText,
          'isNew': item.isNew,
          'slot': item.slot,
          'stats': {
            'strength': item.strength,
            'dexterity': item.dexterity,
            'intelligence': item.intelligence,
            'vitality': item.vitality,
          },
        }),
      );
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> unequip(context, Item item) async {
    await Provider.of<Heroes>(context, listen: false).unequipItem(item, item.owner);
    item.owner = ' ';
    notifyListeners();
    await saveItem(item);
  }

  Bandit.Hero getHeroWithFluteEquipped(context) {
    int i = 0;
    Bandit.Hero foundHero;
    while (i < _itemList.length && foundHero == null) {
      Item item = _itemList[i];
      if (item.name.toLowerCase() == 'storyteller\s flute' && item.isEquipped()) {
        foundHero = Provider.of<Heroes>(context, listen: false).findById(item.owner);
      }
      i++;
    }
    return foundHero;
  }

  List<Item> getHeroItems(Bandit.Hero hero) {
    List <Item> ret = [];
    for (String itemId in hero.items) {
      ret.add(_itemList.firstWhere((item) => item.id == itemId, orElse: () => null));
    }
    return ret;
  }

  Future<void> deleteMarketItems() async {
    _marketItems = [];
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
            .toString()}/users/$userId/marketItems.json?auth=$token');
    await http.delete(url);
    notifyListeners();
  }

  Future<void> refreshMarketItems(context) async {
    await deleteMarketItems();
    int i = 0;
    final int length = Provider.of<Subscription>(context, listen: false).isPremium ? MARKET_ITEMS_LENGTH + 1 : MARKET_ITEMS_LENGTH;
    while(i < length) {
      Rarity rarity = rollRarity();
      Item item;
      if (rarity != Rarity.Mythic) {
        item = generateRandomItem(rarity, null);
      } else {
        item = pickRandomMythicItem(null);
      }
      _marketItems.add(item);
      i++;
    }
    notifyListeners();
    marketItems.forEach((item) async {
      final url = Uri.parse(
          'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/${world
              .toString()}/users/$userId/marketItems/${item.id}.json?auth=$token');
      try {
        await http.put(
          url,
          body: json.encode({
            'damage': item.damage,
            'id': item.id,
            'image': item.image,
            'name': item.name,
            'owner': item.owner,
            'rarity': item.rarity.toString().parseRarity().toLowerCase(),
            'armor': item.armor,
            'type': item.itemType,
            'abilityText': item.abilityText,
            'isNew': item.isNew,
            'slot': item.slot,
            'stats': {
              'strength': item.strength,
              'dexterity': item.dexterity,
              'intelligence': item.intelligence,
              'vitality': item.vitality,
            },
          }),
        );
      } catch (error) {
        throw error;
      }
    });
  }

  Item pickRandomMythicItem(Bandit.Hero hero) {
    List<Item> availableItems = new List<Item>.from(getMythicItems());
    if (hero != null) {
      availableItems.removeWhere((item) => !hero.availableTypes.contains(item));
    }
    var rng = Random();
    int roll = rng.nextInt(availableItems.length);
    return availableItems[roll];
  }

  Item pickRandomLegendaryItem(Bandit.Hero hero) {
    List<Item> availableItems = new List<Item>.from(getLegendaryItems());
    if (hero != null) {
      availableItems.removeWhere((item) => !hero.availableTypes.contains(item));
    }
    var rng = Random();
    int roll = rng.nextInt(availableItems.length);
    return availableItems[roll];
  }

  Rarity rollRarity() {
    var rng = Random();
    final int roll = rng.nextInt(100);
    Rarity rarity = Rarity.Mythic;
    if (roll < 80) {
      rarity = Rarity.Common;
    } else {
      if (roll < 97) {
        rarity = Rarity.Rare;
      }
    }
    return rarity;
  }


  Item generateRandomItem(Rarity rarity, Bandit.Hero hero) {
    if (rarity == Rarity.Common || rarity == Rarity.Rare) {
      var rng = Random();
      List<ItemType> possibleTypes = [
        ItemType.Armor,
        ItemType.Axe,
        ItemType.Bow,
        ItemType.Crossbow,
        ItemType.Dagger,
        ItemType.Gun,
        ItemType.One_Handed_Sword,
        ItemType.Shield,
        ItemType.Staff,
        ItemType.Staff,
        ItemType.Trinket,
        ItemType.Two_Handed_Sword
      ];
      if (hero != null) {
        possibleTypes = hero.availableTypes;
      }
      final ItemType type = possibleTypes[rng.nextInt(possibleTypes.length)];
      final bool isArmor = type == ItemType.Armor || type == ItemType.Shield ? true : false;
      final bool isTrinket = type == ItemType.Trinket;

      return Item(
        id: Uuid().v1(),
        name: generateName(type, rarity),
        image: getImage(type),
        owner: ' ',
        damage: isArmor || isTrinket ? 0 : rollDamage(type, rarity),
        armor: isArmor ? rollArmor(type, rarity) : 0,
        type: type,
        stats: rollStats(type, rarity, hero),
        rarity: rarity,
        abilityText: ' ',
        isNew: true,
        slot: setSlot(type),
      );
    }

    if (rarity == Rarity.Mythic) {
      return pickRandomMythicItem(hero);
    }
    if (rarity == Rarity.Legendary) {
      return pickRandomLegendaryItem(hero);
    }
  }

  String setSlot(ItemType type) {
    switch (type) {
      case ItemType.Trinket:
        {
          return 'trinket';
        }
      case ItemType.One_Handed_Sword:
        {
          return 'weapon';
        }
      case ItemType.Two_Handed_Sword:
        {
          return 'weapon';
        }
      case ItemType.Axe:
        {
          return 'weapon';
        }
      case ItemType.Bow:
        {
          return 'weapon';
        }
      case ItemType.Crossbow:
        {
          return 'weapon';
        }
      case ItemType.Gun:
        {
          return 'weapon';
        }
      case ItemType.Staff:
        {
          return 'weapon';
        }
      case ItemType.Dagger:
        {
          return 'weapon';
        }
      case ItemType.Armor:
        {
          return 'armor';
        }
      case ItemType.Shield:
        {
          return 'shield';
        }
    }
  }

  Map<String, int> rollStats(ItemType type, Rarity rarity, Bandit.Hero hero) {
    var rng = Random();
    switch (type) {
      case ItemType.Armor:
        {
          return {
            'strength': 0,
            'dexterity': 0,
            'intelligence': 0,
            'vitality': rarity == Rarity.Common ? (rng.nextInt(VITALITY_SPREAD) + VITALITY_COMMON_OFFSET) :
            (rng.nextInt(VITALITY_SPREAD) + VITALITY_RARE_OFFSET)
          };
        }
      case ItemType.Shield:
        {
          return {
            'strength': 0,
            'dexterity': 0,
            'intelligence': 0,
            'vitality': rarity == Rarity.Common ? (rng.nextInt(VITALITY_SPREAD) + VITALITY_COMMON_OFFSET) :
            (rng.nextInt(VITALITY_SPREAD) + VITALITY_RARE_OFFSET)
          };
        }
      default: {
        int rolledStat = rarity == Rarity.Common ? (rng.nextInt(STAT_SPREAD) + STAT_COMMON_OFFSET) :
        (rng.nextInt(STAT_SPREAD) + STAT_RARE_OFFSET);
        if (hero == null) {
          int roll = rng.nextInt(3);
          if (roll == 0) {
            return {
              'strength': rolledStat,
              'dexterity': 0,
              'intelligence': 0,
              'vitality': 0
            };
          } else {
            if (roll == 1) {
              return {
                'strength': 0,
                'dexterity': rolledStat,
                'intelligence': 0,
                'vitality': 0
              };
            } else {
              if (roll == 2) {
                return {
                  'strength': 0,
                  'dexterity': 0,
                  'intelligence': rolledStat,
                  'vitality': 0
                };
              } else {
                return {
                  'strength': 0,
                  'dexterity': 0,
                  'intelligence': 0,
                  'vitality': rolledStat
                };
              }
            }
          }
        }
        if (hero.mainStat == 'strength') {
          return {
            'strength': rolledStat,
            'dexterity': 0,
            'intelligence': 0,
            'vitality': 0
          };
        } else {
          if (hero.mainStat == 'dexterity') {
            return {
              'strength': 0,
              'dexterity': rolledStat,
              'intelligence': 0,
              'vitality': 0
            };
          } else {
            return {
              'strength': 0,
              'dexterity': 0,
              'intelligence': rolledStat,
              'vitality': 0
            };
          }
        }
      }
    }
  }

  int rollArmor(ItemType type, Rarity rarity) {
    var rng = Random();
    switch (type) {
      case ItemType.Armor:
        {
          return rarity == Rarity.Common ? (rng.nextInt(ARMOR_ARMOR_SPREAD) + ARMOR_COMMON_OFFSET) :
          (rng.nextInt(ARMOR_ARMOR_SPREAD) + ARMOR_RARE_OFFSET);
        }
      case ItemType.Shield:
        {
          return rarity == Rarity.Common ? (rng.nextInt(SHIELD_ARMOR_SPREAD) + SHIELD_COMMON_OFFSET) :
          (rng.nextInt(SHIELD_ARMOR_SPREAD) + SHIELD_RARE_OFFSET);
        }
    }
  }

  int rollDamage(ItemType type, Rarity rarity) {
    var rng = Random();
    switch (type) {
      case ItemType.One_Handed_Sword:
        {
          return rarity == Rarity.Common ? (rng.nextInt(SWORD_DAMAGE_SPREAD) + ONE_HANDED_SWORD_DMG_COMMON_OFFSET) :
           (rng.nextInt(SWORD_DAMAGE_SPREAD) + ONE_HANDED_SWORD_DMG_RARE_OFFSET);
        }
      case ItemType.Two_Handed_Sword:
        {
          return rarity == Rarity.Common ? (rng.nextInt(SWORD_DAMAGE_SPREAD) + TWO_HANDED_SWORD_DMG_COMMON_OFFSET) :
          (rng.nextInt(SWORD_DAMAGE_SPREAD) + TWO_HANDED_SWORD_DMG_RARE_OFFSET);
        }
      case ItemType.Axe:
        {
          return rarity == Rarity.Common ? (rng.nextInt(AXE_DAMAGE_SPREAD) + AXE_DMG_COMMON_OFFSET) :
          (rng.nextInt(AXE_DAMAGE_SPREAD) + AXE_DMG_RARE_OFFSET);
        }
      case ItemType.Bow:
        {
          return rarity == Rarity.Common ? (rng.nextInt(BOW_DAMAGE_SPREAD) + BOW_DMG_COMMON_OFFSET) :
          (rng.nextInt(BOW_DAMAGE_SPREAD) + BOW_DMG_RARE_OFFSET);
        }
      case ItemType.Crossbow:
        {
          return rarity == Rarity.Common ? (rng.nextInt(CROSSBOW_DAMAGE_SPREAD) + CROSSBOW_DMG_COMMON_OFFSET) :
          (rng.nextInt(CROSSBOW_DAMAGE_SPREAD) + CROSSBOW_DMG_RARE_OFFSET);
        }
      case ItemType.Gun:
        {
          return rarity == Rarity.Common ? (rng.nextInt(GUN_DAMAGE_SPREAD) + GUN_DMG_COMMON_OFFSET) :
          (rng.nextInt(GUN_DAMAGE_SPREAD) + GUN_DMG_RARE_OFFSET);
        }
      case ItemType.Staff:
        {
          return rarity == Rarity.Common ? 10 : 15;
        }
      case ItemType.Dagger:
        {
          return rarity == Rarity.Common ? (rng.nextInt(DAGGER_DAMAGE_SPREAD) + DAGGER_DMG_COMMON_OFFSET) :
          (rng.nextInt(DAGGER_DAMAGE_SPREAD) + DAGGER_DMG_RARE_OFFSET);
        }
    }
  }

  String getImage(type) {
    switch (type) {
      case ItemType.Trinket:
        {
          return 'assets/images/items/necklace.jpg';
        }
      case ItemType.One_Handed_Sword:
        {
          return 'assets/images/items/onehandsword.jpg';
        }
      case ItemType.Two_Handed_Sword:
        {
          return 'assets/images/items/twohandsword.png';
        }
      case ItemType.Axe:
        {
          return 'assets/images/items/axe.jpg';
        }
      case ItemType.Bow:
        {
          return 'assets/images/items/bow.jpg';
        }
      case ItemType.Crossbow:
        {
          return 'assets/images/items/crossbow.jpg';
        }
      case ItemType.Gun:
        {
          return 'assets/images/items/pistol.jpg';
        }
      case ItemType.Staff:
        {
          return 'assets/images/items/staff.jpg';
        }
      case ItemType.Dagger:
        {
          return 'assets/images/items/dagger.jpg';
        }
      case ItemType.Shield:
        {
          return 'assets/images/items/shield.jpg';
        }
      case ItemType.Armor:
        {
          return 'assets/images/items/armor.jpg';
        }
    }
  }

  String generateName(ItemType type, Rarity rarity) {
    var rng = Random();

    final List<String> commonTrinketNames = ['Necklace'];
    final List<String> commonOneHandSwordAdjectives = ['Short', 'Small'];
    final List<String> commonTwoHandSwordAdjectives = ['Long', 'Great'];
    final List<String> commonAxeAdjectives = ['Great', 'Battle'];
    final List<String> commonBowAdjectives = ['Short', 'Compound'];
    final List<String> commonCrossbowAdjectives = ['Hand', 'Compound'];
    final List<String> commonGunAdjectives = ['Hand', 'Short'];
    final List<String> commonStaffAdjectives = ['Wizard\'s', 'Magician\'s'];
    final List<String> commonDaggerAdjectives = ['Short', 'Iron'];
    final List<String> commonShieldAdjectives = ['Round', 'Leather'];
    final List<String> commonArmorAdjectives = ['Leather', 'Chain'];

    final List<String> rareTrinketNames = ['Necklace'];
    final List<String> rareOneHandSwordAdjectives = ['Light', 'Swift'];
    final List<String> rareTwoHandSwordAdjectives = ['Bastard', 'Broad'];
    final List<String> rareAxeAdjectives = ['Wicked', 'Barbed'];
    final List<String> rareBowAdjectives = ['Long', 'Recurve'];
    final List<String> rareCrossbowAdjectives = ['Recurve', 'Repeating'];
    final List<String> rareGunAdjectives = ['Long', 'Hunting'];
    final List<String> rareStaffAdjectives = ['Witch\'s', 'Warlock\'s'];
    final List<String> rareDaggerAdjectives = ['Steel', 'Wicked'];
    final List<String> rareShieldAdjectives = ['Tower', 'Heavy'];
    final List<String> rareArmorAdjectives = ['Plate', 'Steel'];

    if (rarity == Rarity.Common) {
      switch (type) {
        case ItemType.Trinket:
          {
            return commonTrinketNames[rng.nextInt(commonTrinketNames.length)];
          }
        case ItemType.One_Handed_Sword:
          {
            String firstName = commonOneHandSwordAdjectives[rng.nextInt(
                commonOneHandSwordAdjectives.length)];
            return firstName + ' Sword';
          }
        case ItemType.Two_Handed_Sword:
          {
            String firstName = commonTwoHandSwordAdjectives[rng.nextInt(
                commonTwoHandSwordAdjectives.length)];
            return firstName + ' Sword';
          }
        case ItemType.Axe:
          {
            String firstName = commonAxeAdjectives[rng.nextInt(
                commonBowAdjectives.length)];
            return firstName + ' Axe';
          }
        case ItemType.Bow:
          {
            String firstName = commonBowAdjectives[rng.nextInt(
                commonBowAdjectives.length)];
            return firstName + ' Bow';
          }
        case ItemType.Crossbow:
          {
            String firstName = commonCrossbowAdjectives[rng.nextInt(
                commonCrossbowAdjectives.length)];
            return firstName + ' Crossbow';
          }
        case ItemType.Gun:
          {
            String firstName = commonGunAdjectives[rng.nextInt(
                commonGunAdjectives.length)];
            return firstName + ' Pistol';
          }
        case ItemType.Staff:
          {
            String firstName = commonStaffAdjectives[rng.nextInt(
                commonStaffAdjectives.length)];
            return firstName + ' Staff';
          }
        case ItemType.Dagger:
          {
            String firstName = commonDaggerAdjectives[rng.nextInt(
                commonDaggerAdjectives.length)];
            return firstName + ' Dagger';
          }
        case ItemType.Shield:
          {
            String firstName = commonShieldAdjectives[rng.nextInt(
                commonShieldAdjectives.length)];
            return firstName + ' Shield';
          }
        case ItemType.Armor:
          {
            String firstName = commonArmorAdjectives[rng.nextInt(
                commonArmorAdjectives.length)];
            return firstName + ' Armor';
          }
      }
    }

    if (rarity == Rarity.Rare) {
      switch (type) {
        case ItemType.Trinket:
          {
            return rareTrinketNames[rng.nextInt(rareTrinketNames.length)];
          }
        case ItemType.One_Handed_Sword:
          {
            String firstName = rareOneHandSwordAdjectives[rng.nextInt(
                rareOneHandSwordAdjectives.length)];
            return firstName + ' Sword';
          }
        case ItemType.Two_Handed_Sword:
          {
            String firstName = rareTwoHandSwordAdjectives[rng.nextInt(
                rareTwoHandSwordAdjectives.length)];
            return firstName + ' Sword';
          }
        case ItemType.Axe:
          {
            String firstName = rareAxeAdjectives[rng.nextInt(
                rareAxeAdjectives.length)];
            return firstName + ' Axe';
          }
        case ItemType.Bow:
          {
            String firstName = rareBowAdjectives[rng.nextInt(
                rareBowAdjectives.length)];
            return firstName + ' Bow';
          }
        case ItemType.Crossbow:
          {
            String firstName = rareCrossbowAdjectives[rng.nextInt(
                rareCrossbowAdjectives.length)];
            return firstName + ' Crossbow';
          }
        case ItemType.Gun:
          {
            String firstName = rareGunAdjectives[rng.nextInt(
                rareGunAdjectives.length)];
            return firstName + ' Rifle';
          }
        case ItemType.Staff:
          {
            String firstName = rareStaffAdjectives[rng.nextInt(
                rareStaffAdjectives.length)];
            return firstName + ' Staff';
          }
        case ItemType.Dagger:
          {
            String firstName = rareDaggerAdjectives[rng.nextInt(
                rareDaggerAdjectives.length)];
            return firstName + ' Dagger';
          }
        case ItemType.Shield:
          {
            String firstName = rareShieldAdjectives[rng.nextInt(
                rareShieldAdjectives.length)];
            return firstName + ' Shield';
          }
        case ItemType.Armor:
          {
            String firstName = rareArmorAdjectives[rng.nextInt(
                rareArmorAdjectives.length)];
            return firstName + ' Cuirass';
          }
      }
    }
  }
}