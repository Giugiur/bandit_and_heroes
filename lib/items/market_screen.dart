import 'package:bandit_and_heroes/resources/gold_cost.dart';
import 'package:bandit_and_heroes/widgets/loading_elevated_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'item_tile.dart';
import 'item.dart';
import 'items.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources.dart';
import '../resources/resources_bar.dart';
import '../resources/gold_cost.dart';
import '../utils/constants.dart';

class MarketScreen extends StatefulWidget {
  static const routeName = '/market';
  @override
  _MarketScreenState createState() => _MarketScreenState();
}

class _MarketScreenState extends State<MarketScreen> {
  var _isLoading = false,
      _isInit = false;
  List<Item> marketItems = [];

  void _refreshItems() {
    setState(() {
      _isLoading = true;
    });
    Provider.of<Resources>(context, listen: false).setGold(-GOLD_REFRESH_COST);
    Provider.of<Items>(context, listen: false).refreshMarketItems(context)
      .whenComplete(() => setState(() {
        _isLoading = false;
      }));
  }
  void didChangeDependencies() {
    if (!_isInit) {
      setState(() {
        _isLoading = true;
      });
      marketItems = Provider.of<Items>(context).marketItems;
      if (marketItems.length == 0) {
        Provider.of<Items>(context, listen: false).refreshMarketItems(context)
            .whenComplete(() => setState(() {
          _isLoading = false;
        }));
      }
      setState(() {
        _isLoading = false;
      });
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  bool _hasEnoughGold() {
    return Provider.of<Resources>(context).gold >= GOLD_REFRESH_COST;
  }

  void dispose() {
    _isLoading = false;
    marketItems = [];
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    marketItems = Provider.of<Items>(context).marketItems;
    int i = 0;
    double dynamicHeight = 0.0;
    while (i < marketItems.length) {
      dynamicHeight += 0.1;
      i++;
    }

    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        padding: const EdgeInsets.all(20),
        height: deviceSize.height,
        width: deviceSize.width,
        child: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [ _isLoading ? Center(child: CircularProgressIndicator(),) :
              Text(
                'The Market',
                style: Theme.of(context).textTheme.headline2,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20,),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (ctx, index) => ItemTile(
                    marketItems[index],
                    true
                ),
                itemCount: marketItems.length,
              ),
              SizedBox(height: 20,),
              LoadingElevatedButton('Refresh Items', false, !_hasEnoughGold(), _refreshItems),
              Center(
                child: Container(
                  width: 45,
                  child: GoldCost(GOLD_REFRESH_COST)
                ),
              ),
            ]
          ),
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
