import 'package:bandit_and_heroes/world/world.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'expandable_item_list.dart';
import 'item.dart';
import 'items.dart';
import 'market_screen.dart';
import '../bandit_bar/bandit_bar.dart';
import '../resources/resources_bar.dart';

class ItemsScreen extends StatefulWidget {
  static const routeName = '/items';

  @override
  _ItemsScreenState createState() => _ItemsScreenState();
}

class _ItemsScreenState extends State<ItemsScreen> {
  bool _isLoading = false;

  void didChangeDependencies() async {
    super.didChangeDependencies();
    setState(() {
      _isLoading = true;
    });
    if (Provider.of<Items>(context).itemList.isEmpty) {
      await Provider.of<World>(context, listen: false).getUserData();
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Item> legendaryItemList = Provider.of<Items>(context).filterItemsByRarity(Rarity.Legendary);
    final List<Item> mythicItemList = Provider.of<Items>(context).filterItemsByRarity(Rarity.Mythic);
    final List<Item> rareItemList = Provider.of<Items>(context).filterItemsByRarity(Rarity.Rare);
    final List<Item> commonItemList = Provider.of<Items>(context).filterItemsByRarity(Rarity.Common);
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        padding: const EdgeInsets.all(20),
        height: deviceSize.height,
        width: deviceSize.width,
        child: SingleChildScrollView(
          child: _isLoading ? Center(child: CircularProgressIndicator(),) :
          ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Container(
                alignment: Alignment.topCenter,
                child: InkWell(
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Want more items? See the ',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        TextSpan(
                          text: 'Market!',
                          style: Theme.of(context).textTheme.overline,
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed(MarketScreen.routeName);
                  }
                ),
              ),
              ExpandableItemList(legendaryItemList, 'Legendary', Colors.orange),
              ExpandableItemList(mythicItemList, 'Mythic', Colors.purple),
              ExpandableItemList(rareItemList, 'Rare', Colors.blue),
              ExpandableItemList(commonItemList, 'Common', Colors.grey),
            ]
          ),
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
