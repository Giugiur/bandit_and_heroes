import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'item.dart';
import 'item_detail.dart';
import 'items.dart';
import '../heroes/heroes.dart';
import '../resources/gold_cost.dart';
import '../resources/resources.dart';
import '../utils/constants.dart';
import '../utils/extensions.dart';
import '../widgets/announcer.dart';
import '../widgets/loading_elevated_button.dart';

class ItemTile extends StatelessWidget {

  ItemTile(this.item, this.isMarket);

  final Item item;
  final bool isMarket;

  bool _hasEnoughGold(context, int cost) {
    return Provider.of<Resources>(context).gold >= cost;
  }

  void _buyItem(context, Item item, int cost) {
    Announcer(context).showActionMessage(
      'Are you sure you want to buy this item?',
      'The gold will be deducted from your coins.',
      'Buy',
      () async {
        await Provider.of<Items>(context, listen: false).buyItem(context, item, cost);
      }
    );
  }

  Future<void> _sellItem(context, Item item, int cost) async {
    await Announcer(context).showActionMessage(
      'Are you sure you want to sell this item?',
      '$cost gold will be added to your coins.',
      'Sell',
      () async {
        await Provider.of<Items>(context, listen: false).sellItem(context, item, cost);
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    String itemOwner = item.owner != ' ' ? Provider.of<Heroes>(context).findById(item.owner).heroClass : 'None';
    bool isAssetImage = item.image.contains('assets');
    int cost = GOLD_COST_COMMON_ITEM;
    int sellingCost = SELL_COST_COMMON_ITEM;
    if (item.rarity == Rarity.Rare) {
      cost = GOLD_COST_RARE_ITEM;
      sellingCost = SELL_COST_RARE_ITEM;
    } else {
      if (item.rarity == Rarity.Mythic) {
        cost = GOLD_COST_MYTHIC_ITEM;
        sellingCost = SELL_COST_MYTHIC_ITEM;
      } else {
        if (item.rarity == Rarity.Legendary) {
          sellingCost = SELL_COST_MYTHIC_ITEM;
        }
      }
    }
    var announcer = Announcer(context);
    return ListTile(
      leading: InkWell(
        onTap: () => showItemDetail(context, item),
        child: Container(
          width: 50,
          height: 50,
          child: Stack(
            children: [
              isAssetImage ?
                Image.asset(
                  item.image,
                  fit: BoxFit.contain,
                ) :
                FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: item.image,
                    fit: BoxFit.contain
                ),
              if(!isMarket && item.isNew != null && item.isNew)
                Image.asset(
                  'assets/images/new_item.png',
                  height: 25,
                  width: 25,
                  fit: BoxFit.cover
                ),
            ],
          )
        )
      ),
      title: InkWell(
        onTap: () => showItemDetail(context, item),
        child: Text(
           item.name,
        ),
      ),
      subtitle: InkWell(
        onTap: () => showItemDetail(context, item),
        child: isMarket ?
          GoldCost(cost)
          : Text(
          'Equipped by: ${itemOwner.capitalize().removeUnderscore()}',
          ),
      ),
      trailing: isMarket ?
          Container(
            width: 100,
            child: LoadingElevatedButton('Buy', false, !_hasEnoughGold(context, cost), () => _buyItem(context, item, cost))
          )
        : PopupMenuButton(
            color: Theme.of(context).canvasColor,
            initialValue: 2,
            child: IconButton(
              icon: Icon(Icons.more_vert),
            ),
            onSelected: (result) {
              if (result == 0) {
                if (itemOwner != ' ') {
                  Provider.of<Items>(context,listen: false).unequip(context, item);
                  announcer.showInfoMessage('This item has been unequipped from your hero.', 'It is now available to equip in another hero.');
                } else {
                  announcer.showErrorMessage('You can\'t unequip this item.', 'This item is not equipped to any hero.');
                }
              }
              if (result == 1) {
                if (item.owner != ' ') {
                  //unequip first
                }
                _sellItem(context, item, sellingCost);
              }
            },
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  value: 0,
                  child: Text('Unequip'),
                ),
                PopupMenuItem(
                  value: 1,
                  child: Text('Sell'),
                )
              ];
            },
          ),
    );
  }
}
