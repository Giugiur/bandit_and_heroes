import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'item.dart';
import 'items.dart';
import '../heroes/hero_selector.dart';
import '../utils/extensions.dart';
import '../widgets/loading_elevated_button.dart';

showItemDetail(context, Item item) {
  var _isLoading = false;
  final bool isArmor = item.itemType == 'Armor' || item.itemType == 'Shield' ? true : false;
  bool isAssetImage = item.image.contains('assets'); //!contains http
  bool itemFound = Provider.of<Items>(context, listen: false).findById(item.id) != null;
  if (item.isNew && itemFound) {
    item.isNew = false;
    Provider.of<Items>(context, listen: false).saveItem(item);
  }

  Future<void> _equipItem(context, item) async {
    await Provider.of<Items>(context, listen: false).equip(context, item, Provider.of<Items>(context, listen: false).heroToEquip);
  }

  bool _isDisabled(context, Item item) {
    return Provider.of<Items>(context).heroToEquip == null || !itemFound;
  }

  int calculateHeight() {
    int height = 300;
    if (item.abilityText != ' ') {
      height += 50;
    }
    if (itemFound) {
      height += 200;
    }
    return height;
  }

  showModalBottomSheet(
    context: context,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: const Radius.circular(15.0),
          topRight: const Radius.circular(15.0)
      ),
    ),
    builder: (context) {
      return SingleChildScrollView(
        child: Container(
          height: calculateHeight().toDouble(),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [
              Text(
                item.name,
                style: Theme.of(context).textTheme.headline2,
              ),
              Text(
                '(${item.rarity.toString().parseRarity()} ${item.itemType.removeUnderscore()})',
                style: Theme.of(context).textTheme.headline3,
              ),
              SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      width: 150,
                      height: 150,
                      child: isAssetImage ?
                        Image.asset(
                          item.image,
                          fit: BoxFit.contain,
                        ) :
                        FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            image: item.image,
                            fit: BoxFit.cover
                        ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: isArmor ? 'Armor: ' : 'Damage: ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              TextSpan(
                                text: isArmor ? '+${item.armor.toString()}' : '+${item.damage.toString()}',
                                style: Theme.of(context).textTheme.overline,
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Strength: ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              TextSpan(
                                text: '+${item.strength.toString()}',
                                style: Theme.of(context).textTheme.overline,
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Dexterity: ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              TextSpan(
                                text: '+${item.dexterity.toString()}',
                                style: Theme.of(context).textTheme.overline,
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Intelligence: ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              TextSpan(
                                text: '+${item.intelligence.toString()}',
                                style: Theme.of(context).textTheme.overline,
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Vitality: ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              TextSpan(
                                text: '+${item.vitality.toString()}',
                                style: Theme.of(context).textTheme.overline,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 10,),
              if (item.abilityText != null)
                Text(
                  item.abilityText,
                  style: Theme.of(context).textTheme.headline6,
                ),
              SizedBox(height: 10,),
              if (itemFound)
                Text(
                  'Equip To: ',
                  style: Theme.of(context).textTheme.headline4,
                ),
              if (itemFound)
                HeroSelector(
                  screen: 'item_detail',
                  item: item
                ),
              SizedBox(height: 10,),
              if (itemFound)
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                  child: LoadingElevatedButton('Equip', _isLoading, _isDisabled(context, item), () async {
                    _isLoading = true;
                    await _equipItem(context, item);
                    _isLoading = false;
                  }),
                ),
            ]
          ),
        ),
      );
    },
  );
}