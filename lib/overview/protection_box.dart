import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:custom_timer/custom_timer.dart';


class ProtectionBox extends StatefulWidget {
  DateTime lastAttack;
  Duration protection;

  ProtectionBox(this.lastAttack, this.protection);


  @override
  _ProtectionBoxState createState() => _ProtectionBoxState();
}

class _ProtectionBoxState extends State<ProtectionBox> {
  @override
  Widget build(BuildContext context) {
    Duration _timeProtected = widget.lastAttack != null ? (widget.lastAttack.add(widget.protection).difference(DateTime.now())) :
        Duration(seconds: 1);

    return Container(
        margin: const EdgeInsets.only(left: 5),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).unselectedWidgetColor),
          borderRadius: BorderRadius.circular(25),
        ),
        child: widget.lastAttack != null ? _timeProtected.isNegative ?
        Center(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Text(
                'Protection is off, make your Party ready!',
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.center
              ),
            ),
        ) : Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Text(
                'Protected for:',
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.center
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 5),
              child: CustomTimer(
                from: _timeProtected,
                to: Duration(hours: 0),
                onBuildAction: CustomTimerAction.auto_start,
                builder: (CustomTimerRemainingTime remaining) {
                  return Text(
                    "${remaining.hours}h:${remaining.minutes}m:${remaining.seconds}s",
                    style: Theme.of(context).textTheme.headline3,
                  );
                },
              ),
            ),
          ],
        ) : Center(child:
        Text(
          'You are protected until you make your first attack.',
          style: Theme.of(context).textTheme.headline4,
          textAlign: TextAlign.center,
        )),
    );
  }
}
