import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RankBox extends StatelessWidget {
  final int points;
  final int winrate;

  RankBox(this.points, this.winrate);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).unselectedWidgetColor),
        borderRadius: BorderRadius.circular(25),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 40,
            child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                          'Points',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                    ),
                    Text(
                      points.toString(),
                      style: Theme.of(context).textTheme.headline3
                    ),
                  ],
                ),
            ),
          ),
          Expanded(
            flex: 60,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      'Win Rate',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  Text(
                      '$winrate%',
                      style: Theme.of(context).textTheme.headline3
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
