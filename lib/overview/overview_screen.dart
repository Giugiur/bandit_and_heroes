
import 'package:bandit_and_heroes/battle/battle_detail_screen2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'protection_box.dart';
import 'rank_box.dart';
import '../bandit_bar/bandit_bar.dart';
import '../battle/battles.dart';
import '../battle/battle_record.dart';
import '../battle/battle_card.dart';
import '../news/news.dart';
import '../news/news_slider.dart';
import '../resources/resources_bar.dart';
import '../utils/helpers.dart';
import '../world/world.dart';

//This is the main/home screen on which the user lands after a successful login
class OverviewScreen extends StatefulWidget {
  static const routeName = '/overview';

  @override
  _OverviewScreenState createState() => _OverviewScreenState();
}

class _OverviewScreenState extends State<OverviewScreen> {
  var _isLoading = false,
  _isInit = false;
  int _points;
  int _winrate;
  int _battlesWon;
  int _battlesLost;
  Duration _worldProtection;
  DateTime _lastAttack;
  BattleRecord _lastBattle;
  List<dynamic> _news;

  void didChangeDependencies() async {
    if (!_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<News>(context, listen: false).news.then((response) {
         _news = response;
      }).whenComplete(() {
        setState(() {
          _isLoading = false;
        });
      });
      var userData = ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      if (userData == null) {
        setState(() {
          _isLoading = true;
        });
        await Provider.of<World>(context, listen: false).getUserData();
        setState(() {
          _isLoading = false;
        });
      }
      _worldProtection = Provider.of<World>(context, listen: false).protection;
      _battlesWon = Provider.of<World>(context, listen: false).battlesWon;
      _battlesLost = Provider.of<World>(context, listen: false).battlesLost;
      _points = Provider.of<World>(context, listen: false).points;
      _winrate = _battlesWon != 0 ? ((_battlesWon / (_battlesWon + _battlesLost))*100).floor() : 0;
      _lastAttack = Provider.of<World>(context, listen: false).lastAttack;
      Provider.of<Battles>(context, listen: false).getLastBattles(context).then((response) {
        if (response.isNotEmpty) {
          setState(() {
            _lastBattle = response[0];
          });
        }
      });
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        padding: const EdgeInsets.all(20),
        height: deviceSize.height,
        child:_isLoading ? Center(child: CircularProgressIndicator(),) :
        SingleChildScrollView(
          child: Column(
            children: [
              Text(
                'Overview',
                style: Theme.of(context).textTheme.headline2,
              ),
              SizedBox(height: 10,),
              Container( // Boxes
                height: deviceSize.height*0.15,
                child: Row(
                  children: [
                     Expanded(child: RankBox(_points, _winrate)),
                     Expanded(child: ProtectionBox(_lastAttack, _worldProtection)),
                  ],
                ),
              ),
              Container( // News
                margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        'News',
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ),
                    _isLoading ? Center(child: CircularProgressIndicator(),) : NewsSlider(_news),
                  ],
                ),
              ),
              Container( // Last Battle
                margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        'Last Battle',
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ),
                    _lastBattle != null ? BattleCard(_lastBattle)
                    : Center(child: Text('There\'s no record of battles yet.')),
                  ],
                ),
              ),
            ]
          ),
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
