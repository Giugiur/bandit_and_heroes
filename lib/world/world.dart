import 'dart:convert';
import 'package:bandit_and_heroes/heroes/heroes.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../auth/auth.dart';
import '../heroes/hero.dart' as Bandit;
import '../items/item.dart';
import '../users/user.dart';
import '../utils/constants.dart';
import '../utils/helpers.dart';

// Class for handling world functionality
class World with ChangeNotifier {
  final String token;
  final String userId;
  Map<String, dynamic> _currentWorld;
  Map<String, Object> _userData;

  World(this.token, this.userId);

  Map<String, dynamic> get world {
    if (_currentWorld != null) {
      return _currentWorld;
    }
    SharedPreferences.getInstance().then((prefs) {
      if (prefs.getString('worldData') != null) {
        final worldData = json.decode(prefs.getString('worldData'));
        _currentWorld = worldData['world'];
        notifyListeners();
        return _currentWorld;
      }
      return null;
    });
  }

  Map<String, dynamic> get userData {
    return _userData;
  }

  Map<String, dynamic> get resources {
    return _userData != null ? _userData['resources'] : null;
  }

  int get points {
    return _userData != null ? _userData['points'] : null;
  }

  void setPoints(int points) {
    _userData['points'] = points;
  }

  int get battlesWon {
    return _userData != null ? _userData['battlesWon'] : null;
  }

  void setBattlesWon(int battlesWon) {
    _userData['battlesWon'] = battlesWon;
  }

  int get battlesLost {
    return _userData != null ? _userData['battlesLost'] : null;
  }

  void setBattlesLost(int battlesLost) {
    _userData['battlesLost'] = battlesLost;
  }

  DateTime get lastAttack{
    return userData['lastAttack'] != null ? DateTime.parse(userData['lastAttack']) : null;
  }

  List<User> getWorldUsers () {
    if (_userData != null && _userData['users'] != null) {
      List<User> users = [];
      final extractedData = _userData['users'] as Map<String, dynamic>;
      extractedData.forEach((userId, user) {
        if (user != null) {
          users.add(buildUser(userId, user));
        }
      });
      return users;
    } else {
      return [];
    }
  }

  Future<List<User>> getAttackableUsers (context) async {
    final w = _currentWorld != null? _currentWorld['id'] : null;
    int userPoints = points;
    if (userPoints == null) {
      userPoints = await getFieldFromUser(userId, 'points');
    }
    final int lowerLimit = userPoints - ATTACKABLE_POINTS_RANGE;
    final int upperLimit = userPoints + ATTACKABLE_POINTS_RANGE;
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/$w/users.json?auth=$token&orderBy="points"&startAt=$lowerLimit&endAt=$upperLimit');
    final String username = await Provider.of<Auth>(context, listen: false).username;
    try {
      final response = await http.get(url);
      List<User> userList = [];
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      extractedData.forEach((userId, user) {
        if (user != null && user['username'] != username && !isProtected(context, user)) {
          userList.add(buildUser(userId, user));
        }
      });
      return userList;
    } catch (error) {
      throw error;
    }
  }

  Future<List<Map<String, String>>> getLeaderboards (context) async {
    final w = _currentWorld != null? _currentWorld['id'] : null;
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/$w/users.json?auth=$token&orderBy="points"&limitToFirst=500');
    try {
      final response = await http.get(url);
      List<Map<String, String>> rowList = [];
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      extractedData.forEach((userId, user) {
        int i = 0;
        while (i < rowList.length && int.parse(rowList[i]['points']) > user['points']) {
          i++;
        }
        rowList.insert(i, {'position': i.toString(), 'username': user['username'], 'points': user['points'].toString()});
        int j = i;
        while(j < rowList.length ) {
          rowList[j]['position'] = (int.parse(rowList[j]['position']) + 1).toString();
          j++;
        }
      });
      return rowList;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> getFieldFromUser(String userId, String field) async{
    final w = _currentWorld != null? _currentWorld['id'] : null;
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/$w/users/$userId/$field.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      return extractedData;
    } catch (error) {
      throw error;
    }
  }

  Future<void> setProtection(String id) async {
    final w = _currentWorld != null? _currentWorld['id'] : null;
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/$w/users/$id.json?auth=$token');
    try {
      await http.patch(
        url,
        body: json.encode({
          'lastAttack': DateTime.now().toIso8601String()
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  bool isProtected(context, userData) {
    Map<String, dynamic> heroes = userData['heroes'];
    DateTime lastAttack = DateTime.parse(userData['lastAttack']);
    if ((heroes != null && heroes.length > 1) || userData['battlesWon'] > 0 || userData['battlesLost'] > 0) {
      Duration protectionTime = lastAttack.add(protection).difference(DateTime.now());
      return protectionTime.isNegative ? false : true;
    } else {
      return true;
    }
  }

  Duration get protection {
    return _currentWorld != null ? Duration(hours: _currentWorld['protection']) : Duration(hours: 4);
  }

  Duration get questCooldown {
    return _currentWorld != null ? Duration(hours: _currentWorld['questCooldown']) : Duration(hours: 4);
  }

  List<Item> get items {
    if (_userData != null && _userData['items'] != null) {
      List<Item> items = [];
      final extractedData = _userData['items'] as  Map<String, dynamic>;
      extractedData.forEach((itemId, item) {
        if (item != null) {
          items.add(buildItem(item));
        }
      });
      return items;
    } else {
      return [];
    }
  }

  List<Bandit.Hero> get heroes {
    if (_userData != null && _userData['heroes'] != null) {
      List<Bandit.Hero> heroes = [];
      final extractedData = _userData['heroes'] as Map<String, dynamic>;
      extractedData.forEach((heroId, hero) {
        if (hero != null) {
          heroes.add(buildHero(hero));
        }
      });
      return heroes;
    } else {
      return [];
    }
  }

  List<Item> get marketItems {
    if (_userData != null && _userData['marketItems'] != null) {
      List<Item> marketItems = [];
      final extractedData = _userData['marketItems'] as Map<String, dynamic>;
      extractedData.forEach((itemId, item) {
        if (item != null) {
          marketItems.add(buildItem(item));
        }
      });
      return marketItems;
    } else {
      return [];
    }
  }

  setWorld (world) async {
    _currentWorld = world;
    final prefs = await SharedPreferences.getInstance();
    if (_currentWorld == null) {
      prefs.remove('worldData');
    } else {
      final worldData = json.encode({
        'world': _currentWorld,
      });
      prefs.setString('worldData', worldData);
    }
    notifyListeners();
  }

  int get id {
    return _currentWorld != null ? _currentWorld['id'] : null;
  }

  Future<List> getWorlds() async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/worldsData.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      return extractedData;
    } catch (error) {
      throw error;
    }
  }

  Future<void> createNewAccount(context) async {
    final w = _currentWorld != null? _currentWorld['id'] : null;
    if (_currentWorld == null) {
      return null;
    }
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/$w/users/$userId.json?auth=$token');
    try {
      final response = await http.put(
        url,
        body: json.encode({
          'battlesWon': 0,
          'battlesLost': 0,
          'lastAttack': (DateTime.now().subtract(Duration(hours: 24))).toIso8601String(),
          'points': 0,
          'resources': {'gold': 50, 'sapphires': 5, 'lastQuest': (DateTime.now().subtract(Duration(hours: 24))).toIso8601String()},
          'username': Provider.of<Auth>(context, listen: false).username,
        })
      );
      _userData = json.decode(response.body);
      notifyListeners();
      return _userData;
    } catch (error) {
      throw error;
    }
  }

  Future<Object> getUserData() async {
    final w = _currentWorld != null? _currentWorld['id'] : null;
    final url = Uri.parse('https://bandit-and-heroes-default-rtdb.firebaseio.com/worlds/$w/users/$userId.json?auth=$token');
    try {
      final response = await http.get(url);
      _userData = json.decode(response.body);
      notifyListeners();
      return _userData;
    } catch (error) {
      throw error;
    }
  }

}
