import 'dart:core';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'world_selector.i18n.dart';
import 'world.dart';
import '../overview/overview_screen.dart';
import '../widgets/announcer.dart';
import '../widgets/loading_elevated_button.dart';
import '../widgets/tipper.dart';
import '../widgets/version_display.dart';

// Screen that displays and manages the available worlds
class WorldSelectorScreen extends StatefulWidget {
  static const routeName = '/world_selector';
  @override
  _WorldSelectorScreenState createState() => _WorldSelectorScreenState();
}

class _WorldSelectorScreenState extends State<WorldSelectorScreen> {

  List<Map<String, dynamic>> _activeWorlds = [];
  List<DropdownMenuItem<Map<String, dynamic>>> _dropdownMenuItems;
  Map<String, dynamic> _selectedWorld = {};
  var _isLoading = false;

  void _showError(error) {
    Announcer(context).showErrorMessage('There was an unexpected error', error.toString());
    setState(() {
      _isLoading = false;
    });
  }

  final tipper = Tipper();

  List<DropdownMenuItem<Map<String, dynamic>>> _buildDropDownMenuItems(List<Map<String, dynamic>> listItems) {
    List<DropdownMenuItem<Map<String, dynamic>>> items = [];
    for (Map<String, dynamic> world in listItems) {
      if (world['status'] == 'active') {

      }
      items.add(
        DropdownMenuItem(
          child: Text(world['name']),
          value: world,
        ),
      );
    }
    return items;
  }

  void initState() {
    setState(() {
      _isLoading = true;
    });
    Provider.of<World>(context, listen: false).getWorlds().then((response) {
      response.forEach((world) {
        if (world != null) {
          _activeWorlds.add(world);
        }
      });
      _dropdownMenuItems = _buildDropDownMenuItems(_activeWorlds);
      _selectedWorld = null;
    }).catchError((error) {
      _showError(error);
    }).whenComplete(() => setState(() {
      _isLoading = false;
    }));
    super.initState();
  }

  void _createNewAccount() {
    setState(() {
      _isLoading = true;
    });
     Provider.of<World>(context, listen: false).createNewAccount(context).then((response) {
       Navigator.pushNamed(context, OverviewScreen.routeName);
       Announcer(context).showSuccessMessage('Success!', 'Welcome to ${_selectedWorld['name']}. Start by recruiting a hero from the Heroes screen.');
     }).catchError((error) {
       _showError(error);
     }).whenComplete(() => setState(() {
       _isLoading = false;
     }));
  }
  void _enterWorld() async {
    if (_selectedWorld['status'] == 'active') {
      setState(() {
        _isLoading = true;
      });
      Provider.of<World>(context, listen: false).getUserData().then((response) {
        if (response != null) {
          setState(() {
            _isLoading = false;
          });
          Navigator.of(context).pushNamed(
            OverviewScreen.routeName,
            arguments: response,
          );
        } else {
          setState(() {
            _isLoading = false;
          });
          Announcer(context).showActionMessage(
              'It seems that don\'t you own an account in this world',
              'Do you want to create a new one right now?',
              'Create',
              _createNewAccount
          );
        }
      }).catchError((error) {
        _showError(error);
      });
    } else {
      Announcer(context).showErrorMessage(
          'World inactive',
          'This world is inactive right now, but check back again soon!',
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 50,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: VersionDisplay(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 0),
        child: Center(
          child: ListView(
              shrinkWrap: true,
              children: [
                Text(
                  'Active Worlds',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(height: 60),
                SizedBox(
                  width: double.infinity,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).canvasColor,
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(
                          width: 0.0,
                          style: BorderStyle.none
                      ),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButtonFormField (
                        value: _selectedWorld,
                        items: _dropdownMenuItems,
                        hint: Text(
                          'Select a world',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        style: Theme.of(context).textTheme.bodyText1,
                        onChanged: (value) {
                          setState(() {
                            _isLoading = true;
                            _selectedWorld = value;
                          });
                          Provider.of<World>(context, listen: false).setWorld(_selectedWorld).then((_) {
                            setState(() {
                              _isLoading = false;
                            });
                          });
                        },
                      ),
                  ),
                ),
              ),
              SizedBox(height: 30),
              LoadingElevatedButton('Sign In', _isLoading, _selectedWorld == null, _enterWorld),
              SizedBox(height: 60),
              tipper,
            ]
          ),
        ),
      ),
    );
  }
}

