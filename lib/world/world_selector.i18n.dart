import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

  static var _t = Translations('en_us') + {
    'en_us': 'Select a world',
    'es': 'Elige un mundo',
  } + {
    'en_us': 'Sign In',
    'es': 'Ingresar',
  } + {
    'en_us': 'Active Worlds',
    'es': 'Mundos Activos',
  } + {
    'en_us': 'Tip: Premium suscription will let you queue heroes for quests so you don\'t have to manually set them on quest every 4 hours.',
    'es': 'Tip: La suscripción Premium te deja poner en cola heroes para hacer aventuras, para que no tengas que hacerlo tu mismo cada 4 horas.',
  } + {
    'en_us': 'Active Worlds',
    'es': 'Mundos Activos',
  } + {
    'en_us': 'Success!',
    'es': 'Éxito!',
  } + {
    'en_us': 'Welcome to Minos',
    'es': 'Bienvenido a Minos',
  } + {
    'en_us': 'Welcome to Valantis',
    'es': 'Bienvenido a Valantis',
  } + {
    'en_us': 'There was an unexpected error',
    'es': 'Hubo un error inesperado',
  } + {
    'en_us': 'It seems that don\'t you own an account in this world',
    'es': 'Parece que no tienes una cuenta en este mundo.',
  } + {
    'en_us': 'Do you want to create a new one right now?',
    'es': '¿Quieres crear una nueva ahora?',
  } + {
    'en_us': 'Do you want to create a new one right now?',
    'es': '¿Quieres crear una nueva ahora?',
  } + {
    'en_us': 'Create',
    'es': '¿Crear?',
  }
  ;

  String get i18n => localize(this, _t);
}