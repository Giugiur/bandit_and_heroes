import 'dart:math';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'quest.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/heroes.dart';
import '../items/item.dart';
import '../items/items.dart';
import '../quests/quest_result_screen.dart';
import '../resources/resources.dart';
import '../utils/helpers.dart';
import '../widgets/announcer.dart';

class Quests with ChangeNotifier {
  final String token;
  List<Quest> _questList = [];
  Quest questInProgress;
  Quest lastQuestResolved;
  List<Map<String, dynamic>> queue = [];
  Bandit.Hero _selectedHero;
  bool resolvingQuest = false;
  Quests(this.token);

  void setQuest(Quest quest) {
    questInProgress = quest;
    notifyListeners();
  }

  void setHero(Bandit.Hero hero) {
    _selectedHero = hero;
    notifyListeners();
  }

  Bandit.Hero get selectedHero {
    return _selectedHero;
  }

  List<Quest> get questList {
    return _questList;
  }

  void resolveQuest(context) async {
    resolvingQuest = true;
    if (questInProgress == null) {
      return;
    }
    String result;
    var rng = new Random(),
      announcer = Announcer(context);
    int chance = questInProgress.chanceOfSuccess(_selectedHero),
      roll = rng.nextInt(100),
      xp = questInProgress.experienceTable(),
      gold = questInProgress.goldTable(),
      sapphires = questInProgress.sapphiresTable();
    List<Item> items = questInProgress.itemsTable(context);

    void _goToQuestResult() {
      final Map<String, dynamic> arguments = {
        'quest': lastQuestResolved,
        'result': result,
        'xp': xp,
        'gold': gold,
        'sapphires': sapphires,
        'items': items
      };
      Navigator.of(context).pushNamed(
        QuestResultScreen.routeName,
        arguments: arguments
      );
    }
    lastQuestResolved = Quest.clone(questInProgress);
    if (roll <= chance) {
      result = 'success';
      Bandit.Hero heroWithFlute = Provider.of<Items>(context, listen: false).getHeroWithFluteEquipped(context);
      if (heroWithFlute == null) {
        await Provider.of<Heroes>(context, listen: false).finishQuest(lastQuestResolved.hero, lastQuestResolved.id, xp, result);
      } else {
        await Provider.of<Heroes>(context, listen: false).finishQuest(lastQuestResolved.hero, lastQuestResolved.id, (xp/2).ceil(), result);
        heroWithFlute.addExperience((xp/2).ceil());
      }
      Provider.of<Resources>(context, listen: false).setGold(gold);
      Provider.of<Resources>(context, listen: false).setSapphires(sapphires);
      items.forEach((item) => Provider.of<Items>(context, listen: false).addItem(item));
    } else {
      result = 'failed';
      await Provider.of<Heroes>(context, listen: false).finishQuest(lastQuestResolved.hero, lastQuestResolved.id, 0, result);
    }
    announcer.showActionMessage(
        'Your hero has returned from their quest',
        'Do you want to see how they fared?',
        'Yes',
        _goToQuestResult
    );
    _selectedHero = null;
    questInProgress = null;
    startNextQuest(context);
    resolvingQuest = false;
    notifyListeners();
  }

  void startNextQuest (context) {
    if (queue.isNotEmpty) {
      _selectedHero = new Bandit.Hero.clone(queue[0]['hero']);
      setQuest(new Quest.clone(queue[0]['quest']));
      final String timestamp = DateTime.now().toIso8601String();
      Provider.of<Quests>(context, listen: false).startQuest(context, timestamp, questInProgress);
      if (queue.isNotEmpty && lastQuestResolved != null) {
        queue.removeAt(0);
      }
      notifyListeners();
    }
  }

  bool anyOnQuest(context) {
    Bandit.Hero heroOnQuest = Provider.of<Heroes>(context, listen: false).heroes.firstWhere((hero) => hero.isOnQuest(context), orElse: () => null);
    return heroOnQuest != null;
  }

  void addToQueue (Bandit.Hero hero, Quest quest) {
    quest.setHeroOnQuest(hero);
    final Map<String, dynamic> queueItem = {
      'hero': hero,
      'quest': quest
    };
    queue.add(queueItem);
    notifyListeners();
  }

  Future<void> startQuest(context, String timestamp, Quest quest) async {
    quest.setHeroOnQuest(_selectedHero);
    Provider.of<Resources>(context, listen: false).setLastQuest(timestamp);
    await Provider.of<Heroes>(context, listen: false).startQuest(_selectedHero, timestamp);
  }

  Future<void> get fetchQuests async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/quests.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      extractedData.forEach((quest) {
        _questList.add(buildQuest(quest));
      });
      notifyListeners();
    } catch (error) {
      throw(error);
    }
  }
}

