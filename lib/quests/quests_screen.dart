import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'quest.dart';
import 'quests.dart';
import 'quest_queue.dart';
import 'quest_tile.dart';
import '../bandit_bar/bandit_bar.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/hero_selector.dart';
import '../resources/resources_bar.dart';
import '../widgets/empty_state.dart';

class QuestsScreen extends StatefulWidget {
  static const routeName = '/quests';

  @override
  _QuestsScreenState createState() => _QuestsScreenState();
}

class _QuestsScreenState extends State<QuestsScreen> {
  List<Quest> _completeQuestList = [];
  var _isLoading = false;
  List<Map<String, dynamic>> queue;

  void didChangeDependencies() async {
    setState(() {
      _isLoading = true;
    });
    if (Provider.of<Quests>(context, listen: false).questList.isEmpty) {
       await Provider.of<Quests>(context, listen: false).fetchQuests;
    }
    _completeQuestList = Provider.of<Quests>(context, listen: false).questList;
    setState(() {
      queue = Provider.of<Quests>(context, listen: false).queue;
      _isLoading = false;
    });
    super.didChangeDependencies();
  }

  bool isInQueue(Quest quest, Bandit.Hero hero) {
    int i = 0;
    bool found = false;
    Quest questInProgress = Provider.of<Quests>(context, listen: false).questInProgress;
    if (questInProgress != null && questInProgress.id == quest.id && questInProgress.hero.id == hero.id) {
      found = true;
    }
    while (i < queue.length && !found) {
      if (queue[i]['quest'].id == quest.id && queue[i]['quest'].hero.id == hero.id) {
        found = true;
      }
      i++;
    }
    return found;
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    Bandit.Hero hero = Provider.of<Quests>(context).selectedHero;
    List<Quest> _questList = List<Quest>.from(_completeQuestList);
    if (hero != null) {
      _questList.removeWhere((quest) => (quest.id == hero.questsCompleted.firstWhere((heroQuest) => heroQuest == quest.id, orElse: () => null)
        || isInQueue(quest, hero)
        || !quest.acceptsDifficulty(hero)));
    }
    _questList.sort((a, b) => a.difficulty.index.compareTo(b.difficulty.index));

    return Scaffold(
      appBar: BanditBar(),
      body: _isLoading == true ? Center(child: CircularProgressIndicator(),)
          : SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
              child: HeroSelector(screen: 'quests'),
            ),
            Divider(),
            if (queue.isNotEmpty) Container(
                padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                child: QuestQueue()
            ),
            if (queue.isNotEmpty) Divider(),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
              child: _questList.length == 0 ? EmptyState('There are no available quests for this hero right now.')
              : ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  return QuestTile(
                    _questList[index],
                  );
                },
                itemCount: _questList.length,
              ),
            ),
          ]
        ),
      ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}

