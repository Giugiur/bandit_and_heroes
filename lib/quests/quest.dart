import 'dart:math';
import 'package:provider/provider.dart';
import '../utils/constants.dart';
import '../heroes/hero.dart' as Bandit;
import '../items/item.dart';
import '../items/items.dart';

enum Difficulty {
  Easy,
  Medium,
  Hard,
  Insane
}

class Quest {
  Quest({
    this.id,
    this.name,
    this.description,
    this.image,
    this.difficulty,
    this.hero
  });

  Quest.clone(Quest otherQuest): this(
    id: otherQuest.id,
    name: otherQuest.name,
    description: otherQuest.description,
    image: otherQuest.image,
    difficulty: otherQuest.difficulty,
    hero: otherQuest.hero
  );

  final int id;
  final String name;
  final String description;
  final String image;
  final Difficulty difficulty;
  Bandit.Hero hero;

  void setHeroOnQuest (Bandit.Hero heroOnQuest) {
    hero = heroOnQuest;
  }

  int rollRarity() {
    var rng = new Random();
    return rng.nextInt(100);
  }

  List<Item> itemsTable(context) {
    switch (difficulty) {
      case Difficulty.Easy: {
        Rarity rarity = Rarity.Rare;
        if (rollRarity() < 85) {
          rarity = Rarity.Common;
        }
        return [
          Provider.of<Items>(context, listen: false).generateRandomItem(rarity, hero)
        ];
      }
      break;
      case Difficulty.Medium: {
        int roll = rollRarity();
        Rarity rarityItem1;
        Rarity rarityItem2;
        if (roll < 71) {
          rarityItem1 = Rarity.Common;
          rarityItem2 = Rarity.Rare;
        } else if (roll < 96) {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Rare;
        } else {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Mythic;
        }
        return [
          Provider.of<Items>(context, listen: false).generateRandomItem(rarityItem1, hero),
          Provider.of<Items>(context, listen: false).generateRandomItem(rarityItem2, hero),
        ];
      }
      break;
      case Difficulty.Hard: {
        int roll = rollRarity();
        Rarity rarityItem1;
        Rarity rarityItem2;
        if (roll < 51) {
          rarityItem1 = Rarity.Common;
          rarityItem2 = Rarity.Rare;
        } else if (roll < 92) {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Rare;
        } else if (roll < 98) {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Mythic;
        } else {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Legendary;
        }
        return [
          Provider.of<Items>(context, listen: false).generateRandomItem(rarityItem1, hero),
          Provider.of<Items>(context, listen: false).generateRandomItem(rarityItem2, hero),
        ];
      }
      break;
      case Difficulty.Insane: {
        int roll = rollRarity();
        Rarity rarityItem1;
        Rarity rarityItem2;
        if (roll < 83) {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Rare;
        } else if (roll < 93) {
          rarityItem1 = Rarity.Rare;
          rarityItem2 = Rarity.Mythic;
        } else {
          rarityItem1 = Rarity.Mythic;
          rarityItem2 = Rarity.Legendary;
        }
        return [
          Provider.of<Items>(context, listen: false).generateRandomItem(Rarity.Rare, hero),
          Provider.of<Items>(context, listen: false).generateRandomItem(rarityItem1, hero),
          Provider.of<Items>(context, listen: false).generateRandomItem(rarityItem2, hero),
        ];
      }
      break;
    }
  }

  int experienceTable() {
    switch (difficulty) {
      case Difficulty.Easy: {
        var rng = new Random();
        return rng.nextInt(XP_EASY_SPREAD) + XP_EASY_OFFSET;
      }
      break;
      case Difficulty.Medium: {
        var rng = new Random();
        return rng.nextInt(XP_MEDIUM_SPREAD) + XP_MEDIUM_OFFSET;
      }
      break;
      case Difficulty.Hard: {
        var rng = new Random();
        return rng.nextInt(XP_HARD_SPREAD) + XP_HARD_OFFSET;
      }
      break;
      case Difficulty.Insane: {
        var rng = new Random();
        return rng.nextInt(XP_INSANE_SPREAD) + XP_INSANE_OFFSET;
      }
      break;
    }
  }

  int goldTable() {
    switch (difficulty) {
      case Difficulty.Easy: {
        var rng = new Random();
        return rng.nextInt(GOLD_EASY_SPREAD) + GOLD_EASY_OFFSET;
      }
      break;
      case Difficulty.Medium: {
        var rng = new Random();
        return rng.nextInt(GOLD_MEDIUM_SPREAD) + GOLD_MEDIUM_OFFSET;
      }
      break;
      case Difficulty.Hard: {
        var rng = new Random();
        return rng.nextInt(GOLD_HARD_SPREAD) + GOLD_HARD_SPREAD;
      }
      break;
      case Difficulty.Insane: {
        var rng = new Random();
        return rng.nextInt(GOLD_INSANE_SPREAD) + GOLD_INSANE_SPREAD;
      }
      break;
    }
  }

  int sapphiresTable() {
    switch (difficulty) {
      case Difficulty.Easy: {
        var rng = new Random();
        return rng.nextInt(SAPPHIRES_EASY_SPREAD);
      }
      break;
      case Difficulty.Medium: {
        var rng = new Random();
        return rng.nextInt(SAPPHIRES_MEDIUM_SPREAD) + SAPPHIRES_MEDIUM_OFFSET;
      }
      break;
      case Difficulty.Hard: {
        var rng = new Random();
        return rng.nextInt(SAPPHIRES_HARD_SPREAD) + SAPPHIRES_HARD_OFFSET;
      }
      break;
      case Difficulty.Insane: {
        var rng = new Random();
        return rng.nextInt(SAPPHIRES_INSANE_SPREAD) + SAPPHIRES_INSANE_OFFSET;
      }
      break;
    }
  }

  int chanceOfSuccess (Bandit.Hero hero) {
    int factor;
    switch (difficulty) {
      case Difficulty.Easy: {
        factor = DIFF_EASY_FACTOR;
      }
      break;
      case Difficulty.Medium: {
        factor = DIFF_MEDIUM_FACTOR;
      }
      break;
      case Difficulty.Hard: {
        factor = DIFF_HARD_FACTOR;
      }
      break;
      case Difficulty.Insane: {
        factor = DIFF_INSANE_FACTOR;
      }
      break;
    }
    double chance = (hero.level + (hero.level*0.66)) / factor;
    return chance > 1 ? 100 : (chance*100).toInt();
  }

  bool acceptsDifficulty (Bandit.Hero hero) {
    switch (difficulty) {
      case Difficulty.Easy: {
        return true;
      }
      break;
      case Difficulty.Medium: {
        return hero.level >= LEVEL_REQUIRED_FOR_MEDIUM;
      }
      break;
      case Difficulty.Hard: {
        return hero.level >= LEVEL_REQUIRED_FOR_HARD;
      }
      break;
      case Difficulty.Insane: {
        return hero.level >= LEVEL_REQUIRED_FOR_INSANE;
      }
      break;
    }
  }

}