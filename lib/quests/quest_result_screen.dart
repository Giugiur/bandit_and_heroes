import 'dart:math';
import 'package:flutter/material.dart';
import 'quest.dart';
import 'quests_screen.dart';
import '../bandit_bar/bandit_bar.dart';
import '../items/item.dart';
import '../items/item_tile.dart';
import '../resources/resources_bar.dart';
import '../utils/extensions.dart';
import '../utils/helpers.dart';
import '../widgets/loading_elevated_button.dart';

class QuestResultScreen extends StatelessWidget {
  static const routeName = '/quest_result';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final Map<String, dynamic> arguments = ModalRoute.of(context).settings.arguments;
    final Quest quest = arguments['quest'];
    final String result = arguments['result'];
    final int xp = arguments['xp'];
    final int gold = arguments['gold'];
    final int sapphires = arguments['sapphires'];
    final List<Item> items = arguments['items'];
    final double totalHeight = items.length*75.toDouble();

    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        height: deviceSize.height,
        child: SingleChildScrollView(
          child: ListView(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                Stack(
                  children: [
                    Opacity(
                      opacity: 0.25,
                      child: Image.asset(
                        result == 'success' ? 'assets/images/success.jpg' : 'assets/images/failure.jpg',
                        height: 200,
                        width: deviceSize.width,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      height: 200,
                      width: deviceSize.width,
                      child: Center(
                        child: Text(
                          '${result.toUpperCase()}!',
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: result == 'success' ? Column(
                    children: [
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Your ',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            TextSpan(
                              text: quest.hero.heroClass.capitalize().removeUnderscore(),
                              style: Theme.of(context).textTheme.overline,
                            ),
                            TextSpan(
                              text: ' has returned from the ',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            TextSpan(
                              text: quest.name,
                              style: Theme.of(context).textTheme.overline,
                            ),
                            TextSpan(
                              text: ' quest and won ',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            TextSpan(
                              text: xp.toString(),
                              style: Theme.of(context).textTheme.overline,
                            ),
                            TextSpan(
                              text: ' points of experience, ',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            TextSpan(
                              text: gold.toString(),
                              style: Theme.of(context).textTheme.overline,
                            ),
                            TextSpan(
                              text: ' gold, ',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            TextSpan(
                              text: sapphires.toString(),
                              style: Theme.of(context).textTheme.overline,
                            ),
                            TextSpan(
                              text: ' sapphires and the following items: ',
                              style: Theme.of(context).textTheme.bodyText1,
                            )
                          ],
                        ),
                      ),
                      SingleChildScrollView(
                        child: Container(
                          height: min(deviceSize.height*0.50, totalHeight),
                          child: ListView.builder(
                            itemBuilder: (ctx, index) => ItemTile(
                                items[index],
                                false
                            ),
                            itemCount: items.length,
                          ),
                        ),
                      ),
                    ]
                  ) : RichText(
                    text: TextSpan(
                      children: [
                      TextSpan(
                      text: 'Your ',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    TextSpan(
                      text: quest.hero.heroClass.capitalize().removeUnderscore(),
                      style: Theme.of(context).textTheme.overline,
                    ),
                    TextSpan(
                      text: ' has returned from the ',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    TextSpan(
                      text: quest.name,
                      style: Theme.of(context).textTheme.overline,
                    ),
                    TextSpan(
                      text: ' quest and reported his failure, thus missing the rewards.',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ]))
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                  child: LoadingElevatedButton('Go to Quests', false, false, () => Navigator.of(context).pushReplacementNamed(QuestsScreen.routeName)),
                ),
              ]
            ),
          ),
        ),
      bottomNavigationBar: ResourcesBar(),
    );
  }
}
