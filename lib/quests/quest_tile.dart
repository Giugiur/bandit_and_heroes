import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:transparent_image/transparent_image.dart';
import 'quest.dart';
import 'quests.dart';
import '../heroes/hero.dart' as Bandit;
import '../heroes/heroes.dart';
import '../subscription/subscriptions.dart';
import '../utils/extensions.dart';
import '../widgets/announcer.dart';
import '../widgets/loading_elevated_button.dart';

class QuestTile extends StatefulWidget {
  final Quest quest;

  QuestTile(this.quest);

  @override
  _QuestTileState createState() => _QuestTileState();
}

class _QuestTileState extends State<QuestTile> {

  Future<void> _startQuest (BuildContext context) async {
    final String timestamp = DateTime.now().toIso8601String();
    if (Provider.of<Quests>(context, listen: false).anyOnQuest(context)) {
      Provider.of<Quests>(context, listen: false).addToQueue(Provider.of<Quests>(context, listen: false).selectedHero, widget.quest);
      Navigator.of(context).pop();
      Announcer(context).showInfoMessage(
          'Your hero has been added to the queue.',
          'Your queued heroes will be sent on quests automatically.'
      );
    } else {
      Provider.of<Quests>(context, listen: false).startQuest(context, timestamp, widget.quest)
          .then((_) {
        Provider.of<Quests>(context, listen: false).setQuest(widget.quest);
        Navigator.of(context).pop();
        Announcer(context).showInfoMessage(
            'Your hero has gone on a quest.',
            'They have been moved to the bench and are unavailable for the duration of the quest.'
        );
      });
    }
  }

  bool _isDisabled(BuildContext context) {
    Bandit.Hero hero = Provider.of<Quests>(context).selectedHero;
    bool isPremium = Provider.of<Subscription>(context).isPremium;
    return hero == null || (Provider.of<Quests>(context).anyOnQuest(context) && !isPremium);
  }

  void _showQuestDetail (BuildContext context) {
    var _isLoading = false;
    Bandit.Hero hero = Provider.of<Quests>(context, listen: false).selectedHero;
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(15.0),
              topRight: const Radius.circular(15.0)
          ),
        ),
        builder: (context) {
          return Scrollbar(
            isAlwaysShown: true,
            child: SingleChildScrollView(
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: 200,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(25.0),
                          topRight: const Radius.circular(25.0)
                        ),
                      ),
                      child: Image.network(
                        widget.quest.image,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                      child: Column(
                        children: [
                          Text(
                            widget.quest.name,
                            style: Theme.of(context).textTheme.headline3,
                          ),
                          SizedBox(height: 10,),
                          Text(
                            widget.quest.description,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          SizedBox(height: 10,),
                          if (hero != null)
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Difficulty: ',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                                TextSpan(
                                  text: '${widget.quest.difficulty.toString().parseDifficulty()} ',
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                TextSpan(
                                  text: '(${widget.quest.chanceOfSuccess(hero)}%)',
                                  style: Theme.of(context).textTheme.overline,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          LoadingElevatedButton('Start Quest', _isLoading, _isDisabled(context), () async {
                            setState(() {
                              _isLoading = true;
                            });
                            await _startQuest(context);
                            setState(() {
                              _isLoading = false;
                            });
                          }),
                        ]
                      ),
                    ),
                  ]
              ),
            ),
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          InkWell(
            onTap: () => _showQuestDetail(context),
            child: ListTile(
              leading: Container(
                child: Stack(
                  children: [
                    Shimmer(
                      color: Colors.black,
                      enabled: true,
                      direction: ShimmerDirection.fromLTRB(),
                      child: Container(
                        color: Colors.blueGrey[100],
                      ),
                    ),
                    FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: widget.quest.image,
                      height: 200,
                      fit: BoxFit.cover
                    )
                  ]
                ),
                width: 80,
              ),
              title: Text(widget.quest.name),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          Divider(),
        ]
      ),
    );
  }
}
