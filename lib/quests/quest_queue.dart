import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'quests.dart';

class QuestQueue extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> queue = Provider.of<Quests>(context).queue;
    return Container(
      width: double.infinity,
      child: Row(
        children: queue.map((e) => Container(
          padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          child: CircleAvatar(
            backgroundImage: AssetImage(e['hero'].assetImage),
          ),
        )).toList(),
      ),
    );
  }
}
