import 'package:flutter/material.dart';

// Abstracted button which includes loading, disabled and callback functionality
class LoadingElevatedButton extends StatefulWidget {
  final String text;
  final bool isLoading;
  final bool isDisabled;
  final Function handler;

  LoadingElevatedButton(this.text, this.isLoading, this.isDisabled, this.handler);

  @override
  _LoadingElevatedButtonState createState() => _LoadingElevatedButtonState();
}

class _LoadingElevatedButtonState extends State<LoadingElevatedButton> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return widget.isLoading || _isLoading ?
      Center(
        child: CircularProgressIndicator(),
      )
      : SizedBox(
          width: double.infinity,
          height: 40,
          child: ElevatedButton(
            child: Text(
              widget.text
            ),
            onPressed: widget.isDisabled ? null : () {
              _isLoading = true;
              FocusScope.of(context).unfocus();
              widget.handler();
              _isLoading = false;
            },
          ),
        );
  }
}