import 'dart:math';
import 'package:flutter/material.dart';

class Tipper extends StatelessWidget {

  String randomTip() {
    var rng = Random();
    final List<String> tips = [
      'Premium subscription will let you queue heroes for quests so you don\'t have to manually set them on a quest every 4 hours.',
      'Premium subscription will let you see 6 items on the Market instead of the usual 5.',
      'Premium usbscription will let you see the defender\'s points before attacking.',
      'Think twice before recruiting a hero. The more heroes you have, the more expensive is to recruit a new one.',
      'If you recruited only one hero and have not atacked anybody yet, then you will protected against attacks.',
      'When a hero is on a quest, they can not get involved in battles, whether you are attacking or defending.',
      'Remember to equip all your heroes and move them to Your Party before attacking.'
    ];
    return tips[rng.nextInt(tips.length)];
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      'Tip: ${randomTip()}',
      style: Theme.of(context).textTheme.bodyText1,
    );
  }
}
