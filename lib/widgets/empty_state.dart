import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class EmptyState extends StatelessWidget {
  final String text;

  EmptyState(this.text);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40,  vertical: 0),
      height: deviceSize.height,
      child: ListView(
        shrinkWrap: true,
        children: [
          Image.asset(
            'assets/images/empty_state.png',
            height: 150,
          ),
          SizedBox(height: 50,),
          Text(
            text,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline3,
          )
        ],
      ),
    );
  }
}
