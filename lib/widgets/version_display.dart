
import 'dart:ui';
import 'package:flutter/material.dart';


class VersionDisplay extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Text(
      'v1.0.8',
      style: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
