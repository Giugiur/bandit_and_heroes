import 'package:flutter/material.dart';
import 'package:another_flushbar/flushbar.dart';

// A class that handles all messaging (except inline validations) to the user
// It makes use of the Flushbar widget for displaying a beautiful UI.
class Announcer {
  BuildContext context;

  Announcer(this.context);

  showInfoMessage(String title, String message) {
    return Flushbar(
      margin: EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(10),
      backgroundColor: Colors.blue[500],
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3,3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      icon: Icon(
        Icons.info_outline,
        color: Colors.white,
      ),
      title: title,
      message: message,
      duration: Duration(seconds: 4),
    )..show(context);
  }

  showSuccessMessage(String title, String message) {
    return Flushbar(
      margin: EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(10),
      backgroundColor: Colors.green[500],
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3,3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      icon: Icon(
        Icons.check_circle,
        color: Colors.white,
      ),
      title: title,
      message: message,
      duration: Duration(seconds: 4),
    )..show(context);
  }

  showErrorMessage(String title, String message) {
    return Flushbar(
      margin: EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(10),
      backgroundColor: Colors.red[400],
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3,3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      icon: Icon(
        Icons.warning,
        color: Colors.white,
      ),
      title: title,
      message: message,
      duration: Duration(seconds: 4),
    )..show(context);
  }

  showActionMessage(String title, String message, String action, Function handler) {
    return Flushbar(
      margin: EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(10),
      backgroundColor: Colors.black87,
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3,3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      icon: Icon(
        Icons.help_outline,
        color: Colors.white,
      ),
      mainButton: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
        child: TextButton(
          child: Text(
            action.toUpperCase(),
            style: TextStyle(color: Theme.of(context).accentColor),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            handler();
          },
        ),
      ),
      title: title,
      message: message,
      duration: null,
    )..show(context);
  }
}
