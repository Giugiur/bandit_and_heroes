import 'package:flutter/material.dart';

class BenefitDescription extends StatelessWidget {
  final String text;

  BenefitDescription(this.text);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: <InlineSpan>[
          WidgetSpan(
            child: Icon(
              Icons.done,
              color: Colors.green,
              size: 20
            ),
          ),
          TextSpan(
            text: ' $text',
            style: TextStyle(
              fontSize: 16
            )
          ),
        ],
      ),
    );
  }
}
