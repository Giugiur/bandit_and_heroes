import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:provider/provider.dart';
import '../account/account_screen.dart';
import '../subscription/subscriptions.dart';


class PremiumStatus extends StatefulWidget {
  // Really, this is only a stateful widget because of the dispose method.
  @override
  _PremiumStatusState createState() => _PremiumStatusState();
}

class _PremiumStatusState extends State<PremiumStatus> {
  final _tapGestureRecognizer = TapGestureRecognizer();

  void dispose() {
    _tapGestureRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String premiumStatus = Provider.of<Subscription>(context).isPremium ? 'ACTIVE' : 'INACTIVE';
    return Center(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'Premium Status: ',
              style: Theme.of(context).textTheme.subtitle2,
            ),
            TextSpan(
              text: '$premiumStatus. ',
              style: Theme.of(context).textTheme.headline4,
            ),
            if (premiumStatus == 'INACTIVE')
            TextSpan(
              text: 'Activate now!',
              style: Theme.of(context).textTheme.overline,
              recognizer: _tapGestureRecognizer
              ..onTap = () {
                Navigator.pushNamed(context, AccountScreen.routeName);
            })
          ],
        ),
      ),
    );
  }
}
