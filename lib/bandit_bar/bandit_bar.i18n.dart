import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

  static var _t = Translations('en_us') + {
    'en_us': 'Account',
    'es': 'Mi Cuenta',
  } + {
    'en_us': 'Overview',
    'es': 'Vista General',
  } + {
    'en_us': 'Heroes',
    'es': 'Heroes',
  } + {
    'en_us': 'Items',
    'es': 'Objetos',
  } + {
    'en_us': 'Market',
    'es': 'Mercado',
  } + {
    'en_us': 'Quests',
    'es': 'Aventuras',
  } + {
    'en_us': 'Battle History',
    'es': 'Historial de Batalla',
  } + {
    'en_us': 'Leaderboards',
    'es': 'Clasificación',
  } + {
    'en_us': 'Logout',
    'es': 'Salir',
  } + {
    'en_us': 'Session Expired..',
    'es': 'Sesión Expirada..',
  }
  ;
  String get i18n => localize(this, _t);
}