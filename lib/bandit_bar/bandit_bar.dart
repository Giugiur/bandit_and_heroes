import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'bandit_bar.i18n.dart';
import '../account/account_screen.dart';
import '../auth/auth.dart';
import '../battle/attack_screen.dart';
import '../battle/battles_history_screen.dart';
import '../heroes/heroes_screen.dart';
import '../items/items_screen.dart';
import '../leaderboards/leaderboards_screen.dart';
import '../overview/overview_screen.dart';
import '../quests/quests_screen.dart';
import '../utils/custom_icons.dart';
import '../utils/helpers.dart';
import '../world/world.dart';

class BanditBar extends StatelessWidget with PreferredSizeWidget{
  final Size preferredSize = Size.fromHeight(50.0);

  @override
  Widget build(BuildContext context) {
    final _scrollController = ScrollController();
    final world = Provider.of<World>(context, listen: false).world;
    final username = Provider.of<Auth>(context, listen: false).username;
    return AppBar(
      title: (world == null || username == null) ? Text('Session Expired..'): Text('$username: ${world['name']}'),
      leading: Navigator.of(context).canPop() ?
        IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop()
        ) : null,
      actions: [
        IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(15.0),
                      topRight: const Radius.circular(15.0)
                  ),
                ),
                builder: (context) {
                  return Scrollbar(
                    controller: _scrollController,
                    isAlwaysShown: true,
                    child: SingleChildScrollView(
                      controller: _scrollController,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: const Icon(Icons.account_circle),
                            title: Text('Account'),
                            onTap: () async {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, AccountScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(Icons.public),
                            title: Text('Overview'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, OverviewScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(CustomIcons.hood),
                            title: Text('Heroes'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, HeroesScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(Icons.landscape),
                            title: Text('Quests'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, QuestsScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(CustomIcons.crossed_swords),
                            title: Text('Attack'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, AttackScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(Icons.local_police),
                            title: Text('Items'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, ItemsScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(Icons.emoji_events),
                            title: Text('Leaderboards'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, LeaderboardsScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(Icons.history),
                            title: Text('Battle History'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, BattlesHistoryScreen.routeName);
                            },
                          ),
                          Divider(),
                          ListTile(
                            leading: const Icon(Icons.logout),
                            title: Text('Logout'),
                            onTap: () {
                              cleanLogout(context);
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                });
          },
        ),
      ],
    );
  }
}
