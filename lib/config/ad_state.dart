import 'dart:io';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdState {
  Future<InitializationStatus> initialization;

  AdState(this.initialization);

  String get bannerAdUnitId => Platform.isAndroid
    ? 'ca-app-pub-5734705219006067/3806929902' // 3940256099942544/6300978111  5734705219006067/3806929902
    : 'ca-app-pub-5734705219006067/7543337811';  //5734705219006067/7543337811

  BannerAdListener get bannerAdListener => _bannerAdListener;

  BannerAdListener _bannerAdListener = BannerAdListener();
}