import 'package:bandit_and_heroes/auth/auth.dart';
import 'package:bandit_and_heroes/widgets/announcer.dart';
import 'package:bandit_and_heroes/widgets/loading_elevated_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../bandit_bar/bandit_bar.dart';

class ReportBugScreen extends StatefulWidget {
  static const routeName = '/report-bug';

  @override
  _ReportBugScreenState createState() => _ReportBugScreenState();
}

class _ReportBugScreenState extends State<ReportBugScreen> {
  bool _isLoading = false, _isDisabled = true;
  final textController = TextEditingController();

  void _onChanged(String value) {
    setState(() {
      _isDisabled = value.length < 10;
    });
  }

  void _sendReport() async {
    setState(() {
      _isLoading = true;
    });
    Provider.of<Auth>(context, listen:false).sendReport(textController.text).then((response) {
      Announcer(context).showSuccessMessage('Success!', 'We have sent your report. Thanks for helping us improve the game experience');
    }).catchError((error) {
      Announcer(context).showErrorMessage('Error!', 'There was an unexpected error. Please, try again later.');
    }).whenComplete(() {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BanditBar(),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            Text(
              'Report a Bug',
              style: Theme.of(context).textTheme.headline2,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20,),
            TextField(
              maxLines: 5,
              maxLength: 150,
              controller: textController,
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(20.0),
                  hintText: 'Tell us what happened...'
              ),
              onChanged: _onChanged,
            ),
            SizedBox(height: 20,),
            LoadingElevatedButton('Submit', _isLoading, _isDisabled, _sendReport)
          ]
        ),
      ),
    );
  }
}
