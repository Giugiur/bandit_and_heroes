import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeSwitcher with ChangeNotifier {
  var _isLoading = false;
  var _lightTheme = ThemeData(
    fontFamily: 'Nunito',
    brightness: Brightness.light,
    primarySwatch: Colors.teal,
    accentColor: Colors.teal,
    canvasColor: Colors.white,
    primaryColorLight: Colors.red,
    primaryColorDark: Colors.blue,
    unselectedWidgetColor: Colors.blueGrey[300],
    appBarTheme: AppBarTheme(
      color: Colors.teal,
      elevation: 0,
    ),
    textTheme: TextTheme(
      headline1: TextStyle(
        color: Colors.black,
        fontSize: 32.0,
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.none,
      ),
      headline2: TextStyle(
        color: Colors.black,
        fontSize: 24.0,
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.none,
      ),
      bodyText1: TextStyle(
        color: Colors.black54,
        fontSize: 16.0,
        decoration: TextDecoration.none,
      ),
      bodyText2: TextStyle(
        color: Colors.black,
        fontSize: 15.0,
        decoration: TextDecoration.none,
      ),
      overline: TextStyle(
        color: Colors.teal,
        fontSize: 16.0,
        letterSpacing: 0.5,
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: Colors.blueGrey[300],
        fontSize: 16.0,
        decoration: TextDecoration.none,
      ),
      floatingLabelBehavior: FloatingLabelBehavior.never,
      filled: true,
      fillColor: Colors.blueGrey[50],
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(25.0),
        borderSide: BorderSide(color: Colors.blueGrey[300]),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(25.0),
        borderSide: const BorderSide(width: 0.0, style: BorderStyle.none),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: TextStyle(
          fontSize: 18,
        ),
        minimumSize: Size.fromHeight(40),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        side: const BorderSide(width: 0.0, style: BorderStyle.none),
      ),
    ),
  ), _darkTheme = ThemeData(
    fontFamily: 'Nunito',
    brightness: Brightness.dark,
    primarySwatch: Colors.teal,
    primaryColor: Colors.teal,
    accentColor: Colors.tealAccent,
    canvasColor: Color.fromRGBO(32, 36, 66, 1.0),
    unselectedWidgetColor: Colors.blueGrey[300],
    applyElevationOverlayColor: true,
    appBarTheme: AppBarTheme(
      color: Colors.teal,
      elevation: 0,
    ),
    textTheme: TextTheme(
      headline1: TextStyle(
        color: Colors.white,
        fontSize: 32.0,
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.none,
      ),
      headline2: TextStyle(
        color: Colors.white,
        fontSize: 24.0,
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.none,
      ),
      headline3: TextStyle(
        color: Colors.white,
        fontSize: 18.0,
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.none,
      ),
      headline4: TextStyle(
        color: Colors.white70,
        fontSize: 16.0,
        fontWeight: FontWeight.bold,
      ),
      headline6: TextStyle(
        color: Colors.white38,
        fontSize: 15.0,
        fontStyle: FontStyle.italic,
      ),
      bodyText1: TextStyle(
        color: Colors.blueGrey[200],
        fontSize: 16.0,
        decoration: TextDecoration.none,
      ),
      bodyText2: TextStyle(
        color: Colors.white70,
        fontSize: 15.0,
        decoration: TextDecoration.none,
      ),
      overline: TextStyle(
        color: Colors.tealAccent,
        fontSize: 16.0,
        letterSpacing: 0.5,
      ),

      subtitle2: TextStyle(
        color: Colors.amberAccent,
        fontSize: 16.0,
      )
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
          color: Colors.blueGrey[300],
          fontSize: 16.0,
          decoration: TextDecoration.none,
      ),
      floatingLabelBehavior : FloatingLabelBehavior.never,
      filled: true,
      fillColor: Color.fromRGBO(36, 40, 70, 1),
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      border:  OutlineInputBorder(
        borderRadius: BorderRadius.circular(25.0),
        borderSide: BorderSide(color: Colors.blueGrey[300]),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(25.0),
        borderSide: const BorderSide(width: 0.0, style: BorderStyle.none),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: TextStyle(
            fontSize: 18,
        ),
        minimumSize: Size.fromHeight(40),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        side: const BorderSide(width: 0.0, style: BorderStyle.none),
      ),
    ),
  ), _currentTheme;

  ThemeData get lightTheme {
    return _lightTheme;
  }

  ThemeData get darkTheme {
    return _darkTheme;
  }

  ThemeData get getTheme {
    if (_currentTheme != null) {
      return _currentTheme;
    } else {
      SharedPreferences.getInstance().then((prefs) {
        _isLoading = true;
        if (prefs.getString('themeData') == null) {
          switchTheme();
        } else {
          final themeData = json.decode(prefs.getString('themeData'));
          if (themeData['theme'] == 'light') {
            _currentTheme = _lightTheme;
            notifyListeners();
            _isLoading = false;
            return _lightTheme;
          } else {
            _currentTheme = _darkTheme;
            notifyListeners();
            _isLoading = false;
            return _darkTheme;
          }
        }
      });
    }
  }

  bool loading() => _isLoading;

  Future<void> switchTheme () async {
    _currentTheme = _currentTheme == _darkTheme ? _lightTheme : _darkTheme;
    final prefs = await SharedPreferences.getInstance();
    var themeData = ({}) as dynamic;
    if (_currentTheme == lightTheme) {
      themeData = json.encode({
        'theme': 'light',
      });
    } else {
      themeData = json.encode({
        'theme': 'dark',
      });
    }
    prefs.setString('themeData', themeData);
    notifyListeners();
  }
}


