import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class News with ChangeNotifier {
  String token;

  News(this.token);

  Future<List<dynamic>> get news async {
    final url = Uri.parse(
        'https://bandit-and-heroes-default-rtdb.firebaseio.com/news.json?auth=$token');
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      return extractedData;
    } catch (error) {
      throw error;
    }
  }
}