import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsSlider extends StatefulWidget {
  List<dynamic> _news;

  NewsSlider(this._news);

  @override
  _NewsSliderState createState() => _NewsSliderState();
}

class _NewsSliderState extends State<NewsSlider> {

  int _current = 0;
  List<Widget> imageSliders = [];

  void initState() {
    widget._news.forEach((newsDetail) {
      imageSliders.add(
        NewsSlide(
          newsDetail['title'],
          newsDetail['description'],
          newsDetail['imgUrl'],
          newsDetail['link']
        )
      );
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          CarouselSlider(
            options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }
            ),
            items: imageSliders.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      child: i,
                  );
                },
              );
            }).toList()
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imageSliders.map((url) {
              int index = imageSliders.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Theme.of(context).accentColor
                      : Color.fromRGBO(255, 255, 255, 0.5),
                ),
              );
            }).toList(),
          ),
        ]
    );
  }
}

class NewsSlide extends StatelessWidget {
  final String title;
  final String description;
  final String imgUrl;
  final String link;

  NewsSlide(this.title, this.description, this.imgUrl, this.link);

  Future<void> _launchURL(url) async {
    await launch(url);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => link != null ? _launchURL(link) : null,
      child: Stack(
        children: [
          Shimmer(
            color: Colors.black,
            enabled: true,
            direction: ShimmerDirection.fromLTRB(),
            child: Container(
              color: Colors.blueGrey[100],
            ),
          ),
          FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: imgUrl,
              width: double.infinity,
              height: 250,
              fit: BoxFit.cover
          ),
          Positioned(
            bottom: 0,
            child: Container(
                padding: const EdgeInsets.only(left: 10, right: 15, top: 0, bottom: 0),
                width: 300,
                color: Colors.black54,
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: title,
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      TextSpan(
                        text: '\n$description',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ],
                  ),
                )
            ),
          ),
        ],
      ),
    );
  }
}


